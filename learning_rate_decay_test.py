import numpy as np
import timeit
import matplotlib.pyplot as plt


class StepDecay():
    def __init__(self, initAlpha=0.001, factor=0.8, dropEvery=20):
        # store the base initial learning rate, drop factor, and
        # epochs to drop every
        self.initAlpha = initAlpha
        self.factor = factor
        self.dropEvery = dropEvery
        self.epoch = 0
    def __call__(self, epoch):
        # compute the learning rate for the current epoch
        exp = np.floor(1 + self.epoch  / self.dropEvery)
        alpha = self.initAlpha * (self.factor ** exp)
        print("Learning rate: ",alpha,"epoch: ",self.epoch )
        self.epoch += 1
        #return the learning rate
        return float(alpha)


class PolynomialDecay():
    def __init__(self, maxEpochs=100, initAlpha=0.01, power=1.0):
        # store the maximum number of epochs, base learning rate,
	    # and power(exponent) of the polynomial
        self.maxEpochs = maxEpochs
        self.initAlpha = initAlpha
        self.power = power
        #self.epoch = 0
    def __call__(self, epoch):
        # compute the new learning rate based on polynomial decay
        decay = (1 - (epoch / float(self.maxEpochs))) ** self.power
        alpha = self.initAlpha * decay
        print("Learning rate: ", alpha, "epoch: ", epoch)
        #return the new learning rate
        return float(alpha)


if __name__ == "__main__":
    initAlpha = 0.0001
    factor = 0.8
    dropEvery = 20
    SD=StepDecay(initAlpha,factor,dropEvery)

    maxEpochs = 500
    initAlpha = 3e-4
    power = 4.0
    PD = PolynomialDecay(maxEpochs,initAlpha,power)
    alpha = []
    for epoch in range (0,maxEpochs):
        alpha.append(PD.__call__(epoch))
        #alpha.append(SD.__call__(epoch))

    plt.plot(alpha)
    plt.show()