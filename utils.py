import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
import re
import shutil
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
import scipy.io
from sklearn.metrics import   confusion_matrix
from PIL import Image

class Utils():

    flags = {'remove_iligeal_file':0,'debug_mode':0}

    def __init__(self, input_sampels_file_path,input_labels_file_path,output_samples_file_path):
        self.input_sampels_file_path = input_sampels_file_path
        self.input_labels_file_path = input_labels_file_path
        self.output_samples_file_path = output_samples_file_path


    def show_samples(self):
        samples_file = np.load(self.input_sampels_file_path)
        labels_file = np.load(self.input_labels_file_path)
        indices = np.random.permutation(samples_file.shape[0])
        samples_arr = samples_file#[indices[:int(samples_file.shape[0] * 0.8)]], samples_file[indices[int(samples_file.shape[0] * 0.8):]]
        labels_arr = labels_file#[indices[:int(labels_file.shape[0] * 0.8)]]#, labels_file[indices[int(labels_file.shape[0] * 0.8):]]
        show_matrix_col_dim = 1
        show_matrix_row_dim = 13
        samples_offset = 4715

        for i in range (0,15):
            fig, axarr = plt.subplots(show_matrix_row_dim, show_matrix_col_dim)
            #plt.switch_backend('TkAgg')  # TkAgg (instead Qt4Agg)

            for col in range (0,show_matrix_col_dim):
                for row in range (0,show_matrix_row_dim):
                    arr_indx = samples_offset+row+col+(show_matrix_row_dim*i)
                    if (labels_arr[arr_indx] == 1):
                        axarr[row].set_title("with tape", fontsize=10)
                    else:
                        axarr[row].set_title("wo tape", fontsize=10)
                    axarr[row].plot(samples_arr[arr_indx,:,:].T)
            #mng = plt.get_current_fig_manager()
            #mng.resize(*mng.window.maxsize())
            plt.show()

        return()



    def plot_loss_graph(self):
        DISC_LOSS_FILE_PATH = ('C:/Projects/Python/CRNN/loss_files/DCGAN2/losses_disc.txt')
        ADV_LOSS_FILE_PATH = ('C:/Projects/Python/CRNN/loss_files/DCGAN2/losses_adv.txt')
        disc_training_loss = []
        adv_training_loss = []


        with open(DISC_LOSS_FILE_PATH) as loss_file:
            for line in loss_file:
                rd_line = line.split(' ')
                if (len(rd_line) ==4): #Ignore uncompleted line
                    disc_training_loss.append(float(rd_line[3][:-2]))

        with open(ADV_LOSS_FILE_PATH) as loss_file:
            for line in loss_file:
                rd_line = line.split(' ')
                if (len(rd_line) ==4): #Ignore uncompleted line
                    adv_training_loss.append(float(rd_line[3][:-2]))

        print(len(disc_training_loss))
        plot_len = min(len(disc_training_loss),len(adv_training_loss))#protect against unequale length files
        epoch = np.arange(plot_len)
        plt.plot(epoch, disc_training_loss[:plot_len], color='blue',label='DISC')
        plt.plot(epoch, adv_training_loss[:plot_len], color='orange',label='ADV')
        plt.title("DCGAN training Loss")
        plt.legend(loc='upper right')
        plt.show()
        return()


    def separate_foles_to_sub_folders(self):
        num_of_file = 500
        for root, dirnames, filenames in os.walk(self.input_sampels_file_path):
            file_index = 0
            sub_folder = 0
            for file in filenames:
                if (file_index % num_of_file == 0):
                    sub_folder += 1
                    os.mkdir(os.path.join(self.output_samples_file_path, str(sub_folder)))
                os.rename(os.path.join(root, file), os.path.join(root, str(sub_folder), file))
                #target_file = os.path.join(self.output_folder_path,str(sub_folder), file)
                #shutil.copyfile(os.path.join(root, file), target_file)
                file_index += 1
        return ()





def detect_thread_sensor(input_sampels_file_path):
    samples_file = np.load(input_sampels_file_path)[:,:,:100]
    samples_file = samples_file.reshape(samples_file.shape[0],samples_file.shape[2])
    col = np.zeros((samples_file.shape[0],1))
    samples_file =np.hstack((samples_file,col))

    for i in range(0,samples_file.shape[0]):
        samples_file[i,-1] = np.std(samples_file[i,:-1])


    x_axis = np.arange(samples_file.shape[0])
    plt.scatter(x_axis,samples_file[:,-1])
    plt.show()

    std_thres = 40#15
    above_threshold = np.where(samples_file[:,-1] >=std_thres )
    below_threshold = np.where(samples_file[:, -1] < std_thres)
    with_thread_samples = samples_file[above_threshold][:,:100]
    without_thread_samples = samples_file[below_threshold][:,:100]


    x_ones = np.ones((with_thread_samples.shape[0], 1))
    xwith = np.hstack([with_thread_samples, x_ones])

    x_zeros = np.zeros((without_thread_samples.shape[0], 1))
    xwo = np.hstack([without_thread_samples, x_zeros])

    ds = np.vstack([xwith , xwo])
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/X_thread_channels_detect_id13.npy', ds[:, :-1])
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Y_thread_channels_detect_id13.npy', ds[:, -1])


    X_train, X_test, y_train, y_test = train_test_split(ds[:, :-1], ds[:, -1], test_size=0.25, shuffle=True)
    logist_regression = LogisticRegression(C=1, solver='liblinear', warm_start=True, multi_class='ovr', penalty='l1',dual=False).fit(X_train, y_train)  # thresh 0.999 FN 1.2 , FP 0.56
    # logist_regression = LogisticRegression(penalty='l2',dual=False).fit(X_train,y_train)  # thresh 0.999 FN 1.2 , FP 0.56
    print("logistic regression prediction accuracy is: ", logist_regression.score(X_test, y_test) * 100, "%")


def detect_stat(input_sampels_text_file_path):

    text_file = open(input_sampels_text_file_path, "r")
    SN = {'X6J0119017':0,'KH88868063':1,'DA23640635':2,'UM11760887':3,'H4R9855587':4,'RP83175918':5,'H6E3795108':6,'WA44608390':7,'NF39351813':8,'D9C4460894':9}

    data_len = len(open(input_sampels_text_file_path).readlines())

    data_array = np.zeros((10,18,4))# num of banknote = 10 , num of bank Id's = 4 , num of channles =18
    num_of_bank_id_per_sn=np.zeros((10,1,4))
    bank_id_1=np.zeros(18)
    bank_id_2=np.zeros(18)
    bank_id_3=np.zeros(18)
    num_of_samp_bank_id_0 = 0
    num_of_samp_bank_id_1 = 0
    num_of_samp_bank_id_2 = 0
    num_of_samp_bank_id_3 = 0
    for i in range(0,data_len,2):
        data = text_file.readline().split(',')
        sn_key = data[0][:-5]
        if sn_key in SN:
            sn_dim = SN[sn_key]
        else:
            continue;
        bank_id = int(data[1][-2])
        num_of_bank_id_per_sn[sn_dim, :,bank_id] += 1
        '''
        if (bank_id == int(0)):
            num_of_bank_id_per_sn[sn_dim,0,:] += 1
        elif(bank_id==int(1)):
            num_of_bank_id_per_sn[sn_dim,1,:] += 1
        elif(bank_id==int(2)):
            num_of_bank_id_per_sn[sn_dim,0,:] += 1
        elif(bank_id==int(3)):
            num_of_samp_bank_id_3 +=1
        '''
        data = text_file.readline().split(',')

        for j in range(0,len(data)-1):
            if (bank_id==int(0)):
                data_array[sn_dim,int(data[j]),0] +=1
                #bank_id_0[int(data[j])]+=1
            elif (bank_id==int(1)):
                #bank_id_1[int(data[j])]+=1
                data_array[sn_dim, int(data[j]), 1] += 1
            elif (bank_id == int(2)):
                data_array[sn_dim, int(data[j]),2] += 1
                #bank_id_2[int(data[j])]+=1
            elif (bank_id==int(3)):
                data_array[sn_dim, int(data[j]), 3] += 1
                #bank_id_3[int(data[j])]+=1
    det_precent = np.zeros((10,18,4))
    for i in range (0,10):
        for j in range(0,4):
            det_precent[i, :, 0]=  ( data_array[i, :, 0]/num_of_bank_id_per_sn[i, :,0]) * 100
            det_precent[i, :, 1] = ( data_array[i, :, 1]/num_of_bank_id_per_sn[i, :,1]) * 100
            det_precent[i, :, 2] = ( data_array[i, :, 2] / num_of_bank_id_per_sn[i, :,2]) * 100
            det_precent[i, :, 3] = ( data_array[i, :, 3]/num_of_bank_id_per_sn[i, :,3]) * 100
    import pandas as pd
    #SN = {'X6J0119017':0,'KH88868063':1,'DA23640635':2,'UM11760887':3,'H4R9855587':4,'RP83175918':5,'H6E3795108':6,'WA44608390':7,'NF39351813':8,'D9C4460894':9}
    kk = SN.keys()
    with pd.ExcelWriter('C:/Projects/Python/Tape_detect/debug_file2.xlsx') as writer:
        pd.DataFrame(det_precent[0, :, :]).to_excel(writer, sheet_name='X6J0119017')
        pd.DataFrame(det_precent[1, :, :]).to_excel(writer, sheet_name='KH88868063')
        pd.DataFrame(det_precent[2, :, :]).to_excel(writer, sheet_name='DA23640635')
        pd.DataFrame(det_precent[3, :, :]).to_excel(writer, sheet_name='UM11760887')
        pd.DataFrame(det_precent[4, :, :]).to_excel(writer, sheet_name='H4R9855587')
        pd.DataFrame(det_precent[5, :, :]).to_excel(writer, sheet_name='RP83175918')
        pd.DataFrame(det_precent[6, :, :]).to_excel(writer, sheet_name='H6E3795108')
        pd.DataFrame(det_precent[7, :, :]).to_excel(writer, sheet_name='WA44608390')
        pd.DataFrame(det_precent[8, :, :]).to_excel(writer, sheet_name='NF39351813')
        pd.DataFrame(det_precent[9, :, :]).to_excel(writer, sheet_name='D9C4460894')

    print("Stop")

def calc_threshold_for_test_data(input_sampels_with_tape_file_path,input_sampels_wo_tape_file_path):
    samples_file_with_tape = np.loadtxt(input_sampels_with_tape_file_path)
    y_test_h = np.ones_like(samples_file_with_tape)
    samples_file_wo_tape = np.loadtxt(input_sampels_wo_tape_file_path)
    y_test_l = np.zeros_like(samples_file_wo_tape)
    y_test = np.concatenate((y_test_h,y_test_l),axis=0)
    Y_pred = np.concatenate((samples_file_with_tape,samples_file_wo_tape),axis=0)

    thresh = 1.8e10


    check_thresh = 1
    Y_pred_after_threshold = np.zeros_like(Y_pred)
    if (check_thresh):
        for i in range(0, Y_pred.shape[0]):
            if (Y_pred[i] >= thresh):
                Y_pred_after_threshold[i] = 1
            else:
                Y_pred_after_threshold[i] = 0
    matrix = confusion_matrix(y_test, Y_pred_after_threshold)
    tn, fp, fn, tp = matrix.ravel()
    print("False positive %", (fp / (tn + fp)) * 100)
    print("False Negative %", (fn / (fn + tp)) * 100)

    samples_file_with_tape = np.sort(samples_file_with_tape)
    samples_file_with_tape = np.atleast_2d(samples_file_with_tape).T
    samples_file_wo_tape = np.sort(samples_file_wo_tape)[::-1]

    plt.plot(samples_file_with_tape,label = 'With Tape')
    plt.plot(samples_file_wo_tape,label = 'Without Tape')
    plt.legend()
    plt.show()
    overlap_area = np.where(samples_file_with_tape < samples_file_wo_tape[0])

    return()

def signaltonoise(array_1d, axis=0, ddof=0):
    a = array_1d#np.asanyarray(a)
    m = a.mean(axis)
    sd = a.std(axis=axis, ddof=ddof)
    return np.where(sd == 0, 0, m / sd)

def check_multiclass():

    #Y_4_cls = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015_avg_norm_4_cls_with_exp.npy')
    #Y_pred_4_cls = np.load('C:/Projects/Python/Tape_detect/debug/four_cls/Y_pred_4_cls3.npy')

    #Y_4_cls = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015_avg_norm_4_cls_with_exp_wo_mid2.npy')
    #Y_pred_4_cls = np.load('C:/Projects/Python/Tape_detect/debug/four_cls/Y_pred_4_cls5.npy')

    #Y_pred_4_cls = np.load('C:/Projects/Python/Tape_detect/debug/four_cls/Y_pred_4_cls_mid2.npy')
    #Y_4_cls = np.ones((Y_pred_4_cls.shape[0], 1))

    #Y_4_cls = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015_avg_norm_5_cls_with_exp.npy')
    #Y_pred_4_cls = np.load('C:/Projects/Python/Tape_detect/debug/four_cls/Y_pred_4_cls6.npy')

    #Y_4_cls = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015_avg_norm_6_cls.npy')
    #Y_pred_4_cls = np.load('C:/Projects/Python/Tape_detect/debug/four_cls/Y_pred_6_cls.npy')

    Y_4_cls = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015_avg_norm.npy')
    X = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_regular_channels_2015_avg_norm.npy')

    Y_pred_4_cls = np.load('C:/Projects/Python/Tape_detect/debug/four_cls/Y_pred_softmax.npy')


    #Y_4_cls = Y_4_cls[30000:]
    Y_4_cls = np.expand_dims(Y_4_cls, axis=1)



    #Y_pred_4_cls[:,0][Y_pred_4_cls[:,0] < 0.99] =0
    Y_pred_4_cls[:,1][Y_pred_4_cls[:,1] < 0.99] =0
    #Y_pred_4_cls[:,2][Y_pred_4_cls[:,2] < 0.9] = 0
    #Y_pred_4_cls[:,3][Y_pred_4_cls[:, 3] < 0.99] = 0
    #Y_pred_4_cls[:,4][Y_pred_4_cls[:,4] < 0.99] = 0
    #Y_pred_4_cls[:,5][Y_pred_4_cls[:, 5] < 0.99] = 0

    #Y_pred_4_cls_to_2_cls = np.load('C:/Projects/Python/Tape_detect/debug/four_cls/Y_regular_channels_2015_avg_norm_4_cls_wo_exp_test.npy')
    #Y_pred_4_cls_to_2_cls = np.expand_dims(Y_pred_4_cls_to_2_cls, axis=1)
    Y_4_cls_to_2_cls = Y_4_cls.copy()
    Y_4_cls_to_2_cls_for_conf_mat = Y_4_cls.copy()
    Y_4_cls_to_2_cls_for_conf_mat[Y_4_cls_to_2_cls_for_conf_mat > 0] = 1

    Y_pred_4_cls_argmax =np.argmax(Y_pred_4_cls,axis=1)
    Y_pred_4_cls_argmax = np.expand_dims(Y_pred_4_cls_argmax,axis=1)
    Y_pred_4_cls_argmax_for_conf_mat =Y_pred_4_cls_argmax.copy()
    Y_pred_4_cls_argmax_for_conf_mat[Y_pred_4_cls_argmax_for_conf_mat>0]=1


    temp = np.hstack((Y_pred_4_cls,Y_4_cls,Y_4_cls_to_2_cls,Y_pred_4_cls_argmax))
    #temp_wo = temp[temp[:, 4] == 0]
    #fp = np.where(temp_wo[:,5] !=0)
    #fp_arr = temp_wo[fp]
    #fp_arr_for_cls_1 = fp_arr[fp_arr[:,5]==1]
    #fp_arr_for_cls_2 = fp_arr[fp_arr[:, 5] == 2]
    #temp_cls_1 = temp[temp[:, 5] == 1]
    #temp_cls_1 = temp_cls_1[temp_cls_1[:, 4] == 1]
    #temp_cls_2 = temp[temp[:, 5] == 2]
    #temp_cls_2 = temp_cls_2[temp_cls_2[:, 4] == 2]

    #plt.plot(np.sort(temp_cls_2[:, 2]))
    #plt.plot(np.sort(fp_arr_for_cls_2[:, 2]))
    #plt.show()

    #plt.plot(np.sort(temp_cls_1[:, 2]))
    #plt.plot(np.sort(fp_arr_for_cls_1[:, 2]))
    #plt.show()

    matrix = confusion_matrix(Y_4_cls_to_2_cls_for_conf_mat, Y_pred_4_cls_argmax_for_conf_mat)
    print(matrix)
    tn, fp, fn, tp = matrix.ravel()
    print("False positive %", (fp / (tn + fp)) * 100)
    print("False Negative %", (fn / (fn + tp)) * 100)
    print(((tn + tp) / (fn + fp + tn + tp))*100, '% Accuracy')

def verify_thread():
    #thread_file = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_thread.txt',delimiter=',', dtype=np.str)
    #np.save('C:/Projects/Python/Tape_detect/debug/debug_file_thread.npy',thread_file)
    thread_file = np.load('C:/Projects/Python/Tape_detect/debug/debug_file_thread.npy')

    thread_sides = thread_file[:,5].astype(np.int)

    thr_side_0 = np.where(thread_sides==0)
    thr_side_0=thread_file[thr_side_0[0]]
    bad_thr_1st_ch=thr_side_0[:, 1].astype(np.int)
    bad_thr_1st_ch = np.where(bad_thr_1st_ch==1)
    bad_thr_1st_ch = thr_side_0[bad_thr_1st_ch[0]]
    bad_thr_2nd_ch=thr_side_0[:, 3].astype(np.int)
    bad_thr_2nd_ch = np.where((bad_thr_2nd_ch==1) | (bad_thr_2nd_ch==4))
    bad_thr_2nd_ch = thr_side_0[bad_thr_2nd_ch[0]]
    bad_thr_ch_side_0 =np.concatenate((bad_thr_1st_ch[:,6],bad_thr_2nd_ch[:,6]))

    thr_side_1 = np.where(thread_sides==1)
    thr_side_1=thread_file[thr_side_1[0]]
    bad_thr_1st_ch=thr_side_0[:, 1].astype(np.int)
    bad_thr_1st_ch = np.where(bad_thr_1st_ch==1)
    bad_thr_1st_ch = thr_side_0[bad_thr_1st_ch[0]]
    bad_thr_2nd_ch=thr_side_0[:, 3].astype(np.int)
    bad_thr_2nd_ch = np.where((bad_thr_2nd_ch==1) | (bad_thr_2nd_ch==4))
    bad_thr_2nd_ch = thr_side_0[bad_thr_2nd_ch[0]]

    thr_side_2 = np.where(thread_sides==2)
    thr_side_2=thread_file[thr_side_2[0]]

    thr_side_3 = np.where(thread_sides==3)
    thr_side_3=thread_file[thr_side_3[0]]
    fig, ax = plt.subplots(2, 2, figsize=(8, 10))

    ax[0, 0].scatter(np.arange(thr_side_0.shape[0]),thr_side_0[:,1], label='1st THR CH')
    #ax[0, 0].scatter(np.arange(thr_side_0.shape[0]),thr_side_0[:, 3], label='2nd THR CH')
    ax[0, 0].set_title('Side 0')
    plt.legend()

    #ax[0, 1].scatter(np.arange(thr_side_1.shape[0]),thr_side_1[:,1], label='1st THR CH')
    ax[0, 1].scatter(np.arange(thr_side_1.shape[0]),thr_side_1[:, 3], label='2nd THR CH')
    ax[0, 1].set_title('Side 1')
    plt.legend()

    #ax[1, 0].scatter(np.arange(thr_side_2.shape[0]),thr_side_2[:,1], label='1st THR CH')
    ax[1, 0].scatter(np.arange(thr_side_2.shape[0]),thr_side_2[:, 3], label='2nd THR CH')
    ax[1, 0].set_title('Side 2')
    plt.legend()

    #ax[1, 1].scatter(np.arange(thr_side_3.shape[0]), thr_side_3[:, 1], label='1st THR CH')
    ax[1, 1].scatter(np.arange(thr_side_3.shape[0]), thr_side_3[:, 3], label='2nd THR CH')
    ax[1, 1].set_title('Side 3')
    plt.legend()
    plt.show()

if __name__ == "__main__":
    verify_thread()
    #check_multiclass()

    input_sampels_with_tape_file_path ='C:/Projects/Python/Tape_detect/debug/with_tape_cnn_results.txt'
    input_sampels_wo_tape_file_path ='C:/Projects/Python/Tape_detect/debug/wo_tape_cnn_results.txt'
    #calc_threshold_for_test_data(input_sampels_with_tape_file_path, input_sampels_wo_tape_file_path)



    '''
    from sklearn.ensemble import RandomForestClassifier
    X = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_thread_channels_2015_avg_norm.npy')
    Y = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_thread_channels_2015_avg_norm.npy')
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, shuffle=True, random_state=20)  # , random_state=20
    clf = RandomForestClassifier(max_depth=2, random_state=0)
    clf.fit(X_train, y_train)
    clf.predict(X[0])
    '''




    '''
    data_file = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/four_cls/Y_regular_channels_2015_avg_norm_4_cls_wo_exp_test.np')
    org =data_file[0,:]
    org_minus1 =data_file[1,:]
    org_plus1 = data_file[2, :]
    l1=0
    l2 =0

    l1_1=0
    l2_1=0

    l1_2=0
    l2_2=0

    for i in range (50,119):
        l1 +=  np.abs(org[i] - org_minus1[i-1])
        l2 +=  np.abs(org[i] - org_plus1[i-1])

        l1_1 +=  np.abs(org[i] - org_minus1[i])
        l2_1 +=  np.abs(org[i] - org_plus1[i])

        l1_2 +=  np.abs(org[i] - org_minus1[i+1])
        l2_2 +=  np.abs(org[i] - org_plus1[i+1])

        #l1 +=   np.abs(org_minus1[i+1] - org_minus1[i])
        #l2 +=   np.abs(org_plus1[i+1] - org_plus1[i])

        #l1 += org_minus1[i]
        #l2 +=  org_plus1[i]

    print(l1 , l2)
    '''
    ''''''
    #img = Image.open('C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/New_sensor_12_2020/Temp/0362.bmp')  # .convert('L')

    import SimpleITK as sitk


    #img = Image.open('C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/New_sensor_12_2020/Temp/0362.bmp')  # .convert('L')
    #img_arr = np.array(img)

    #itkimage = sitk.ReadImage('C:/Projects/DLMI/Dataset/SegPC/train/y/106_1.bmp')
    # Convert the image to a  numpy array first and then shuffle the dimensions to get axis in the order z,y,x
    #ct_scan = sitk.GetArrayFromImage(itkimage)
    '''
    # Read the origin of the ct_scan, will be used to convert the coordinates from world to voxel and vice versa.
    origin = np.array(list(reversed(itkimage.GetOrigin())))

    # Read the spacing along each dimension
    spacing = np.array(list(reversed(itkimage.GetSpacing())))
    temp = np.fromfile('C:/Projects/OCT/Retouch/Topcon/Train/TRAIN049/oct.raw', dtype=np.uint8)
    temp = temp.reshape(128, 1024, 512)
    plt.figure(figsize=(4, 4))
    plt.gray()
    plt.subplots_adjust(0, 0, 1, 1, 0.01, 0.01)
    for i in range(0, 16):
        plt.subplot(4, 4, i + 1), plt.imshow(temp[i]), plt.axis('off')
    plt.show()
    '''

    '''
    from scipy import signal
    import matplotlib.pyplot as plt

    t = np.linspace(-1, 18, 2 * 100, endpoint=False)
    i, e = signal.gausspulse(t, fc=5, retquad=False, retenv=True)
    plt.plot(e*.7)

    t = np.linspace(-1, 1, 2 * 30, endpoint=False)
    i, e = signal.gausspulse(t, fc=15, retquad=False, retenv=True)
    plt.plot(e*.5)

    t = np.linspace(-1, 1, 2 * 80, endpoint=False)
    i, e = signal.gausspulse(t, fc=66, retquad=False, retenv=True)
    plt.plot(e*.3)

    plt.show()
    '''

    from sklearn.model_selection import train_test_split
    from sklearn.linear_model import LogisticRegression

    '''
    def sigmoid(x):
        return 1 / (1 + np.exp(-x))

    X = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_regular_channels_2015_avg_norm_4_nar_reg.npy')
    Y = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015avg_norm_4_nar_reg.npy')


    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, shuffle=True, random_state=20)  # , random_state=20
    logist_regression = LogisticRegression(max_iter=5000 ,penalty='l1',solver="liblinear",C=1)
    logist_regression.fit(X_train, y_train)
    Y_pred_all = logist_regression.predict(X)
    Y_pred_all = np.expand_dims(Y_pred_all, axis=1)
    ne = np.where(Y_pred_all != np.expand_dims(Y,axis=1))
    #np.where(coeff !=0)[1]
    print((logist_regression.score(X_test, y_test) * 100) , ne[0].shape[0] ,ne[0])

    lr_coeff = logist_regression.coef_

    Y_pred = np.zeros((X.shape[0], 1))
    for i in range(0, X.shape[0]):
        temp = np.expand_dims(X[i], axis=0)
        Y_pred[i] = sigmoid(np.dot(temp, lr_coeff.T))

    thresh = 0.51
    Y_pred[Y_pred<thresh] =0
    Y_pred[Y_pred >= thresh] = 1

    matrix = confusion_matrix(Y, Y_pred)
    tn, fp, fn, tp = matrix.ravel()
    print("False positive %", (fp / (tn + fp)) * 100)
    print("False Negative %", (fn / (fn + tp)) * 100)
    '''



    import SimpleITK as sitk

    '''
    itkimage = sitk.ReadImage('C:/Projects/OCT/Retouch/Topcon/Train/TRAIN049/oct.mhd')
    # Convert the image to a  numpy array first and then shuffle the dimensions to get axis in the order z,y,x
    ct_scan = sitk.GetArrayFromImage(itkimage)

    # Read the origin of the ct_scan, will be used to convert the coordinates from world to voxel and vice versa.
    origin = np.array(list(reversed(itkimage.GetOrigin())))

    # Read the spacing along each dimension
    spacing = np.array(list(reversed(itkimage.GetSpacing())))
    temp = np.fromfile('C:/Projects/OCT/Retouch/Topcon/Train/TRAIN049/oct.raw', dtype=np.uint8)
    temp = temp.reshape(128, 1024, 512)
    plt.figure(figsize=(4, 4))
    plt.gray()
    plt.subplots_adjust(0, 0, 1, 1, 0.01, 0.01)
    for i in range(0, 16):
        plt.subplot(4, 4, i + 1), plt.imshow(temp[i]), plt.axis('off')
    plt.show()
    '''

    #itkimage = sitk.ReadImage('C:/Projects/OCT/Retouch/Cirrus/Test/TEST002/oct.mhd')
    #itkimage = sitk.ReadImage('C:/Projects/OCT/Retouch/Topcon/Train/TRAIN049/oct.mhd')
    #itkimage = sitk.ReadImage('C:/Projects/OCT/Retouch/Topcon/Test/TEST030/oct.mhd')
    #itkimage = sitk.ReadImage('C:/Projects/OCT/Retouch/Spectralis/Train/TRAIN025/oct.mhd')
    #itkimage = sitk.ReadImage('C:/Projects/OCT/Retouch/Spectralis/Test/TEST015/oct.mhd')


    #ct_scan = sitk.GetArrayFromImage(itkimage)
    #Image_batch = np.fromfile('C:/Projects/OCT/Retouch/Spectralis/Test/TEST015/oct.raw', dtype = np.uint16)
    #Image_batch = Image_batch.reshape(49, 496, 512)
    #plt.imshow(ct_scan[0,:,:])
    #plt.show()



    input_sampels_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/X_tape_detect_in_whole_area_with_thread.npy'
    input_labels_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/Y_tape_detect_in_whole_area_with_thread.npy'

    input_sampels_file_path = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/New_sensor_09_2020/Failures/scn_for_63/'
    output_samples_file_path = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/New_sensor_09_2020/Failures/scn_for_63/'
    #UT =Utils(input_sampels_file_path,input_labels_file_path,output_samples_file_path)
    #UT.show_samples()
    #UT.plot_loss_graph()
    #UT.separate_foles_to_sub_folders()



