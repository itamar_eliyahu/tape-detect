from sklearn.metrics import classification_report, confusion_matrix, precision_recall_curve, auc, accuracy_score
import numpy as np
import matplotlib.pyplot as plt



class Model_Output_Analysis():
    flags = {'tape_detect_next_thread_channels': 1, 'tape_detect_in_watermark_channels': 0,'tape_detect_in_regular_channels': 0, 'tape_detect_in_thread_channels':0, }

    def __init__(self):

        if (self.flags['tape_detect_in_thread_channels']):
            self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_thread_channels_2015_avg_norm.npy')
            self.network_output  = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/tape_detect_in_thread_predict_all.npy')
            self.y_pred_before_sigmoid =np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/thread_channel_model_dense1_by_dense2_wo_sigmoid.npy')


        elif (self.flags['tape_detect_next_thread_channels']):
            self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_next_to_thread_channels_2015_avg_norm.npy')
            self.network_output  = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/tape_detect_next_to_thread_predict_all.npy')
            self.y_pred_before_sigmoid =np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/next_thread_channel_model_dense1_by_dense2_wo_sigmoid.npy')

        elif (self.flags['tape_detect_in_regular_channels']):
            self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_regular_channels_2015_avg_norm.npy')
            self.network_output  = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/tape_detect_in_Regular_predict_all.npy')
            self.y_pred_before_sigmoid =np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/regular_model_dense1_by_dense2_wo_sigmoid.npy')

        elif (self.flags['tape_detect_in_watermark_channels']):
            self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_watermark_channels_2015_avg_norm.npy')
            self.network_output  = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark/tape_detect_in_watermark_predict_all.npy')
            self.y_pred_before_sigmoid =np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark/watermark_model_dense1_by_dense2_wo_sigmoid.npy')

        self.y_pred_before_sigmoid = self.y_pred_before_sigmoid[:, 0]
        self.Y_test = self.network_output[:, 1]
        self.Y_pred = self.network_output[:, 0]
        self.Y_pred_after_threshold = np.zeros_like(self.Y_pred)


    def plot_y_pred_before_sigmoid(self):
        plt.plot(self.y_pred_before_sigmoid[np.where(self.Y_test[:] == 0)],label = 'Without Tape')
        plt.plot(self.y_pred_before_sigmoid[np.where(self.Y_test[:] == 1)],label = 'With Tape')
        plt.xlabel('Sample number')
        plt.xlabel('Network output before sigmoid')
        plt.legend()
        plt.show()
        thresh = 0.51
        for i in range(0, self.y_pred_before_sigmoid.shape[0]):
            if (self.y_pred_before_sigmoid[i] >= thresh):
                self.Y_pred_after_threshold[i] = 1

        matrix = confusion_matrix(self.Y_test, self.Y_pred_after_threshold)
        tn, fp, fn, tp = matrix.ravel()
        print("False positive %", (fp / (tn + fp)) * 100)
        print("False Negative %", (fn / (fn + tp)) * 100)
        print(((tn + tp) / (fn + fp + tn + tp)), '% Accuracy')

    def plot_y_test(self):
        plt.plot(self.Y_pred[np.where(self.Y_test[:] == 0)],label = 'Without Tape')
        plt.plot(self.Y_pred[np.where(self.Y_test[:] == 1)],label = 'With Tape')
        plt.xlabel('Sample number')
        plt.xlabel('Network output from scratch')
        plt.legend()
        plt.show()

    def plot_confusion_matrix(self):

        thresh =  0.25e11
        check_thresh =1
        if (check_thresh):
            for i in range(0, self.Y_pred.shape[0]):
                if (self.Y_pred[i] >= thresh):
                    self.Y_pred_after_threshold[i] = 1
                else:
                    self.Y_pred_after_threshold[i] = 0
        else:
            self.Y_pred_after_threshold =  1 / (1 + np.exp(-self.Y_pred))#sigmoid
        matrix = confusion_matrix(self.Y_test, self.Y_pred_after_threshold)
        tn, fp, fn, tp = matrix.ravel()
        print("False positive %", (fp / (tn + fp)) * 100)
        print("False Negative %", (fn / (fn + tp)) * 100)
        print(((tn + tp) / (fn + fp + tn + tp)), '% Accuracy')

        return()

if __name__ == "__main__":
    MOA = Model_Output_Analysis()
    MOA.plot_y_pred_before_sigmoid()
    MOA.plot_y_test()
    MOA.plot_confusion_matrix()