import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from keras.callbacks import ModelCheckpoint
import cv2

# opencv loads the image in BGR, convert it to RGB
img = cv2.cvtColor(cv2.imread('C:\\Projects\\img_14.jpeg'),cv2.COLOR_BGR2RGB)
lower_white = np.array([220, 220, 220], dtype=np.uint8)
upper_white = np.array([255, 255, 255], dtype=np.uint8)
mask = cv2.inRange(img, lower_white, upper_white)  # could also use threshold
mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))  # "erase" the small white points in the resulting mask
mask = cv2.bitwise_not(mask)  # invert mask

# load background (could be an image too)
#bk = np.full(img.shape, [255,230,230], dtype=np.uint8)  # pink
bk = np.full(img.shape, [255,255,255], dtype=np.uint8)  # white

# get masked foreground
fg_masked = cv2.bitwise_and(img, img, mask=mask)

# get masked background, mask must be inverted
mask = cv2.bitwise_not(mask)
bk_masked = cv2.bitwise_and(bk, bk, mask=mask)

# combine masked foreground and masked background
final = cv2.bitwise_or(fg_masked, bk_masked)
from PIL import Image
image = Image.fromarray(final)
image.save('C:\\Projects\\img_bk_14.jpeg')


final2 = final
final2[1460:1485,522:705,:] = final[835:860,517:700,:]
image = Image.fromarray(final2)
image.save('C:\\Projects\\img_bk3.jpeg')
mask = cv2.bitwise_not(mask)  # revert mask to original
def fit_model():
    best_model_file_path = 'C:/Projects/ADverserial_ML/hw1/'
    callbacks_list = [ModelCheckpoint(filepath=best_model_file_path, monitor='val_loss', verbose=1, save_best_only=True ,save_weights_only=False,mode='auto')]


mnist = tf.keras.datasets.mnist
(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

train_images = train_images / 255.0
test_images = test_images / 255.0
class_names = ['0', '1', '2', '3', '4','5', '6', '7', '8', '9']

plt.figure(figsize=(10,10))
for i in range(25):
    plt.subplot(5,5,i+1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(train_images[i], cmap=plt.cm.binary)
    #plt.xlabel(class_names[train_labels[i]])
plt.show()


model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(10)
])
model.compile(optimizer='adam',loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),metrics=['accuracy'])


model.fit(train_images, train_labels, epochs=10)