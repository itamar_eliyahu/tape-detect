from keras.utils import to_categorical
import matplotlib.pyplot as plt
#from keras.callbacks import ModelCheckpoint
from keras.preprocessing.sequence import pad_sequences
import tensorboard as tensorboard
import os
import numpy as np
from random import shuffle
import tensorflow as tf
from keras.layers import  Dropout, LSTM,MaxPooling1D, Flatten, Activation,Input, Dense, MaxPool2D, Lambda, Bidirectional,Conv1D,GlobalAveragePooling1D,Reshape,AveragePooling1D
from keras.models import Model,Sequential
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint,TensorBoard,EarlyStopping,LearningRateScheduler
import time
from sklearn.metrics import classification_report, confusion_matrix
from keras.utils.vis_utils import plot_model
import pydot
import graphviz
import timeit


def one_dim_cnn_model_2(n_kernel_1,n_kernel_2,n_outputs):
	# each time stamp we got 14 sampels of 14 sensors of Tape ,each sample is 110 long .
	# so our matrix is 14 rows 110 columns.
	# but because this samples are independent , it means that if we detect tape in one sample , out of 14 , we report tape detected
	# so actually  we need to feed the nework with one vector eact time stamp
	# bacause of this reason our input shape is 1 row of 110 columns means 110 features
	input_shape = [1, 110]
	model = Sequential()
	#model.add(Conv1D(filters=64, kernel_size=n_kernel, activation='relu', input_shape=(n_timesteps,n_features)))
	#input shape is (1, 110), which is interpreted as a 1 width, 110 channel array.
	#We probably want the opposite, that is, width 110 and 1 channel.As our array has a single element.
	#performing convolution without padding of SAME will lead to a negative dimension, which produces the error.

	#Note that convolution is always performed on the spatial dimensions,
	#which for Conv1D the second to last dimension in the shape array. The last dimension represents the channels.
	model.add(Reshape((1, 110), input_shape=(input_shape[2],)))
	print(model.output_shape)
	model.add(Conv1D(filters=32, kernel_size=n_kernel_1, activation='relu', input_shape=input_shape, padding="same",strides=2))
	print(model.input)
	print(model.output_shape)

	model.add(Conv1D(filters=32, kernel_size=n_kernel_1, activation='relu', padding="same",strides=2))
	print(model.output_shape)
	model.add(Dropout(0.5))
	print(model.output_shape)
	model.add(MaxPooling1D(pool_size=2,padding="same"))
	print(model.output_shape)

	model.add(Conv1D(filters=32, kernel_size=n_kernel_2, activation='relu', padding="same", strides=2))
	print(model.output_shape)
	model.add(Conv1D(filters=32, kernel_size=n_kernel_2, activation='relu', padding="same"))
	# print(model.output_shape)
	model.add(Dropout(0.5))
	#print(model.output_shape)
	model.add(MaxPooling1D(pool_size=2,padding="same"))

	# print(model.output_shape)
	model.add(Flatten())
	# print(model.output_shape)
	model.add(Dense(100, activation='relu'))
	# print(model.output_shape)
	model.add(Dense(n_outputs, activation='softmax'))
	return(model)


def one_dim_cnn_model(n_kernel_1, n_outputs):

	input_shape = [1, 100]
	model = Sequential()
	model.add(Reshape((100, 1), input_shape=(input_shape)))
	model.add(Conv1D(filters=1, kernel_size=15, activation='relu', input_shape=input_shape,padding="same"))#org
	model.add(AveragePooling1D(pool_size=4))#org
	model.add(Conv1D(filters=8, kernel_size=n_kernel_1, activation='relu'))#org
	model.add(AveragePooling1D(pool_size=10)) #org

	model.add(Flatten())

	model.add(Dense(8, activation='relu'))
	model.add(Dense(n_outputs, activation='sigmoid'))
	print(model.output_shape)
	return (model)


def one_dim_cnn_2_layers_model(n_kernel_1, n_outputs):

	  input_shape = [1, 100]
	  model = Sequential()
	  model.add(Reshape((100, 1), input_shape=(input_shape)))

	  model.add(Conv1D(filters=8, kernel_size=16, activation='relu', input_shape=input_shape,padding="same",strides=1))
	  model.add(MaxPooling1D(pool_size=2))

	  model.add(Conv1D(filters=4, kernel_size=16, activation='relu', input_shape=input_shape,padding="same",strides=1))
	  model.add(MaxPooling1D(pool_size=2))

	  model.add(Flatten())

	  model.add(Dense(4, activation='relu'))
	  model.add(Dense(n_outputs, activation='sigmoid'))
	  print(model.output_shape)
	  return(model)


def one_dim_cnn_2_layers_reduced_model(n_kernel_1, n_outputs):
	input_shape = [1, 100]
	model = Sequential()
	model.add(Reshape((100, 1), input_shape=(input_shape)))

	model.add(Conv1D(filters=4, kernel_size=16, activation='relu', input_shape=input_shape, padding="same", strides=1))
	model.add(MaxPooling1D(pool_size=2))

	model.add(Conv1D(filters=2, kernel_size=16, activation='relu', input_shape=input_shape, padding="same", strides=1))
	model.add(MaxPooling1D(pool_size=2))

	model.add(Flatten())
	model.add(Dense(32, activation='relu'))
	model.add(Dense(16, activation='relu'))
	model.add(Dense(n_outputs, activation='sigmoid'))
	print(model.output_shape)
	return (model)


def one_dim_cnn_rnn_model(n_kernel_1,n_outputs):

	lstm_output_size = 256
	input_shape = [1, 100]
	model = Sequential()
	model.add(Reshape((1, 100), input_shape=(input_shape)))

	#model.add(Conv1D(filters=8, kernel_size=16, activation='relu', input_shape=input_shape,padding="same"))
	#model.add(MaxPooling1D(pool_size=2))

	#model.add(Conv1D(filters=4, kernel_size=16, activation='relu', input_shape=input_shape,padding="same"))
	#model.add(MaxPooling1D(pool_size=2))

	model.add(LSTM(lstm_output_size))
	#print("lstm",model.output_shape)

	model.add(Dense(64))
	model.add(Activation('relu'))

	model.add(Dense(32))
	model.add(Activation('relu'))

	model.add(Dense(1))
	model.add(Activation('sigmoid'))
	#print(model.output_shape)

	return(model)

'''
	# normalize filter values to 0-1 so we can visualize them
	filters, biases = model.layers[1].get_weights()
	f_min, f_max = filters.min(), filters.max()
	filters = (filters - f_min) / (f_max - f_min)
	# plot first few filters
	n_filters, ix = 6, 1
	for i in range(n_filters):
		# get the filter
		f = filters[:, :, i]
		# plot each channel separately
		for j in range(3):
			# specify subplot and turn of axis
			ax = plt.subplot(n_filters, 3, ix)
			ax.set_xticks([])
			ax.set_yticks([])
			# plot filter channel in grayscale
			plt.plot(f[:, j])
			ix += 1
	# show the figure
	#plt.show()
'''
