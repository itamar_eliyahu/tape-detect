import numpy as np
import matplotlib.pyplot as plt
import os
from Data_reader import Data_reader

def Dataste_Gen(Files_sets_list):

    for files_set in Files_sets_list:
        x_output_file_path = files_set[0]
        y_output_file_path = files_set[1]

        file_paths_list = files_set[2]
        DR = Data_reader(file_paths_list,x_output_file_path,y_output_file_path)
        x_with = DR.generate_dataset_for_class()
        file_paths_list = files_set[3]
        DR = Data_reader(file_paths_list,x_output_file_path,y_output_file_path)
        x_wo = DR.generate_dataset_for_class()

        DR.generate_2_classes_dataset(x_with,x_wo)
    return()

if __name__ == "__main__":

    file_set_1=['C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_regular_channels_2015_avg_norm_without_ma_17.npy',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015_avg_norm_without_ma_17.npy',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_with_tape_raw_16_avg_norm.txt',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_without_tape_raw_16_raw_15.txt']
    file_set_2 = [
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_watermark_channels_2015_avg_norm_without_ma_17.npy',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_watermark_channels_2015_avg_norm_without_ma_17.npy',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/watermark_channels_2015_with_tape_raw_16_avg_norm.txt',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/watermark_channels_2015_without_tape_raw_16_raw_15.txt']

    file_set_3=['C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_thread_channels_2015_avg_norm_without_ma_17.npy',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_thread_channels_2015_avg_norm_without_ma_17.npy',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/thread_channels_2015_with_tape_raw_16_raw.txt',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/thread_channels_2015_without_tape_raw_16_raw.txt']
    file_set_4 = [
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_next_to_thread_channels_2015_avg_norm_without_ma_17.npy',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_next_to_thread_channels_2015_avg_norm_without_ma_17.npy',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/next_to_thread_channels_2015_with_tape_raw_16_raw.txt',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/next_to_thread_channels_2015_without_tape_raw_16_raw.txt']

    file_set_5=[
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Datasets/X_regular_channels_2015_raw.npy',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Datasets/Y_regular_channels_2015_raw.npy',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/sw_records/regular_channels_2015_with_tape_raw_from_first_peak.txt',
                'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/sw_records/regular_channels_2015_without_tape_raw_from_first_peak.txt']

    Files_sets_list =  [ file_set_1]#[file_set_1,file_set_4]#[file_set_1,file_set_2,file_set_4]#
    #Files_sets_list = [file_set_2,file_set_3]#Thred + WM - without AVG_norm
    Dataste_Gen(Files_sets_list)

    '''# Creating a list of filenames
    file_1_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/thread_channels_2015_without_tape_raw_11.txt'
    file_2_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/thread_channels_2015_without_tape_raw_12.txt'
    output_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/thread_channels_2015_without_tape_raw_13.txt'

    filenames = [file_1_path, file_2_path]

    # Open file3 in write mode
    with open(output_file_path, 'w') as outfile:
        for names in filenames:
            with open(names) as infile:
                outfile.write(infile.read())
            outfile.write("\n")
        outfile.close()'''