import numpy as np
import matplotlib.pyplot as plt
import re

def plot_vs_dump_file():
    input_file_path =('C:/Projects/Python/Tape_detect/debug/temp.txt')
    text_file = open(input_file_path, "r", encoding='utf-8')
    height =  len(open(input_file_path, "r", encoding='utf-8').readlines())

    pixel_list = []
    for i in range(0, height):
        #print(i)
        #pixel = text_file.readline().split(' ')
        #pixel = pixel[0][-3:]

        #pixel = text_file.readline().split('\t')
        #pixel = pixel[3]
        pixel = text_file.readline()
        tt = re.split('] |,|\ |\t|\n', pixel)
        pixel=tt[3][:3]
        if (' ' in pixel):
            pixel = pixel.replace(' ', '')
        #if pixel[0] == '\t':
            #pixel =  pixel.replace('\t','')
        pixel_list.append(pixel)
        #print(pixel)
    pixel_list = np.array(pixel_list).astype(np.int)
    plt.plot(pixel_list.T)
    plt.show()
    return






if __name__ == "__main__":

    plot_vs_dump_file()




