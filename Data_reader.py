import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.signal import find_peaks
import re
from pandas import read_csv
from random import shuffle
from scipy.signal import find_peaks,peak_widths,peak_prominences

class Data_reader():
    flags = {'plot_sample':0,'save_dataframe_to_file':1,'extract_first_out_of_3_part':0,'extract_second_out_of_3_part':0,'extract_third_out_of_3_part':0,'plot_middle_samples':0,'filter_head_part':0,'add_rnd_noise':0,'avg_norm':0}

    def __init__(self, file_paths_list,x_output_file_path,y_output_file_path):
        self.file_paths_list = file_paths_list
        self.with_tape_flags = 0#with_tape_flags
        self.x_output_file_path = x_output_file_path
        self.y_output_file_path = y_output_file_path
        if ((self.flags['extract_first_out_of_3_part']) or (self.flags['extract_second_out_of_3_part']) or (self.flags['extract_third_out_of_3_part']) ):
            self.num_of_cols_out = 25
        else:
            self.num_of_cols_out = 115

        self.num_of_cols = 115
        self.class_col = []
        self.ind_to_remove =[]

    def add_random_noise(self):
        dataset_plus_rnd_noise = np.zeros_like(self.dataset)
        for ch in range(0, int(dataset_plus_rnd_noise.shape[0])):
            for sample in range(0, int(dataset_plus_rnd_noise.shape[1])):
                rnd_noise = np.random.randint(-10, 10)
                dataset_plus_rnd_noise[ch,sample] = rnd_noise + self.dataset[ch,sample].clip(0, 255)
        self.dataset =  np.concatenate((self.dataset, dataset_plus_rnd_noise))
        return()

    def pad_list(self,list, size, padding):
        return list + [padding] * abs((len(list) - size))

    def filter_head_part(self,dataframe):
        head_sum = []
        mid_sum=[]
        tail_sum = []
        indices_to_remove = []
        for ch in range(0,dataframe.shape[0]):
            head_sum.append(np.sum(dataframe[ch, 0:25]))
            #mid_sum.append(np.abs(np.sum(local_dataframe[ch, 50:75]) - np.sum(local_dataframe[ch, :25])) )
            tail_sum.append(np.sum(dataframe[ch, 50:75]))

            if (np.sum(dataframe[ch, 45:75]) +0> np.sum(dataframe[ch, 5:30])):
                #mid_sum.append(np.abs(np.sum(local_dataframe[ch, 50:75]) - np.sum(local_dataframe[ch, :25])) )
                indices_to_remove.append(ch)
            else:
                mid_sum.append(np.sum(dataframe[ch, 5:30]))

        dataframe = np.delete(dataframe,indices_to_remove,axis =0)
        print(len(indices_to_remove))
        print(dataframe.shape[0])

        return (dataframe)

    def load_files(self):
        file_idx = 0
        line_count = 0
        x_dataframe_3D = []
        #loacl_dataframe = np.array()
        for file_path in self.file_paths_list:
            #matrix = np.loadtxt(self.input_file_path, usecols=range(90))
            self.input_file_path = file_path
            with_tape_flag=self.with_tape_flags[file_idx]
            text_file = open(self.input_file_path, "r")
            height = len(open(self.input_file_path).readlines(  ))

            pixel_list = []
            for i in range(0, height):
                #print(i)
                pixels = text_file.readline().split(' ')
                pixels_clean = []
                if (len(pixels)< 70):
                    print("Wrong sample length")
                for j, value in enumerate(pixels):
                    if (value != ('') and value != ('\n')):
                        pixels_clean.append(value)

                if (len(pixels_clean) < self.num_of_cols):
                    pixels_clean=self.pad_list(pixels_clean,self.num_of_cols,0)

                for pixel in pixels_clean[:self.num_of_cols]:
                    pixel_list.append(int(pixel))



            if (file_idx==0):
                local_dataframe = np.array(pixel_list).reshape(height,self.num_of_cols)
                if ((with_tape_flag) and (self.flags['filter_head_part'])):
                    local_dataframe = self.filter_head_part(local_dataframe)
            else:
                loacl_dataframe_1 = np.array(pixel_list).reshape(height, self.num_of_cols)
                if ((with_tape_flag) and (self.flags['filter_head_part'])):
                    loacl_dataframe_1 = self.filter_head_part(loacl_dataframe_1)
                local_dataframe = np.concatenate((local_dataframe,loacl_dataframe_1),axis=0)


            if (with_tape_flag ==1):
                if (file_idx == 0):
                    local_class_col = np.ones((local_dataframe.shape[0],1))
                else:
                    local_class_col = np.ones((loacl_dataframe_1.shape[0], 1))
            else:
                if (file_idx == 0):
                    local_class_col = np.zeros((local_dataframe.shape[0],1))
                else:
                    local_class_col = np.zeros((loacl_dataframe_1.shape[0], 1))
                
                
            if (file_idx == 0):
                class_col = local_class_col
            else:
                class_col = np.concatenate((class_col, local_class_col))

            file_idx +=1

        #self.dataframe = np.hstack((self.dataframe, class_col))#, axis=1)
        #self.dataframe =np.concatenate((self.dataframe, class_col), axis=1)

        if (self.flags['extract_first_out_of_3_part']):
            '''
                #curr_peak =0
                #for i in range(2,30):
                    #if ((local_dataframe[ch,i] > local_dataframe[ch,i-1]+40)):# and (local_dataframe[ch,i-1] > local_dataframe[ch,i-2])):
                        #curr_peak=i
                #curr_peak = int(np.argmax(local_dataframe[ch, :30]))
                #if (curr_peak > 12):
                    #local_dataframe[ch, :25] = local_dataframe[ch, curr_peak - 5:curr_peak + 20]
                    
                    peak = int(np.argmax(local_dataframe[ch, :20]))
                    if(peak<5):
                        local_dataframe[ch,:25] = local_dataframe[ch, peak:peak+25]
                    else:
                        local_dataframe[ch, :25] = local_dataframe[ch, peak-5:peak + 20]
                    '''
            local_dataframe = local_dataframe[:, :25]
        elif (self.flags['extract_second_out_of_3_part']):
            original_data_frame = local_dataframe
            local_dataframe = local_dataframe[:,25:50]
            #local_dataframe = local_dataframe[:, 20:70]
            if (self.flags['plot_middle_samples']):
                local_dataframe_temp = original_data_frame[:, 25:50]
                for i in range (0,2):
                    fig, axarr = plt.subplots(1, 2)
                    line = original_data_frame[i, :].T
                    middle_part_line = local_dataframe_temp[i, :].T
                    axarr[0].plot(line)
                    axarr[0].set_title("Whole sample")
                    axarr[1].plot(middle_part_line)
                    axarr[1].set_title("Middle part")
                    plt.show()
        elif (self.flags['extract_third_out_of_3_part']):
            local_dataframe = local_dataframe[:, 50:75]

        x_dataframe_3D = np.zeros((local_dataframe.shape[0], 1, self.num_of_cols_out))
        for i in range(0, local_dataframe.shape[0]):
            x_dataframe_3D[i, :, :] = local_dataframe[i, :]

        if (self.flags['save_dataframe_to_file']):
            np.save(self.x_output_file_path, x_dataframe_3D)
            np.save(self.y_output_file_path, class_col)

            #np.savetxt('C:/Projects/Python/Tape_detect/mid_sampels_with_tape.txt',local_dataframe , fmt='%i')
        if (self.flags['plot_sample']):
            for n in range(32):
                ax = plt.subplot(4, 8, n + 1)
                t = local_dataframe[n]
                #t1 = t[:, :, 0]
                plt.plot(local_dataframe[n+64])
                # plt.show()
                plt.title(n)
                plt.axis('off')
            plt.show()

        return ()

    def txt_to_npy(self):
        X = np.genfromtxt(self.file_paths_list)
        #np.save(self.x_output_file_path, X)
        self.dataset = X[:,:-1]# copy without the las column which is the SN
        return()

    def iir_ma(self):
        self.dataset_100=np.zeros((self.dataset.shape[0],100))
        for i in range(0, self.dataset.shape[0]):
            k=0.3
            y_n_minus1=0
            for j in range(0, self.dataset.shape[1]-5):
                if j ==0:
                    y_n_minus1 = (self.dataset[i, j] * k)
                else:
                    y_n_minus1 = (self.dataset[i, j] * k) + (1-k)*y_n_minus1
                self.dataset_100[i, j] = y_n_minus1
        self.dataset = self.dataset_100
        return ()


    def avg_norm(self):
        self.dataset_100=np.zeros((self.dataset.shape[0],100))
        for i in range(0, self.dataset.shape[0]):
            # MA by window
            #N = 3
            #self.dataset_100[i, :] = np.convolve(self.dataset[i, :], np.ones(N) / N, mode='same')
            #N=4
            #self.dataset_100[i, :] = np.convolve(self.dataset[i, :100], np.ones(N) / N, mode='same')
            #self.dataset_100[i, :] = self.dataset_100[i, :].clip(0, 255).astype(np.uint8)

            #if ((self.dataset[i, :100]).std() < 8):
                #self.ind_to_remove.append(i)

            # Without MA by window
            self.dataset_100[i, :] = self.dataset[i, :100].clip(0, 255).astype(np.uint8)
            # mean subtraction whole channel
            self.dataset_100[i, :] = (self.dataset_100[i,:] - np.mean(self.dataset_100[i,:])).round().clip(0, 255)#.astype(np.uint8)

            # mean subtraction only banknote area
            #self.dataset_100[i, :] = (self.dataset_100[i,:] - np.mean(self.dataset_100[i,80:])).round().clip(0, 255)#.astype(np.uint8)

            # Convolution part
            #dary = self.dataset[i, :100]#.clip(0, 255).astype(np.uint8)
            #dary -= np.average(dary)
            #step = np.hstack((np.zeros(10),np.ones(70),np.zeros(20)))
            #dary_step = np.convolve(dary, step, mode='valid')
            #print(dary_step)
            '''
            shift_wo = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/start_end_ind_without.npy').astype(np.int)
            #shift_with = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/start_end_ind_with.npy').astype(np.int)
            shaft = np.mean(self.dataset_100[i, :shift_wo[i,0]]).astype(np.int)
            #note = np.mean((np.sort(self.dataset_100[i, 10:80]))[:35]).astype(np.int)
            note = np.median((self.dataset_100[i, 10:80])).astype(np.int)
            self.dataset_100[i, :]  = (self.dataset_100[i, :] - shaft).clip(0, 255)
            self.dataset_100[i, :] *= 128 / np.maximum(1, (note - shaft))
            self.dataset_100[i, :] = (self.dataset_100[i, :] - 128).clip(0, 255)
            #STD
            #self.dataset_100[i, :] /= np.std(self.dataset_100[i, :])#.round()
            #self.dataset_100[i, :] = (self.dataset_100[i, :])#.clip(0, 255).astype(np.uint8)
            '''
        self.dataset = self.dataset_100
        #self.dataset = np.delete(self.dataset_100,self.ind_to_remove,axis=0)
        #print("num of removed ch:",len(self.ind_to_remove))
        return()

    def axis_step_norm(self):
        temp_list =[]
        self.dataset_100=np.zeros((self.dataset.shape[0],100))
        for i in range(0, self.dataset.shape[0]):
            # MA by window
            N = 6
            self.dataset_100[i, :] = np.convolve(self.dataset[i, :], np.ones(N) / N, mode='valid')
            self.dataset_100[i, :] = self.dataset_100[i, :].clip(0, 255).astype(np.uint8)
            # Axis level mean subtraction
            self.dataset_100[i, :] = (self.dataset_100[i,:] - np.mean(self.dataset_100[i,85:])).clip(0, 255).astype(np.uint8)

            Banknote_level_minus_axis_level = (np.mean(self.dataset_100[i, :85])  - np.mean(self.dataset_100[i, 85:]) ).clip(0, 255).astype(np.uint8)
            if (Banknote_level_minus_axis_level ==0) :
                if (np.mean(self.dataset_100[i, :85])):
                    if (np.mean(self.dataset_100[i, :85]) == 0):
                        print('Fail' , i)
                    self.dataset_100[i, :] /= np.mean(self.dataset_100[i, :85])
                    self.dataset_100[i, :] *= 64
                    temp_list.append( self.dataset_100[i, :].clip(0, 255).astype(np.uint8))
                else:
                    print ('Banknote . Axis level Fail')

            elif(Banknote_level_minus_axis_level >20):
                self.dataset_100[i, :] /= Banknote_level_minus_axis_level
                self.dataset_100[i, :] *= 64
                temp_list.append( self.dataset_100[i, :].clip(0, 255).astype(np.uint8))

        self.dataset = np.array(temp_list)
        return()

    def z_score(self):
        self.dataset_100=np.zeros((self.dataset.shape[0],100))
        for i in range(0, self.dataset.shape[0]):
            # MA by window
            N = 6
            self.dataset_100[i, :] = np.convolve(self.dataset[i, :], np.ones(N) / N, mode='valid')
            self.dataset_100[i, :] = self.dataset_100[i, :].astype(np.uint8)
            # mean subtraction
            temp = (self.dataset_100[i,:] - np.mean(self.dataset_100[i,:]).astype(np.uint8)).clip(0, 255)
            self.dataset_100[i, :] = temp / (np.std(self.dataset_100[i, :]) - np.sqrt(np.mean(self.dataset_100[i,:]).astype(np.uint8)))

        self.dataset = self.dataset_100
        return()

    def generate_dataset_for_class(self):
        self.txt_to_npy()
        if self.flags['add_rnd_noise']:
            self.add_random_noise()
        if self.flags['avg_norm']:
            self.avg_norm()
            #self.iir_ma()
        return (self.dataset)

    def generate_2_classes_dataset(self,class1,class2):
        class1_data = (class1[:, :100])#.astype(np.uint8)
        class1_lable = np.ones((class1_data.shape[0], 1))
        class1_ds = np.hstack([class1_data, class1_lable])

        class2_data = (class2[:, :100])#.astype(np.uint8)
        class2_lable = np.zeros((class2_data.shape[0], 1))
        class2_ds = np.hstack([class2_data, class2_lable])

        ds = np.vstack([class1_ds, class2_ds])
        np.save(self.x_output_file_path,ds[:, :-1])
        np.save(self.y_output_file_path,ds[:, -1])
        return()





    def concatenate_samples_to_file(self):
        for file_path in self.file_paths_list:
            self.input_file_path = file_path
            for root, dirnames, filenames in os.walk(self.input_file_path):
                main_list = []
                for file in filenames:
                    self.single_file_path =os.path.join(root, file)
                    text_file = open(self.single_file_path, "r")
                    height = 14#len(open(self.single_file_path).readlines())
                    with open(self.x_output_file_path, "w") as output:
                        for i in range(0, height):
                            line = text_file.readline()#.split(' ')
                            main_list.append(line.strip())
        if (self.flags['save_dataframe_to_file']):
            with open(self.x_output_file_path, "w") as output:
                for line in main_list:
                    output.writelines(line)
                    output.write('\n')
            output.close()
        return ()


    def plot_raw_data_set(self):
        self.txt_to_npy()
        self.dataset = self.dataset [:,:100]
        self.dataset_raw = self.dataset.copy()
        self.plot_dataset()

    def plot_avg_norm_data_set(self):
        self.txt_to_npy()
        self.dataset_raw = self.dataset.copy()
        #self.add_random_noise()
        #self.avg_norm()
        self.iir_ma()
        #self.axis_step_norm()
        #self.z_score()
        self.dataset = self.dataset [:,:100]
        #np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/regular_channels_wo_tape_norm.npy',self.dataset)
        self.plot_dataset()
    def calc_fft(self):
        from scipy.fftpack import fft,fftshift,fftfreq
        from scipy import signal

        # How many time points are needed i,e., Sampling Frequency
        samplingFrequency =1;
        # At what intervals time points are sampled
        samplingInterval = 1 / samplingFrequency;
        # Begin time period of the signals
        beginTime = 0;
        # End time period of the signals
        endTime = 10;
        # Frequency of the signals
        signalFrequency = 4;
        # Time points
        time = np.linspace(0, 1, 100, endpoint=False)#np.arange(beginTime, endTime, samplingInterval);
        # Create two sine waves
        #amplitude =  signal.square(2 * np.pi * samplingFrequency * time)
        amplitude = np.concatenate((np.zeros(10),np.ones(80),np.zeros(10)))*128
        # Create subplot
        figure, axis = plt.subplots(2, 1)
        plt.subplots_adjust(hspace=1)
        # Time domain representation for sine wave 1
        axis[0].set_title('Square wave')
        axis[0].plot(time, amplitude)
        axis[0].set_xlabel('Time')
        axis[0].set_ylabel('Amplitude')

        # Frequency domain representation
        #fourierTransform = np.fft.fft(amplitude) / len(amplitude)  # Normalize amplitude
        fourierTransform = fftshift(fft(amplitude))
        fourierTransform = fourierTransform.real
        fourierTransform = fourierTransform[range(int(len(amplitude) / 2))]  # Exclude sampling frequency

        tpCount = len(amplitude)
        values = np.arange(int(tpCount / 2))
        timePeriod = tpCount / samplingFrequency
        frequencies = values / timePeriod
        # Frequency domain representation
        axis[1].set_title('FFT')
        axis[1].plot(frequencies, abs(fourierTransform))
        axis[1].set_xlabel('Frequency')
        axis[1].set_ylabel('Amplitude')
        plt.show()

        N = 100
        # sample spacing
        T = 1.0 / 800.0
        t = np.linspace(0.0,1, 100, endpoint=False)
        y= signal.square(2 * np.pi * 2 * t)
        yf = fft(y)
        xf = np.arange(0.0, 1.0 / (2.0 * T), N / 2)

        #fig, ax = plt.subplots()
        #ax.plot(xf, 2.0 / N * np.abs(yf[:N // 2]))
        #plt.show()

        # Number of samplepoints
        N = 100
        # sample spacing
        T = 1.0 / 800.0
        x = np.linspace(0.0, N * T, N)
        y = np.concatenate((np.zeros(10),np.ones(80),np.zeros(10)))*128
        yf = fft(y)
        xf = np.arange(yf.shape[0])#(0.0, 1.0 / (2.0 * T), N / 2)

        fig, ax = plt.subplots()
        ax.plot(xf, yf)#2.0 / N * np.abs(yf[:N // 2]))
        plt.show()

        self.data = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_regular_channels_2015_avg_norm_without_ma_13.npy')
        self.data = self.data[-5:,:]
        for i in range(0, self.data.shape[0]):
            fig, axarr = plt.subplots(1, 2, figsize=(8, 10))
            ii=i#fft(self.data[-i])
            #ii = np.sum(x * np.exp(-2j * np.pi * k * np.arange(n) / n))
            sp = fftshift(fft(self.data[ii]))
            freq = fftshift(fftfreq(self.data[ii].shape[-1]))
            #plt.plot(freq, sp.real, freq, sp.imag)
            plt.plot(freq, sp.real)

            axarr[0].plot(self.data[ii])
            axarr[0].set_title('Original')
            #axarr[1].plot(ii.real)
            axarr[1].plot(freq, sp.real, freq, sp.imag)
            axarr[1].set_title('FFT')

            plt.show()

    def plot_dataset(self):
        #samples_file = np.load(self.x_output_file_path)
        #samples_file =  samples_file.reshape(samples_file.shape[0],samples_file.shape[2])[:,:100]

        labels_file = np.load(self.y_output_file_path)#[:,:80]
        '''
        samples_file1 = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_temp.npy')#50
        samples_file2 = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_temp_all.npy')
        
        sig = samples_file2[50]
        bin_range = 52#(np.arange(257))
        hist, edges = np.histogram(sig, bins=bin_range, density=False)
        print("Stop")
        '''
        '''
        plt.plot(samples_file[1,:,:].T)
        plt.plot(samples_file[2, :, :].T)
        plt.plot(samples_file[6, :, :].T)
        plt.plot(samples_file[7, :, :].T)
        plt.plot(samples_file[8, :, :].T)
        plt.show()
        '''
        true_labele_indices = np.where(labels_file==1)
        false_labele_indices = np.where(labels_file == 0)
        #avg_with_tape = np.average(samples_file[true_labele_indices[0],:].reshape(true_labele_indices[0].shape[0],samples_file.shape[2]),axis=0)
        #avg_wo_tape = np.average(samples_file[false_labele_indices[0], :], axis=0)
        avg_with_tape = np.average(self.dataset,axis=0)

        #plt.plot(avg_wo_tape.T, color='blue',label='AVG of channels wo tape')
        plt.plot(avg_with_tape.T, color='red',label='Channels AVG')
        plt.legend(loc='upper right')
        plt.show()
        indices = np.random.permutation(self.dataset.shape[0])
        labels_arr = labels_file#[indices[:int(labels_file.shape[0] * 0.8)]]#, labels_file[indices[int(labels_file.shape[0] * 0.8):]]
        show_matrix_col_dim = 2
        show_matrix_row_dim = 10#13
        samples_offset = 0
        arr_indx_dbg=[]
        for i in range (0,55):
            fig, axarr = plt.subplots(show_matrix_row_dim, show_matrix_col_dim ,figsize=(8, 10))
            #plt.switch_backend('TkAgg')  # TkAgg (instead Qt4Agg)

            for row in range (0,show_matrix_row_dim):
                arr_indx = samples_offset+row+(show_matrix_row_dim*i)
                arr_indx_dbg.append(arr_indx)

                axarr[row,0].plot(self.dataset_raw[arr_indx, :].T)
                axarr[row, 1].plot(self.dataset[arr_indx, :].T)
                if (row ==0):
                    axarr[row,0].set_title("Raw" )
                    axarr[row, 1].set_title("Mean subtraction" )

            #mng = plt.get_current_fig_manager()
            #mng.resize(*mng.window.maxsize())
            #fig.set_aspect('auto')
            plt.show()
        return()

    def generate_shifted_narrow_tape_channels(self):
        #fig, axarr = plt.subplots(10,1)
        samples_file = np.load(self.x_output_file_path)#[:,:100]
        samples_file = samples_file[1000:-1,:,:100]
        #samples_file = samples_file.reshape(samples_file.shape[0], samples_file.shape[2])[:,:100]
        num_of_samples = int(samples_file.shape[0])#int(samples_file.shape[0]/10)
        generated_data = []#np.zeros((num_of_samples*6,100))
        fail_cntr = 0
        for k in range(0,num_of_samples): # Averaging every 10 channels which belong to same sampling
            #print('K',k)
            avg_with_tape =samples_file[k]# np.average(samples_file[k*10:(k+1)*10], axis=0)
            fall =0
            peak = 0
            peaks, _ = find_peaks(avg_with_tape[0,:100], distance=10,width=5, height=np.max(avg_with_tape[0,:100]))#, height=np.max(avg_with_tape[0,:100])
            if(peaks.shape[0]>0) :
                if ((peaks[-1]>40) and (peaks[-1]<60)):
                    peak = int(peaks[-1]-5)
                    fall = int(peaks[-1]+5)
            #plt.plot(avg_with_tape.T)
            #plt.title("Peak " +str(peak) + "  Fall "+str(fall) )
            #plt.show()

            '''
            for i in range (20,60):
                diff = (avg_with_tape[0,i+1] - avg_with_tape[0,i])
                if (( (avg_with_tape[0,i+1] - avg_with_tape[0,i]) >40) and (avg_with_tape[0,i] >50)):
                    peak =i
                    break
            for j in range (peak,peak+20):
                diff1 = (avg_with_tape[0, j] - avg_with_tape[0, j+1 ])
                diff2 = (avg_with_tape[0, j+2] - avg_with_tape[0, j ])
                if (( diff1> 0) and (diff1 >40) and ( diff2> 0) and (diff2 >40)):
                    fall = j+2
                    break
            if (peak<55):
                fall = peak+15
            '''

            if ((peak!=0) and (fall!=0) ):
                for j in range(0,3):#generate 3 right shifted channels from the AVG result
                    generated_sig_shift_right = avg_with_tape.copy()
                    if (j==0):
                        rand_shift = 10
                    elif (j == 1):
                        rand_shift = 5
                    else:
                        rand_shift = 10
                    pulse = avg_with_tape[:,peak:fall]
                    #generated_sig_shift_right[:,peak-5:fall+5]=avg_with_tape[:,fall+3:fall+(fall-peak)+13]#clean signal
                    generated_sig_shift_right[:, peak :fall ] =  avg_with_tape[:, peak-10 :peak]  # clean signal
                    generated_sig_shift_right[:, peak + rand_shift:(peak + rand_shift) + (fall - peak)] = pulse
                    generated_data.append(generated_sig_shift_right)

                for j in range(0,3):#generate 3 left shifted channels from the AVG result
                    generated_sig_shift_left = avg_with_tape.copy()
                    if (j == 0):
                        rand_shift_left = 10
                    elif (j == 1):
                        rand_shift_left = 20
                    else:
                        rand_shift_left = 15
                    pulse = avg_with_tape[:,peak:fall]
                    #generated_sig_shift_left[:,peak-5:fall+5]=avg_with_tape[:,fall+3:fall+(fall-peak)+13]#clean signal
                    generated_sig_shift_left[:, peak :fall ] = avg_with_tape[:, peak-10 :peak]  # clean signal
                    generated_sig_shift_left[:,peak-rand_shift_left:(peak-rand_shift_left)+(fall-peak)]=pulse
                    generated_data.append(generated_sig_shift_left)
            else:
                fail_cntr +=1
        print('Fails',fail_cntr)
        generated_data_arr = np.array(generated_data)
        generated_data_arr = generated_data_arr.reshape(generated_data_arr.shape[0],generated_data_arr.shape[2])
        np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/generated_regular_ch_13_2015_10mm_tape_avg_norm.npy' , generated_data_arr[:,:100])

    def generate_shifted_middle_channels(self,file_name):
        #fig, axarr = plt.subplots(10,1)
        samples_file = self.valid_data
        #samples_file = samples_file.reshape(samples_file.shape[0],1,samples_file.shape[1])
        #samples_file = samples_file.reshape(samples_file.shape[0], samples_file.shape[2])[:,:100]
        num_of_samples = int(samples_file.shape[0])#int(samples_file.shape[0]/10)
        generated_data = []#np.zeros((num_of_samples*6,100))
        fail_cntr = 0
        for k in range(0,num_of_samples): # Averaging every 10 channels which belong to same sampling
            avg_with_tape = np.zeros((1,110))
            avg_with_tape[0,:100] =samples_file[k,:]# np.average(samples_file[k*10:(k+1)*10], axis=0)
            #avg_with_tape = np.expand_dims(avg_with_tape,axis=0)
            peaks, _ = find_peaks(avg_with_tape[0,20:100], height=120,distance=3)#distance=5, height=np.max(avg_with_tape[0,20:100]))
            #results_half = peak_widths(avg_with_tape[0,20:100], peaks2, rel_height=0.5)
            #prominences = peak_prominences(avg_with_tape[0,20:100], peaks2)[0]
            #peak = peaks[0]+10
            #fall = peaks[0]-10

            fall =0
            peak = 0
            for i in range (30,60):
                diff = (avg_with_tape[0,i+1] - avg_with_tape[0,i])
                if (( (avg_with_tape[0,i+1] - avg_with_tape[0,i]) >80) and (avg_with_tape[0,i+1] >160)):
                    peak =i
                    break
            for j in range (peak+5,peak+20):
                diff1 = (avg_with_tape[0, j] - avg_with_tape[0, j+1 ])
                diff2 = (avg_with_tape[0, j+2] - avg_with_tape[0, j ])
                #if (( diff1> 0) and (diff1 >40) and ( diff2> 0) and (diff2 >40)):
                if (diff1 > 40):
                    fall = j+2
                    break

            if ((peak != 0) and (fall != 0)):
                for n in range (fall,80):# verify there is only one peak
                    if (avg_with_tape[0,n] > 60):
                        continue

                for l in range (30,peak):# verify there is only one peak
                    if (avg_with_tape[0,l] > 60):
                        continue

            if (peak>40):
                continue

            if ((peak!=0) and (fall!=0) and (peaks.shape[0] <3) ):
                for j in range(0,9):#generate 3 right shifted channels from the AVG result
                    generated_sig_shift_right = avg_with_tape.copy()
                    if (j==0):
                        rand_shift = 27
                    elif (j == 1):
                        rand_shift = 20
                    elif (j == 2):
                        rand_shift = 22
                    elif (j == 3):
                        rand_shift = 24
                    elif (j == 4):
                        rand_shift = 26
                    elif (j == 6):
                        rand_shift = 15
                    elif (j == 7):
                        rand_shift = 28
                    else:
                        rand_shift = 10
                    pulse = avg_with_tape[:,peak:fall]
                    generated_sig_shift_right[:,peak-5:fall+5]=avg_with_tape[:,fall+3:fall+(fall-peak)+13]#clean signal
                    generated_sig_shift_right[:, peak + rand_shift:(peak + rand_shift) + (fall - peak)] = pulse
                    generated_data.append(generated_sig_shift_right)
                    #if ((peak+fall)>12):
                        #pulse = avg_with_tape[:, [peak,peak+1,peak+2,fall-2,fall-1,fall]]
                        #pulse = avg_with_tape[:, peak:fall]
                        #generated_sig_shift_right[:, peak + rand_shift:(peak + rand_shift) + 6] = pulse
                        #generated_sig_shift_right[:, peak + rand_shift:(peak + rand_shift) + 6] = pulse
                        #generated_data.append(generated_sig_shift_right)

                for j in range(0,9):#generate 3 left shifted channels from the AVG result
                    generated_sig_shift_left = avg_with_tape.copy()
                    if (j == 0):
                        rand_shift_left = -15
                    elif (j == 1):
                        rand_shift_left = -10
                    elif (j == 2):
                        rand_shift_left = -8
                    elif (j == 3):
                        rand_shift_left = -6
                    elif (j == 4):
                        rand_shift_left = -4
                    elif (j == 5):
                        rand_shift_left = -12
                    elif (j == 6):
                        rand_shift_left = -14
                    elif (j == 7):
                        rand_shift_left = -13
                    else:
                        rand_shift_left = -5
                    pulse = avg_with_tape[:,peak:fall]
                    generated_sig_shift_left[:,peak-5:fall+5]=avg_with_tape[:,fall+3:fall+(fall-peak)+13]#clean signal
                    generated_sig_shift_left[:,peak-rand_shift_left:(peak-rand_shift_left)+(fall-peak)]=pulse
                    generated_data.append(generated_sig_shift_left)
                    #if ((peak+fall)>12):
                        #pulse = avg_with_tape[:, [peak,peak+1,peak+2,fall-2,fall-1,fall]]
                        #generated_sig_shift_left[:, peak + rand_shift_left:(peak + rand_shift_left) + 6] = pulse
                        #generated_data.append(generated_sig_shift_left)

            else:
                fail_cntr +=1
        print(fail_cntr)
        generated_data_arr = np.array(generated_data)
        generated_data_arr = generated_data_arr.reshape(generated_data_arr.shape[0],generated_data_arr.shape[2])
        file_name = self.x_output_file_path.split('/')[-1]
        np.save(str('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/generated/'+file_name),generated_data_arr[:,:100])
        #np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/ten_mm/generated/generated_watermark_channels_2015_10mm_tape_avg_norm_clean.npy' , generated_data_arr[:,:100])

    def running_wind(self):
        samples_file = np.load(self.x_output_file_path)[:, :, :100]
        samples_file = samples_file.reshape(samples_file.shape[0], samples_file.shape[2])
        std_cols = np.zeros((samples_file.shape[0],21))
        samples_file = np.hstack((samples_file, std_cols))
        offset = 20
        wind_width = 20
        for i in range (0,samples_file.shape[0]):
            #for j in range(0,20):
                #samples_file[i,100+j] = np.std(samples_file[i,offset+j:wind_width+offset+j])

            samples_file[i, -1] =  np.std(samples_file[i,:100])# np.max(samples_file[i,100:119])
        std_thresh = 60 # 25 for regular /
        above_std_thresh = np.where(samples_file[:, -1] > std_thresh)
        self.valid_data = samples_file[above_std_thresh[0]][:,:100]
        file_name = self.x_output_file_path.split('/')[-1]
        np.save(str('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/'+file_name),self.valid_data)
        self.generate_shifted_middle_channels(str('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/generated/std_thersh_clean/'+file_name))

if __name__ == "__main__":
    ''' 
    x1 = np.array([2, 4, 5, 7, 8, 9, 1])
    x2 = np.array([32, 54, 65, 77, 88, 99, 81])
    y1 = (x1 + x2)/2
    y1 = y1 - np.mean(y1)
    x1_1 = x1 - np.mean(x1)
    x2_1 = x2 - np.mean(x2)
    y2 = (x1 + x2) / 2
    plt.plot(y1)
    plt.plot(y2)
    plt.show()
    '''


    #file = np.genfromtxt('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/thread_channels_2015_without_tape_raw_GD_M4.txt')
    #np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_thread_2015_gd_wo_tape_GD_M4.npy',file[:,:100])
    #np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_thread_2015_gd_wo_tape_GD_M4.npy',np.zeros(file.shape[0]))

    #ff= np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/x_narrow.npy')
    #file_paths_list = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_with_tape_raw_from_first_peak_mid.txt'

    #file_paths_list = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/thread_channels_2015_without_tape_raw_16.txt'
    #file_paths_list = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/next_to_thread_channels_2015_with_tape_raw_16.txt'
    file_paths_list = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/thread_channels_2015_with_tape_raw_16.txt'
    #file_paths_list = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/watermark_channels_2015_without_tape_raw_16.txt'

    #file_paths_list ='C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/sw_records/watermark_channels_2005_without_tape_raw_16.txt'
    #x= np.genfromtxt(file_paths_list)
    #y=np.ones((x.shape[0]))
    #np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/x_narrow.npy',x)
    #np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/y_narrow.npy', y)
    #regular_channels_2015_without_tape_raw_from_first_peak
    x_output_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/thread_channels_2015_without_tape_raw_8.npy'
    y_output_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/Y_temp.npy'
    DR = Data_reader(file_paths_list,x_output_file_path,y_output_file_path)
    #DR.calc_fft()
    #DR.plot_avg_norm_data_set()
    DR.plot_raw_data_set()




    print("End")

