import numpy as np
from keras.utils import to_categorical
import tensorflow as tf
from keras.layers import Dropout, MaxPooling1D, Flatten, Input, Dense, MaxPool2D, Lambda, Bidirectional, Conv1D, \
    GlobalAveragePooling1D, Reshape
from keras.models import Model, Sequential
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping, LearningRateScheduler
import time
from sklearn.metrics import classification_report, confusion_matrix, precision_recall_curve, auc, accuracy_score
from keras.utils.vis_utils import plot_model
import pydot
import graphviz
import timeit
import matplotlib.pyplot as plt
from cnn_model import one_dim_cnn_rnn_model
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
import re
from scipy.special import expit as sigmoid

class Model_optimization():
    flags = {'remove_iligeal_file': 0, 'debug_mode': 1, 'save_network_outputs': 1, 'tape_detect_in_thread_channels': 0,
             'tape_detect_in_watermark_channels': 1, 'tape_detect_in_regular_channels': 0}

    def __init__(self, tf_lite_input_model_file_path, X_test, Y_test, keras_model_path):
        self.tf_lite_input_model_file_path = tf_lite_input_model_file_path

        self.Y_test = Y_test
        self.X_test = X_test
        self.X_test = self.X_test.reshape(self.X_test.shape[0],1,self.X_test.shape[1])

        self.keras_model_path = keras_model_path


    def load_keras_model_weights(self):
        n_kernel_1 = 16
        n_outputs = 1

        self.keras_model = one_dim_cnn_rnn_model(n_kernel_1, n_outputs)
        self.keras_model.load_weights(self.keras_model_path)
        self.kears_model_weights = []
        for layer in self.keras_model.layers:
            layer_w = layer.get_weights()
            self.kears_model_weights.append(layer_w)


        self.W = self.keras_model.layers[1].get_weights()[0]
        self.U = self.keras_model.layers[1].get_weights()[1]
        self.b = self.keras_model.layers[1].get_weights()[2]

        self.cls1_layer_weights = self.keras_model.layers[2].get_weights()[0]
        self.cls1_layer_biases  = self.keras_model.layers[2].get_weights()[1]

        self.cls2_layer_weights = self.keras_model.layers[4].get_weights()[0]
        self.cls2_layer_biases  = self.keras_model.layers[4].get_weights()[1]

        self.cls3_layer_weights = self.keras_model.layers[6].get_weights()[0]
        self.cls3_layer_biases  = self.keras_model.layers[6].get_weights()[1]



        extractor = Model(inputs=self.keras_model.inputs, outputs=[layer.output for layer in self.keras_model.layers])
        Layers = [layer.output for layer in self.keras_model.layers]
        self.features = extractor(self.X_test[:1,:,:])
        self.keras_model_lstm_input = self.features[0].numpy()
        self.keras_model_lstm_outputs = self.features[1].numpy()
        self.keras_model_cls1_outputs = self.features[2].numpy()
        self.keras_model_cls2_outputs = self.features[4].numpy()
        self.keras_model_cls3_outputs = self.features[6].numpy()






        return ()
    def calculate_network_outputs(self):

        # Extract LSTM weights
        units = int(int(self.keras_model.layers[1].trainable_weights[0].shape[1]) / 4)

        W_i = self.W[:, :units].T
        W_f = self.W[:, units: units * 2].T
        W_c = self.W[:, units * 2: units * 3].T
        W_o = self.W[:, units * 3:].T

        U_i = self.U[:, :units]
        U_f = self.U[:, units: units * 2]
        U_c = self.U[:, units * 2: units * 3]
        U_o = self.U[:, units * 3:]

        b_i = np.expand_dims(self.b[:units],axis=1)
        b_f = np.expand_dims(self.b[units: units * 2],axis=1)
        b_c = np.expand_dims(self.b[units * 2: units * 3].T,axis=1)
        b_o = np.expand_dims(self.b[units * 3:].T,axis=1)

        def forget_gate(x, h, Weights_hf, Weights_xf, Bias_xf, prev_cell_state):
            forget_hidden = np.dot(Weights_hf, h)  # + Bias_hf
            forget_eventx = np.dot(Weights_xf, x) + Bias_xf
            temp = forget_eventx  # (forget_hidden + forget_eventx)
            # temp = 1/(1 + np.exp(-temp))
            temp = sigmoid_x(temp)
            return np.multiply(temp, prev_cell_state)

        def input_gate(x, h, Weights_hi, Weights_xi, Bias_xi, Weights_hl, Weights_xl, Bias_xl):
            ignore_hidden = np.dot(Weights_hi, h)  # + Bias_hi
            ignore_eventx = np.dot(Weights_xi, x) + Bias_xi
            learn_hidden = np.dot(Weights_hl, h)  # + Bias_hl
            learn_eventx = np.dot(Weights_xl, x) + Bias_xl
            temp = ignore_eventx  # (ignore_eventx + ignore_hidden)
            # temp = 1/(1 + np.exp(-temp))
            temp = sigmoid_x(temp)

            # return np.multiply(temp, np.tanh(learn_eventx + learn_hidden))
            return np.multiply(temp, tanh_x(learn_eventx))

        def cell_state(forget_gate_output, input_gate_output):
            return forget_gate_output + input_gate_output

        def output_gate(x, h, Weights_ho, Weights_xo, Bias_xo, cell_state):
            out_hidden = np.dot(Weights_ho, h)  # + Bias_ho
            out_eventx = np.dot(Weights_xo, x) + Bias_xo
            temp = out_eventx  # (out_eventx + out_hidden)
            # temp = 1/(1 + np.exp(-temp))
            temp = sigmoid_x(temp)

            # return np.multiply(temp, np.tanh(cell_state))
            return np.multiply(temp, tanh_x(cell_state))


        def model_output(lstm_output, fc_Weight, fc_Bias):
            '''Takes the LSTM output and transforms it to our desired
            output size using a final, fully connected layer'''
            return np.dot(fc_Weight, lstm_output) + fc_Bias

        def tanh_x(x):
            return ((np.exp(x) - np.exp(-x)) / (np.exp(x) + np.exp(-x)))

        def sigmoid_x(x):
            return (1 / (1 + np.exp(-x)))

        #Calculate LSTM
        x_t = self.X_test[0, :, :].T
        h = np.zeros((units, 1))
        c = np.zeros((units, 1))
        f = forget_gate(x_t, h, U_f, W_f, b_f, c)
        i = input_gate(x_t, h, U_i, W_i, b_i, U_c, W_c, b_c)
        c = cell_state(f, i)
        lstm_output = output_gate(x_t, h, U_o, W_o, b_o, c)

        self.first_classifier_fc_layer = np.dot(lstm_output.T, self.cls1_layer_weights) + self.cls1_layer_biases
        self.first_classifier_fc_layer = np.maximum(self.first_classifier_fc_layer, 0)

        self.second_classifier_fc_layer = np.dot(self.first_classifier_fc_layer, self.cls2_layer_weights) + self.cls2_layer_biases
        self.second_classifier_fc_layer = np.maximum(self.second_classifier_fc_layer, 0)

        self.network_output = np.dot(self.second_classifier_fc_layer, self.cls3_layer_weights) + self.cls3_layer_biases


        return ()




if __name__ == "__main__":

    temp = np.array([1,2,3])
    rr=np.multiply(temp, temp)
    rr1 = np.dot(temp, temp)

    best_TFlite_model_file_path = "C:/Projects/Python/Tape_detect/best_model_rnn/Tape_detect_in_regular_channels/quantized_tflite_model.tflite"
    keras_model_file_path = "C:/Projects/Python/Tape_detect/best_model_rnn/Tape_detect_in_regular_channels/best_model_3.h5"
    X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_regular_channels_2015_avg_std_norm_4m.npy')
    Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015_avg_std_norm_4m.npy')

    X_test_for_inference = 'dd'
    Y_test_for_inference= 'dd'

    MO = Model_optimization(best_TFlite_model_file_path, X_test, Y_test, keras_model_file_path)
    MO.load_keras_model_weights()
    MO.calculate_network_outputs()
    #MO.model_inference()
    # MO.load_weights()
    # MO.calculate_and_plot_confusion_matrix()
    #MO.calculate_layers_outputs()
    # MO.calculate_network_outputs_for_batch_input()
    # MO.calculate_classification()
    # MO.inference()

print("Stop")