import numpy as np
import matplotlib.pyplot as plt
import os

'''
# low res data upper left corner
path = 'C:/Projects/Python/debug/debug_low_res_samples_100_side1_with_h_w.txt'
tile_sum_100 =[]
tile_std_100 =[]
with open(path, 'r') as path:
    lines = [line.strip().split(' ') for line in path]
for line in lines:
    h=int(line[-1])
    w=int(line[-2])
    img = np.array(line[:-2],dtype=np.int32)
    img=img.reshape(w,h)
    img=np.rot90(img)
    tile_sum_100.append(np.mean(img[:7,:7]))
    tile_std_100.append(np.std(img[:7, :7]))
    #plt.imshow(img[:7,:7],cmap='gray')
    #plt.show()

path = 'C:/Projects/Python/debug/debug_low_res_samples_200_side1_with_h_w.txt'
tile_sum_200 =[]
tile_std_200 =[]
with open(path, 'r') as path:
    lines = [line.strip().split(' ') for line in path]
for line in lines:
    h=int(line[-1])
    w=int(line[-2])
    img = np.array(line[:-2],dtype=np.int32)
    img=img.reshape(w,h)
    img = np.rot90(img)
    tile_sum_200.append(np.mean(img[:7,:7]))
    tile_std_200.append(np.std(img[:7, :7]))

path = 'C:/Projects/Python/debug/debug_low_res_samples_500_side1_with_h_w.txt'
tile_sum_500 =[]
tile_std_500 =[]
with open(path, 'r') as path:
    lines = [line.strip().split(' ') for line in path]
for line in lines:
    h=int(line[-1])
    w=int(line[-2])
    img = np.array(line[:-2],dtype=np.int32)
    img=img.reshape(w,h)
    img = np.rot90(img)
    tile_sum_500.append(np.mean(img[:7,:7]))
    tile_std_500.append(np.std(img[:7, :7]))


plt.scatter(tile_sum_500,tile_std_500,label='STD 500')
plt.scatter(tile_sum_200,tile_std_200,label='STD 200')

plt.scatter(np.arange(len(tile_sum_500)),tile_sum_500,label='STD 500')
plt.scatter(np.arange(len(tile_sum_200)),tile_sum_200,label='STD 200')
#plt.scatter(np.arange(len(tile_sum_100)),tile_sum_100,label='STD 100')
plt.legend()
plt.show()

X1 = np.array(tile_sum_500)
Y1= np.zeros_like(X1)
X2 = np.array(tile_sum_200)
Y2= np.ones_like(X2)

X = np.concatenate((np.expand_dims(X1,axis=1),np.expand_dims(X2,axis=1)),axis=0)
Y = np.concatenate((np.expand_dims(Y1,axis=1),np.expand_dims(Y2,axis=1)),axis=0)
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,Y, test_size=0.25, shuffle=True)

#logist_regression = LogisticRegression(C=1,solver='liblinear',warm_start=True,multi_class='ovr',penalty='l2',dual=False).fit(X_train, y_train)
logist_regression = LogisticRegression().fit(X_train, y_train)

print("logistic regression prediction accuracy is: ", logist_regression.score(X_test, y_test) * 100, "%")
'''


'''
# low res data
path = 'C:/Projects/Python/debug/debug_low_res_samples_100_side1.txt'
with open(path, 'r') as path:
    lines = [line.strip().split(' ') for line in path]
max_len = max(len(line) for line in lines)
data_100_low_res = np.zeros([len(lines), max_len])
for i, line in enumerate(lines):
    data_100_low_res[i, :len(line)] = line  
feat_100 = np.sum(data_100_low_res[0,:6])

path = 'C:/Projects/Python/debug/debug_low_res_samples_200_side1.txt'
with open(path, 'r') as path:
    lines = [line.strip().split(' ') for line in path]
max_len = max(len(line) for line in lines)
data_200_low_res = np.zeros([len(lines), max_len])
for i, line in enumerate(lines):
    data_200_low_res[i, :len(line)] = line
feat_200 = np.sum(data_200_low_res[0,:6])

path = 'C:/Projects/Python/debug/debug_low_res_samples_500_side1.txt'
with open(path, 'r') as path:
    lines = [line.strip().split(' ') for line in path]
max_len = max(len(line) for line in lines)
data_500_low_res = np.zeros([len(lines), max_len])
for i, line in enumerate(lines):
    data_500_low_res[i, :len(line)] = line
feat_500 = np.sum(data_500_low_res[0,:6])


plt.scatter(np.arange(feat_100.shape[0]),feat_100,label='STD 100')
plt.scatter(np.arange(feat_200.shape[0]),feat_200,label='STD 200')
#plt.scatter(np.arange(std_500.shape[0]),std_500)
plt.legend()
plt.show()
p=0
'''


# circle , H and triangle tiles
path = 'C:/Projects/Python/debug/debug_sn_rect_samples_100_side1.txt'
data_100= np.genfromtxt(path, delimiter=' ')
#for i in range(0,data_100.shape[0]):
#    img = data_100[i,:].reshape(10,50)
#    plt.imshow(img,cmap='gray')
#    plt.show()
path = 'C:/Projects/Python/debug/debug_sn_rect_samples_200_side1.txt'
data_200= np.genfromtxt(path, delimiter=' ')
#for i in range(0,data_100.shape[0]):
#    img = data_200[i,:].reshape(10,50)
#    plt.imshow(img,cmap='gray')
#    plt.show()
path = 'C:/Projects/Python/debug/debug_sn_rect_samples_500_side1.txt'
data_500= np.genfromtxt(path, delimiter=' ')
'''
std_100 = np.std(data_100,axis=1)
std_200 = np.std(data_200,axis=1)
std_500 = np.std(data_500,axis=1)


plt.scatter(np.arange(std_100.shape[0]),std_100,label='STD 100')
plt.scatter(np.arange(std_200.shape[0]),std_200,label='STD 200')
plt.scatter(np.arange(std_500.shape[0]),std_500,label='STD 500')
plt.legend()
plt.show()
p=0
'''
''''''

# 100 , 200 and 500 notes in low res
def adjust_gamma(image, gamma):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    return (table[image])
import matplotlib.patches as patches
from skimage.morphology import skeletonize

Gamma = 1
path2='C:/Projects/Python/debug/debug_file_100new_low_res.txt'
txt_file = np.loadtxt(path2,delimiter=',')
img = txt_file[:].reshape(70,96)
img = ((img - img.min()) / (img.max() - img.min())) * 255
img = img.astype(np.int32)
img1 = adjust_gamma(img, Gamma)

'''
from skimage.filters import prewitt_h, prewitt_v,sobel,threshold_otsu
from skimage.util import invert
img = np.rot90(img1)
threshold_value = threshold_otsu(img1)
img = img > threshold_value
img=img.astype(float)
img=invert(img)
skeleton = skeletonize(img)
'''

path3='C:/Projects/Python/debug/debug_file_200new_low_res.txt'
txt_file = np.loadtxt(path3,delimiter=',')
img = txt_file[:].reshape(72,97)
img = ((img - img.min()) / (img.max() - img.min())) * 255
img = img.astype(np.int32)
img2 = adjust_gamma(img, Gamma)

path4='C:/Projects/Python/debug/debug_file_500new_low_res.txt'
txt_file = np.loadtxt(path4,delimiter=',')
img = txt_file[:].reshape(74,96)
img = ((img - img.min()) / (img.max() - img.min())) * 255
img = img.astype(np.int32)
img3 = adjust_gamma(img, Gamma)

rect0 = patches.Rectangle((46,63),15,15,linewidth=1,edgecolor='r',facecolor='none')
fig, ax = plt.subplots(1, 3)
ax[0].imshow(np.rot90(img1),cmap='gray')
ax[0].add_patch(rect0)
ax[0].set_title('100 New')

rect1 = patches.Rectangle((46,63),15,15,linewidth=1,edgecolor='r',facecolor='none')
ax[1].imshow(np.rot90(img2),cmap='gray')
ax[1].add_patch(rect1)
ax[1].set_title('200 New')

rect2 = patches.Rectangle((46,63),15,15,linewidth=1,edgecolor='r',facecolor='none')
ax[2].imshow(np.rot90(img3),cmap='gray')
ax[2].add_patch(rect2)
ax[2].set_title('500 New')
plt.show()
print('')

'''
path1='C:/Projects/Python/debug/debug_file_10new_side1_low_res.txt'
txt_file = np.loadtxt(path1,delimiter=',')
img1 = txt_file[:].reshape(60,91)

path2='C:/Projects/Python/debug/debug_file_10new_side2_low_res.txt'
txt_file = np.loadtxt(path2,delimiter=',')
img2 = txt_file[:].reshape(61,93)

path3='C:/Projects/Python/debug/debug_file_10new_side3_low_res.txt'
txt_file = np.loadtxt(path3,delimiter=',')
img3 = txt_file[:].reshape(61,92)

path4='C:/Projects/Python/debug/debug_file_10new_side4_low_res.txt'
txt_file = np.loadtxt(path4,delimiter=',')
img4 = txt_file[:].reshape(61,91)

fig, ax = plt.subplots(2, 2)
ax[0,0].imshow(np.rot90(img1),cmap='gray')
ax[0,0].set_title('Side1')
ax[0,1].imshow(np.rot90(img2),cmap='gray')
ax[0,1].set_title('Side2')
ax[1,0].imshow(np.rot90(img3),cmap='gray')
ax[1,0].set_title('Side3')
ax[1,1].imshow(np.rot90(img4),cmap='gray')
ax[1,1].set_title('Side4')

plt.show()
'''


'''
path = 'C:/Projects/Python/debug/debug_sn_side1.txt'
path = 'C:/Projects/Python/debug/debug_sn_side2.txt'
path = 'C:/Projects/Python/debug/debug_sn_side3.txt'
path = 'C:/Projects/Python/debug/debug_sn_side4.txt'

path = 'C:/Projects/Python/debug/debug_low_res_samples_200_side1_with_h_w_110.txt'

txt_file = np.loadtxt(path,delimiter=',',dtype=np.int32)


img = txt_file[:].reshape(73,96)
#img = txt_file[:].reshape(100,200)

plt.imshow(img,cmap='gray')
plt.show()
os.remove(path)
'''

#path = 'C:/Projects/Python/debug/debug_file_side1.txt'
#path = 'C:/Projects/Python/debug/debug_file_side2.txt'
#path = 'C:/Projects/Python/debug/debug_file_side3.txt'
#path = 'C:/Projects/Python/debug/debug_file_side4.txt'
#path = 'C:/Projects/Python/debug/debug_file_thread_rect.txt'
path = 'C:/Projects/Python/debug/debug_low_res_samples_200_side1_with_h_w_110.txt'
#path = 'C:/Projects/Python/debug/rectSmall.txt'
txt_file=np.genfromtxt(path, delimiter=' ')
#txt_file = np.loadtxt(path,delimiter=' ')
img = txt_file[:].reshape(73,96)
img = np.rot90(img)
plt.imshow((img[:90,:70]),cmap='gray')
plt.show()
os.remove(path)
''''''

