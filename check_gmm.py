import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt
import matplotlib as mpl
import itertools
from scipy.stats import multivariate_normal as mvn

from sklearn import mixture

color_iter = itertools.cycle(['navy', 'c', 'cornflowerblue', 'gold',
                              'darkorange'])

def plot_results(X, Y_, means, covariances, index, title):
    splot = plt.subplot(2, 1, 1 + index)
    for i, (mean, covar, color) in enumerate(zip(means, covariances, color_iter)):
        v, w = linalg.eigh(covar)
        v = 2. * np.sqrt(2.) * np.sqrt(v)
        u = w[0] / linalg.norm(w[0])
        # as the DP will not use every component it has access to
        # unless it needs it, we shouldn't plot the redundant
        # components.
        if not np.any(Y_ == i):
            continue
        plt.scatter(X[Y_ == i, 0], X[Y_ == i, 1], .8, color=color)

        # Plot an ellipse to show the Gaussian component
        angle = np.arctan(u[1] / u[0])
        angle = 180. * angle / np.pi  # convert to degrees
        ell = mpl.patches.Ellipse(mean, v[0], v[1], 180. + angle, color=color)
        ell.set_clip_box(splot.bbox)
        ell.set_alpha(0.5)
        splot.add_artist(ell)

    plt.xlim(-9., 5.)
    plt.ylim(-3., 6.)
    plt.xticks(())
    plt.yticks(())
    plt.title(title)


X = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/regular_channels_2015_wo_tape_std_norm.npy')
X=X[:,:30]

X_mid = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/regular_channels_2015_with_beg_tape_std_norm.npy')
X_mid=X_mid[:,:30]

X_test=np.concatenate((X,X_mid),axis=0)


gmm = mixture.GaussianMixture(n_components=4, covariance_type='full').fit(X)
# fit models with 1-10 components
'''
models = [None for i in range(X.shape[0])]
for i in range(len(X)):
    models[i] = gmm(X[i]).fit(X)
'''

# compute the AIC and the BIC
AIC = gmm.aic(X)
BIC = gmm.bic(X)

fig = plt.figure(figsize=(5, 1.7))
fig.subplots_adjust(left=0.12, right=0.97,bottom=0.21, top=0.9, wspace=0.5)

# plot 3: posterior probabilities for each component
ax = fig.add_subplot(111)
x = np.linspace(-6, 6, X_test.shape[0])
p = gmm.predict_proba(X_test)
p = p[:, (1, 0, 2)]  # rearrange order so the plot looks better
p = p.cumsum(1).T

ax.fill_between(x, 0, p[0], color='gray', alpha=0.3)
ax.fill_between(x, p[0], p[1], color='gray', alpha=0.5)
ax.fill_between(x, p[1], 1, color='gray', alpha=0.7)
ax.set_xlim(-6, 6)
ax.set_ylim(0, 1)
ax.set_xlabel('$x$')
ax.set_ylabel(r'$p({\rm class}|x)$')

ax.text(-5, 0.3, 'class 1', rotation='vertical')
ax.text(0, 0.5, 'class 2', rotation='vertical')
ax.text(3, 0.3, 'class 3', rotation='vertical')

plt.show()
pred = np.zeros((X.shape[0],1))
# Fit a Gaussian mixture with EM using five components
gmm = mixture.GaussianMixture(n_components=3, covariance_type='full').fit(X)
ff=gmm.fit_predict(X)
print("Mean : ",gmm.means_)
print("Cove : ",gmm.covariances_)

for i in range(0,pred.shape[0]):
    pred[i,:]=gmm.predict(X[i,:].reshape(1, -1))
image2_labels= gmm.predict(X[5,:].reshape(1, -1))
probs = gmm.predict_proba(X)
#
original_shape = X[0,:].shape
segmented= image2_labels.reshape(original_shape[0],original_shape[1])
plt.imshow(segmented,cmap='gray')
plt.show()