import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":

    X = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_watermark_channels_clean.npy')#.astype(np.uint8)
    for i in range(0, X.shape[0]):
        bin_range = (np.arange(257))
        hist, edges = np.histogram(X[i, :], bins=bin_range, density=False)
        hist_min_zeros = hist[1:]
        second_max = np.argsort(hist_min_zeros)[-2] + 1
        X[i, :] = np.array(X[i, :].clip(0, 255) - second_max).clip(0, 255).astype(np.uint8)


    Din = data[76,:].clip(0,255)#astype(np.uint8)
    mn = np.mean(Din)
    bin_range = (np.arange(257))
    hist, edges = np.histogram(Din, bins=bin_range, density=False)
    hist_min_zeros = hist[1:]
    second_max = np.argsort(hist_min_zeros)[-2]+1
    new_sig =np.array(Din-second_max).clip(0, 255).astype(np.uint8)
    plt.plot(new_sig)
    plt.show()


    data_norm1 = (Din - np.mean(Din))
    #data_norm1 /= (np.std(data_norm1))

    #plt.plot(data_norm1)
    #data_norm2 = Din - np.max(Din)
    #data_norm2 /= (np.max(Din) - np.min(Din))
    #plt.plot(data_norm2)
    #plt.show()
    print("Stop")
    '''
    y = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/find_peak_casing.npy')
    s= np.sum(y,axis=0)
    x_wo = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_thread_channels_wo_tape.npy')
    x_wo = x_wo.reshape(x_wo.shape[0],x_wo.shape[2])[:,:100]
    '''

    ''' 
    #channels_wo_tape = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_thread_channels_wo_tape_14_&_15.npy')
    channels_wo_tape = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_2_thread_channels_wo_tape_minus_avg.npy')

    channels_wo_tape = channels_wo_tape.reshape(channels_wo_tape.shape[0],channels_wo_tape.shape[2])[:,:100]

    regular_channels_with_tape = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_regular_channels_with_tape_clean.npy')[:,:100].clip(0, 255).astype(np.uint8)
    for i in range(0 ,regular_channels_with_tape.shape[0] ):
        regular_channels_with_tape[i,:] =   (regular_channels_with_tape[i,:] - np.mean(regular_channels_with_tape[i,:])).clip(0, 255).astype(np.uint8)
    regular_channels_with_tape = np.around(regular_channels_with_tape/3).astype(np.uint32)

    #regular_channels_wo_tape = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/regular_channels_wo_tape.npy')[:,:100]
    #regular_channels_wo_tape = regular_channels_wo_tape.reshape(regular_channels_wo_tape.shape[0], regular_channels_wo_tape.shape[2])[:, :100]
    #regular_channels_wo_tape /= 3
    #regular_channels_wo_tape = np.around(regular_channels_wo_tape).astype(np.uint32)

    num_of_sets = 25
    output_arr_len = (channels_wo_tape.shape[0] * num_of_sets)
    averaged_channels_with_tape = np.zeros((output_arr_len,100))
    averaged_channels_wo_tape = np.zeros_like(channels_wo_tape)
    for j in range(0,num_of_sets):
        for i in range(0,channels_wo_tape.shape[0]):
            rnd_index = np.random.randint(0, regular_channels_with_tape.shape[0])
            #rnd_index2 = np.random.randint(0, regular_channels_wo_tape.shape[0])
            averaged_channels_with_tape[(j*channels_wo_tape.shape[0])+i,:] = channels_wo_tape[i,:] + regular_channels_with_tape[rnd_index,:]
            #averaged_channels_wo_tape[i, :] = channels_wo_tape[i, :] + regular_channels_wo_tape[rnd_index2, :]
            #print ((j*channels_wo_tape.shape[0])+i)
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_thread_channels_with_tape.npy',averaged_channels_with_tape)
    
    
    
    '''
    #x_with1= np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_thread_channels_with_tape.npy')
    x_with1= np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_thread_channels_with_tape_minus_avg.npy')
    #for i in range (0,1000):
    #    plt.plot(x_with1[i,:])
    #    plt.show()
    x_ones1 = np.ones((x_with1.shape[0],1))
    xwith1 = np.hstack([x_with1, x_ones1])

    #x_with2= np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_thread_channels_with_tape_2&3.npy')
    ##x_with = x_with.reshape(x_with.shape[0],x_with.shape[2])
    #x_ones2 = np.ones((x_with2.shape[0],1))
    #xwith2 = np.hstack([x_with2, x_ones2])

    #x_wo = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_thread_channels_wo_tape.npy')
    x_wo = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_thread_channels_wo_tape_minus_avg.npy')

    x_wo = x_wo.reshape(x_wo.shape[0],x_wo.shape[2])[:,:100]
    x_zeros = np.zeros((x_wo.shape[0],1))
    xwo = np.hstack([x_wo, x_zeros])

    ds = np.vstack([xwith1,xwo])
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_thread_channels_minus_avg.npy',ds[:,:-1])
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/Y_thread_channels_minus_avg.npy',ds[:,-1])
