from sklearn.metrics import classification_report, confusion_matrix, precision_recall_curve, auc, accuracy_score
import numpy as np
import matplotlib.pyplot as plt


'''
#Next to thread
X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Thread/X_close_to_thread_channels_2015.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Thread/Y_close_to_thread_channels_2015.npy')
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/network_output_next_to_thread_ch.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/next_to_thread_model_2015_dense1_by_dense2_wo_sigmoid.npy') 
'''

'''
#thread
'C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/network_output_thread_ch.npy'
X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Thread/X_thread_channels_2015.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Thread/Y_thread_channels_2015.npy')
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/network_output_thread_ch.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/thread_ch_model_2015_dense1_by_dense2_wo_sigmoid.npy')
'''

#thread channel only

#X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Thread/X_thread_channel_only_2015.npy')
#Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Thread/Y_thread_channel_only_2015.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/network_output_thread_ch.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/thread_channel_only_model_2015_dense1_by_dense2_wo_sigmoid.npy')


X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Thread/X_thread_channel_only_with_10mm_2015.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Thread/Y_thread_channel_only_with_10mm_2015.npy')
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/network_output_thread_channel_only.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/thread_channel_only_with_10mm_model_2015_dense1_by_dense2_wo_sigmoid.npy')



''''''
#Watermark
X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_watermark_channels_2015_std_avg_norm_4m.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_watermark_channels_2015_std_avg_norm_4m.npy')

part = 20
section_start = 20
section_end = section_start + part
#X_test = np.concatenate((X_test[section_start:section_end, :100],X_test[:part, :100]))
#Y_test = np.concatenate((Y_test[section_start:section_end], Y_test[:part]))

#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark/watermark_model_dense1_by_dense2_wo_sigmoid.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark/network_output_2015.npy')
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark/from_scratch_network_outputs.npy')





'''
#Regular all
X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_regular_channels_2015_avg_std_norm_4m.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015_avg_std_norm_4m.npy')

part =  40000
section_start = 457000
section_end = section_start + part
#X_test = np.concatenate((X_test[section_start:section_end, :100], X_test[:part, :100]))
#Y_test = np.concatenate((Y_test[section_start:section_end], Y_test[:part]))

Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/regular_model_dense1_by_dense2_wo_sigmoid.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/regular_all_2015_dense1_by_dense2_wo_sigmoid.npy')
'''



'''
#Regular CH13

X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Regular/X_regular_ch13_2015.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Regular/Y_regular_ch13_2015.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/regular_ch13_2015_dense1_by_dense2_wo_sigmoid.npy')
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/network_output_regular_ch13.npy')
'''

'''
#Regular CH4
X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Regular/X_regular_ch4_2015.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Regular/Y_regular_ch4_2015.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/regular_ch4_2015_dense1_by_dense2_wo_sigmoid.npy')
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/network_output_regular_ch4.npy')
'''



'''
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/regular_ch4_3cls_2015_dense1_by_dense2_wo_sigmoid.npy')
cls_0_indx = [np.where(Y_pred[:, -1] == 0)]
cls_1_indx = [np.where(Y_pred[:, -1] == 1)]
cls_2_indx = [np.where(Y_pred[:, -1] == 2)]

cls_0_data_arr = Y_pred[cls_0_indx][0]
cls_0_data_arr_max_col = np.argmax(cls_0_data_arr[:,:3],axis=1)
cls_1_data_arr = Y_pred[cls_1_indx][0]
cls_1_data_arr_max_col = np.argmax(cls_1_data_arr[:,:3],axis=1)
cls_2_data_arr = Y_pred[cls_2_indx][0]
cls_2_data_arr_max_col = np.argmax(cls_2_data_arr[:,:3],axis=1)


cls_0_data = cls_0_data_arr[:,0]
cls_1_data = cls_1_data_arr[:,1]
cls_2_data = cls_2_data_arr[:,2]


plt.plot(cls_0_data)
plt.plot(cls_1_data)
plt.plot(cls_2_data)
plt.show()
'''
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/network_output_regular_ch4_wo_narrow.npy')




#2005
'''
X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/X_watermark_2005.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Y_watermark_2005.npy')

part = 1000
#X_test =  np.concatenate((X_test[-part:, :100], X_test[:part, :100]))
#Y_test =  np.concatenate((Y_test[-part:], Y_test[:part]))
X_test = X_test[12000:14000, :100]
Y_test = Y_test[12000:14000]

Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark_2005/network_output_2005_2ksamp.npy')
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark_2005/tape_detect_in_watermark_2005_all.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark_2005/network_output_2005.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark_2005/watermark_model_2005_dense1_by_dense2_wo_sigmoid.npy')
'''

'''
X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/X_regular_ch4_2005.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Y_regular_ch4_2005.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular_2005/regular_model_2005_ch4_dense1_by_dense2_wo_sigmoid.npy')
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular_2005/network_output_ch4.npy')
'''

'''
X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/X_regular_ch13_2005.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Y_regular_ch13_2005.npy')
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular_2005/regular_model_2005_ch13_dense1_by_dense2_wo_sigmoid.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular_2005/network_output_ch13.npy')
'''

'''
X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/X_regular_2005.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Y_regular_2005.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular_2005/regular_model_2005_dense1_by_dense2_wo_sigmoid.npy')
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular_2005/network_output_regular.npy')

part = 10000
X_test = np.concatenate((X_test[-part:, :100], X_test[:part, :100]))
Y_test = np.concatenate((Y_test[-part:], Y_test[:part]))
'''


'''
# 2 thread channels averaged
X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/X_2_thread_channels_avg_2015.npy')
Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Y_2_thread_channels_avg_2015.npy')
#Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/thread_2ch_avg_model_2015_dense1_by_dense2_wo_sigmoid.npy')
Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/network_output_thread_2ch_avg.npy')

part = 1000
#X_test = np.concatenate((X_test[-part:, :100], X_test[:part, :100]))
#Y_test = np.concatenate((Y_test[-part:], Y_test[:part]))
'''

plt.plot(Y_pred[np.where(Y_pred[:, 1] == 0)])
plt.plot(Y_pred[np.where(Y_pred[:, 1] == 1)])
plt.show()


Y_pred_after_threshold = np.zeros_like(Y_pred[:, 0])
thresh =  0.2e14

tol = Y_pred.shape[0]
for i in range(0, tol):
    if (Y_pred[i, 0] >= thresh):
        Y_pred_after_threshold[i] = 1

matrix = confusion_matrix(Y_test, Y_pred_after_threshold)
tn, fp, fn, tp = matrix.ravel()

print("True negative", tn)
print('False negative', fn)
print(((fn) / (fn + tn)) * 100, '% false negative Fails', '\n')

print("True positive", tp)
print("False positive", fp)
print(((fp) / (fp + tp)) * 100, '% false positive Fails', '\n')

print("Stop")