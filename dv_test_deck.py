import numpy as np
import matplotlib.pyplot as plt
import os
from shutil import copyfile
import pandas as pd
from matplotlib.widgets import Cursor
import shutil

class DV_Test_deck():
    flags = {'plot_sample':0,'2015_2005':0}

    def __init__(self):
        if (self.flags['2015_2005']):
            self.scn_root_dir = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/DV_test_deck/tape_test_deck_2015_all_files/Tape/'
            self.scn_output_dir = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/DV_test_deck/tape_test_deck_2015_all_files/Tape/'
        else:
            self.scn_root_dir = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/DV_test_deck/tape_test_deck_2005/Tape/'
            self.scn_output_dir = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/DV_test_deck/tape_test_deck_2005/Tape/'


        if (self.flags['2015_2005']):
            self.banknotes_with_tape_values_fp = np.loadtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_sn_FP.txt', delimiter=' ', dtype=np.str)[:, 1]
            self.banknotes_with_tape_sn_fp = np.loadtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_sn_FP.txt', delimiter=' ', dtype=np.str)[:, 0]

            self.banknotes_with_tape_ch_fp = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_ch_FP.txt',delimiter=',', dtype=np.str)
            self.banknotes_with_tape_raw_data = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/debug_file_fp_tape.txt')

            self.banknotes_with_tape_sn = np.loadtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_sn.txt',delimiter='-', dtype=np.str)[:,0]
            self.banknotes_with_tape_values = np.loadtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_sn.txt', delimiter=' ', dtype=np.str)[:, 1]
            self.banknotes_with_tape_ch = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_ch.txt',delimiter=',', dtype=np.str)

        else:
            self.banknotes_with_tape_values_fp = np.loadtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_sn_FP_2005.txt', delimiter=' ',dtype=np.str)[:, 1]
            self.banknotes_with_tape_sn_fp = np.loadtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_sn_FP_2005.txt', delimiter=' ', dtype=np.str)[:, 0]

            self.banknotes_with_tape_ch_fp = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_ch_FP_2005.txt', delimiter=',',dtype=np.str)
            self.banknotes_with_tape_raw_data = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/debug_file_fp_tape_2005.txt')

            self.banknotes_with_tape_sn = np.loadtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_sn_2005.txt', delimiter='-',dtype=np.str)[:, 0]
            self.banknotes_with_tape_values = np.loadtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_sn_2005.txt', delimiter=' ',dtype=np.str)[:, 1]
            self.banknotes_with_tape_ch = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/dv_test_deck/banknotes_with_tape_ch_2005.txt', delimiter=',',dtype=np.str)


        self.SN_list = []
        self.TD_NUM_list = []
        for subdir, dirs, files in os.walk(self.scn_root_dir):
            for file in files:
                if file[:10] not in self.SN_list:
                    self.SN_list.append(file[:10])
                    path1=subdir.split("\\")
                    self.TD_NUM_list.append(path1[0].split('/')[-1])

    def rearange_DV_test_dect(self):
        SN_list = []
        for subdir, dirs, files in os.walk(self.scn_root_dir ):
            #for file in files:
                #if file[:10] not in SN_list:
                    #SN_list.append(file[:10])

            for sn in self.SN_list:
                if  os.path.exists(os.path.join(self.scn_output_dir,sn)):
                    continue
                else:
                    os.mkdir(os.path.join(self.scn_output_dir,sn))

            for file in files:
                copyfile(os.path.join(subdir,file), os.path.join(self.scn_output_dir,file[:10],file))

    def check_dv_test_deck_stat(self):
        self.stat_array = np.zeros((len(self.SN_list),3+4)).astype(np.str)# 4 for tape channels
        self.stat_array[:,0] =self.SN_list
        unique, counts = np.unique(self.banknotes_with_tape_sn, return_counts=True)
        for i in range (0,unique.shape[0]):
            sn = unique[i]
            num_of_notes_with_sn = counts[i]
            row = np.where(self.stat_array[:,0] == sn)[0]
            self.stat_array[row, 1] = num_of_notes_with_sn

            curr_sn_idx=np.where(self.banknotes_with_tape_sn == unique[i])
            curr_sn_detected_channels = self.banknotes_with_tape_ch[curr_sn_idx][:,3]
            unique_ch, counts_ch = np.unique(curr_sn_detected_channels, return_counts=True)
            unique_ch_s = (unique_ch).astype(np.str)
            if ((np.where(unique_ch_s=='Next to thread'))[0].shape[0]):
                self.stat_array[row, 3] = counts_ch[(np.where(unique_ch_s=='Next to thread'))]
            if ((np.where(unique_ch_s == 'Thread'))[0].shape[0]):
                self.stat_array[row, 4] = counts_ch[np.where(unique_ch_s == 'Thread')]
            if ((np.where(unique_ch_s == 'Regular'))[0].shape[0]):
                self.stat_array[row, 5] = counts_ch[np.where(unique_ch_s == 'Regular')]
            if ((np.where(unique_ch_s == 'WM'))[0].shape[0]):
                self.stat_array[row, 6] = counts_ch[np.where(unique_ch_s == 'WM')]

            DIR = (os.path.join(self.scn_output_dir,sn ))
            total_num_of_banknotes = (len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))]))
            self.stat_array[row, 2] = total_num_of_banknotes

        matrix = pd.DataFrame(data=self.stat_array, columns=["SN", "num of Detected", "Total Banknotes ", "Next to Thread", "Thread", "Regular", "Watermark"])
        matrix.to_csv('C:/Projects/Python/Tape_detect/debug/dv_test_deck/stat_8_180.csv')

    def check_dv_test_deck_stat_2005(self):
        self.stat_array = np.zeros((len(self.SN_list),3+4+2)).astype(np.str)# 4 for tape channels
        self.stat_array[:,0] =self.TD_NUM_list
        self.stat_array[:, 1] = self.SN_list
        unique, counts = np.unique(self.banknotes_with_tape_sn, return_counts=True)
        for i in range (0,unique.shape[0]):
            sn = unique[i]
            num_of_notes_with_sn = counts[i]
            row = np.where(self.stat_array[:,1] == sn)[0]
            self.stat_array[row, 2] = num_of_notes_with_sn

            curr_sn_idx=np.where(self.banknotes_with_tape_sn == unique[i])
            curr_sn_detected_channels = self.banknotes_with_tape_ch[curr_sn_idx][:,3]
            unique_ch, counts_ch = np.unique(curr_sn_detected_channels, return_counts=True)
            unique_ch_s = (unique_ch).astype(np.str)
            if ((np.where(unique_ch_s=='Next to thread'))[0].shape[0]):
                self.stat_array[row, 5] = counts_ch[(np.where(unique_ch_s=='Next to thread'))]
            if ((np.where(unique_ch_s == 'Thread'))[0].shape[0]):
                self.stat_array[row, 6] = counts_ch[np.where(unique_ch_s == 'Thread')]
            if ((np.where(unique_ch_s == 'Regular'))[0].shape[0]):
                self.stat_array[row, 7] = counts_ch[np.where(unique_ch_s == 'Regular')]
            if ((np.where(unique_ch_s == 'WM'))[0].shape[0]):
                self.stat_array[row, 8] = counts_ch[np.where(unique_ch_s == 'WM')]
            idx = int(np.where(np.array(self.SN_list) == sn)[0])
            DIR = (os.path.join(self.scn_output_dir,self.TD_NUM_list[idx]))
            #total_num_of_banknotes = (len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))]))
            total_num_of_banknotes =sum([len(files) for _, _, files in os.walk(DIR)])
            self.stat_array[row, 3] = total_num_of_banknotes
            self.stat_array[row, 4] =  ( num_of_notes_with_sn* 100 ) / total_num_of_banknotes
        matrix = pd.DataFrame(data=self.stat_array, columns=["Test Deck Num","SN", "num of Detected", "Total Banknotes ","%", "Next to Thread", "Thread", "Regular", "Watermark"])
        matrix.to_csv('C:/Projects/Python/Tape_detect/debug/dv_test_deck/stat_8_227_2005.csv')

    def search_and_copy_by_sn(self):
        self.scn_root_dir_2 = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_03_23_2021/Without_tape_scn'
        self.sn='WL20892118'
        self.scn_output_dir_2 = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/Temp/01_04/'
        os.mkdir(os.path.join(self.scn_output_dir_2, self.sn))
        for subdir, dirs, files in os.walk(self.scn_root_dir_2 ):
            for file in files:
                if file[:10] == self.sn:
                    copyfile(os.path.join(subdir,file), os.path.join(self.scn_output_dir_2, self.sn,file))

    def  move_to_folder_by_sn(self):
        self.scn_source_folder_path= 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/DV_test_deck/tape_test_deck_2015_all_files/Tape_set1/D6B1467243/'
        self.scn_dest_folder_path= 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/DV_test_deck/tape_test_deck_2015_all_files/Tape/70/D6B1467243/'

        for subdir, dirs, files in os.walk(self.scn_source_folder_path ):
            for file in files:
                if os.path.exists(os.path.join(self.scn_dest_folder_path,file)):
                    shutil.move(os.path.join(self.scn_source_folder_path , file),(os.path.join(self.scn_dest_folder_path,str(file[:-9]+'-'+str(np.random.randint(0,9999))+'.scn'))))
                else:
                    shutil.move(os.path.join(self.scn_source_folder_path, file), (os.path.join(self.scn_dest_folder_path,file)))
    def check_values_above_threshold_per_network(self):
        plot_his =0
        #thr = self.banknotes_with_tape_ch[self.banknotes_with_tape_ch[:,3]=='Thread']
        thr_indices_fp = np.where(self.banknotes_with_tape_ch_fp[:, 3] == 'Thread')[0]
        thr_values_fp = np.sort(self.banknotes_with_tape_values_fp[thr_indices_fp].astype(np.int64))
        thr_indices = np.where(self.banknotes_with_tape_ch[:, 3] == 'Thread')[0]
        thr_values = np.sort(self.banknotes_with_tape_values[thr_indices].astype(np.int64))

        n_thr_indices_fp = np.where(self.banknotes_with_tape_ch_fp[:, 3] == 'Next to thread')[0]
        n_thr_values_fp = np.sort(self.banknotes_with_tape_values_fp[n_thr_indices_fp].astype(np.int64))
        n_thr_indices = np.where(self.banknotes_with_tape_ch[:, 3] == 'Next to thread')[0]
        n_thr_values = np.sort(self.banknotes_with_tape_values[n_thr_indices].astype(np.int64))

        wm_indices_fp = np.where(self.banknotes_with_tape_ch_fp[:, 3] == 'WM')[0]
        wm_values_fp = np.sort(self.banknotes_with_tape_values_fp[wm_indices_fp].astype(np.int64))
        wm_indices = np.where(self.banknotes_with_tape_ch[:, 3] == 'WM')[0]
        wm_values = np.sort(self.banknotes_with_tape_values[wm_indices].astype(np.int64))

        reg_indices_fp = np.where(self.banknotes_with_tape_ch_fp[:, 3] == 'Regular')[0]
        reg_values_fp = np.sort(self.banknotes_with_tape_values_fp[reg_indices_fp].astype(np.int64))
        reg_indices = np.where(self.banknotes_with_tape_ch[:, 3] == 'Regular')[0]
        reg_values = np.sort(self.banknotes_with_tape_values[reg_indices].astype(np.int64))

        fig, ax = plt.subplots(2, 2, figsize=(8, 10))

        ax[0,0].plot(thr_values_fp,label='thr_indices_fp')
        ax[0,0].plot(thr_values, label='thr_indices')
        #ax[0,0].hist(thr_values_fp, label='Thread FP', bins=30, histtype='step', fill=None)
        #ax[0,0].hist(thr_values, label='Thread tape', bins=30, histtype='step', fill=None)
        ax[0,0].set_title('Thread')
        ax[0, 0].legend()

        ax[0,1].plot(n_thr_values_fp,label='n_thr_indices_fp')
        ax[0,1].plot(n_thr_values, label='n_thr_indices')
        #ax[0,1].hist(n_thr_values_fp, label='Next to thread FP', bins=30, histtype='step', fill=None)
        #ax[0,1].hist(n_thr_values, label='Next to thread tape', bins=30, histtype='step', fill=None)
        ax[0,1].set_title('Next To Thread')
        ax[0, 1].legend()

        xs = set(wm_values_fp)
        xs.intersection(wm_values)
        intersection_list = list(xs)
        '''
        h1,b1 = np.histogram(wm_values_fp)
        h2, b2 = np.histogram(wm_values)
        def histogram_intersection(h1, h2, bins):
            bins = np.diff(bins)
            sm = 0
            for i in range(len(bins)):
                sm += min(bins[i] * h1[i], bins[i] * h2[i])
            return sm
        ff= histogram_intersection(h1, h2, b1)
        '''
        #ax[1,0].hist(wm_values_fp,label='WM_fp',bins=200)
        #ax[1,0].hist(wm_values, label='WM',bins=200)
        #plt.hist(wm_values, bins=20, cumulative=True, label='CDF DATA', histtype='step')
        #plt.hist(wm_values_fp, bins=10, cumulative=True, label='CDF DATA', histtype='step', alpha=0.55, color='red')
        #ax[1,0].hist(wm_values_fp, label='WM FP', bins='auto', histtype='step', fill=None)
        #ax[1,0].hist(wm_values, label='WM tape', bins='auto', histtype='step', fill=None)
        ax[1,0].plot(wm_values_fp,label='WM_fp')
        ax[1,0].plot(wm_values, label='WM')
        ax[1,0].set_title('WM')
        ax[1, 0].legend()


        if (plot_his ==0):
            ax[1,1].plot(reg_values_fp,label='Regular FP')
            ax[1,1].plot(reg_values, label='Regular tape')
        else:
            ax[1,1].hist(reg_values_fp, label='Regular FP', bins='auto', histtype='step', fill=None)
            ax[1,1].hist(reg_values, label='Regular tape', bins='auto', histtype='step', fill=None)
        ax[1,1].set_title('Regular')
        ax[1, 1].legend()


        plt.show()
        return

    def plot_tape_channels(self):
        for i in range (0,self.banknotes_with_tape_raw_data.shape[0]):
            #Raw
            plt.plot(self.banknotes_with_tape_raw_data[i,:],label = 'Raw' )
            plt.legend()

            #kernel =np.concatenate((np.zeros(3),np.ones(10),np.zeros(3)))
            #gg = np.convolve(kernel,self.banknotes_with_tape_raw_data[i,:])
            #plt.plot(gg,label = 'Conv')
            #plt.title(str(self.banknotes_with_tape_ch_fp[i, 3]) + '- CH:'+str(self.banknotes_with_tape_ch_fp[i, 0])+' Output Value:' + str(self.banknotes_with_tape_values_fp[i]) + ' side ID:' +str(self.banknotes_with_tape_ch_fp[i,2])+' shaft:' +str(axis_level)+' step'+str(step))
            plt.title(str(self.banknotes_with_tape_ch_fp[i, 3]) + '- CH:'+str(self.banknotes_with_tape_ch_fp[i, 0])+' Output Value:' + str(self.banknotes_with_tape_values_fp[i]) + ' side ID:' +str(self.banknotes_with_tape_ch_fp[i,2]))

            #plt.savefig((os.path.join('C:/Projects/Python/Tape_detect/debug/dv_test_deck/FP_plots_DSP_209_2015',str(self.banknotes_with_tape_sn_fp[i]))))
            #plt.close()
            #matrix = pd.DataFrame(data=self.banknotes_with_tape_raw_data)
            #matrix.to_csv('C:/Projects/Python/Tape_detect/debug/dv_test_deck/with tape.csv')
            plt.show()

    def remove_FP_from_training_set(self):
        #self.banknotes_without_tape_dataset_path = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/without_m3_m4/'
        self.banknotes_without_tape_dataset_path = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/2005/Without tape SCN/'

        #self.fp_banknotes_without_path = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/FP/'
        self.fp_banknotes_without_path = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/FP_2005/'
        for subdir, dirs, files in os.walk(self.banknotes_without_tape_dataset_path):
            for file in files:
                if file[:15] in self.banknotes_with_tape_sn_fp:
                    if os.path.exists(os.path.join(self.fp_banknotes_without_path, file)):
                        os.remove(os.path.join(subdir, file))
                    else:
                        shutil.move(os.path.join(subdir, file), self.fp_banknotes_without_path)


def combine_folders():
    dest_fold=('C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/DV_test_deck/try/')
    for subdir, dirs, files in os.walk('C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/DV_test_deck/tape_test_deck_2015/'):
        for file in files:
            if os.path.exists(os.path.join(dest_fold, file)):
                shutil.copyfile(os.path.join(subdir, file), (os.path.join(dest_fold, file[:11]+str(np.random.randint(1000,9999))+'.scn')))
            else:
                shutil.copyfile(os.path.join(subdir, file), (os.path.join(dest_fold, file)))
   # divide the files to subfolders
    for i in range(0, 29):
        os.mkdir(os.path.join(dest_fold, str(i)))
    for subdir, dirs, files in os.walk(dest_fold):
        j =29
        ind = 0
        for file in files:
            shutil.move(os.path.join(subdir, file), (os.path.join(dest_fold,str(j), file)))
            if (ind <999):
                ind+=1
            else:
                ind=0
                j+=1

def check_tape_loaction():
    tape_location_file = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_thread_location_2005.txt',delimiter=',', dtype='unicode' )
    tape_location_file = tape_location_file.astype('object')
    tape_location_file[:,0] = tape_location_file[:,0].astype('str')
    tape_location_file[:,1] = tape_location_file[:,1].astype('int')
    tape_location_file[:,2] = tape_location_file[:,2].astype('str')
    tape_location_file[:,3] = tape_location_file[:,3].astype('int')
    tape_location_file[:,4] = tape_location_file[:,4].astype('str')
    tape_location_file[:,5] = tape_location_file[:,5].astype('int')
    tape_location_file[:,6] = tape_location_file[:,6].astype('str')

    side_0 = tape_location_file[tape_location_file[:, 5] == 0]
    side_2 = tape_location_file[tape_location_file[:,5]==2]
    side_0_2 = np.vstack((side_0,side_2))
    left_right_ch_0_2 = np.hstack((np.expand_dims(side_0_2[:,1],axis=1),np.expand_dims(side_0_2[:,3],axis=1)))
    left_right_ch_sort_0_2 = np.sort(left_right_ch_0_2)

    couple_7_8 = np.where(left_right_ch_sort_0_2[:,0]==7) and np.where(left_right_ch_sort_0_2[:,1]==8)
    couple_8_9 = np.where(left_right_ch_sort_0_2[:,0]==8) and np.where(left_right_ch_sort_0_2[:,1]==9)
    couple_9_10 = np.where(left_right_ch_sort_0_2[:,0]==9) and np.where(left_right_ch_sort_0_2[:,1]==10)
    couple_10_11 = np.where(left_right_ch_sort_0_2[:,0]==10) and np.where(left_right_ch_sort_0_2[:,1]==11)

    couple_7_8_count = couple_7_8[0].shape[0]
    couple_8_9_count = couple_8_9[0].shape[0]
    couple_9_10_count = couple_9_10[0].shape[0]
    couple_10_11_count  = couple_10_11[0].shape[0]

    side_1 = tape_location_file[tape_location_file[:, 5] == 1]
    side_3 = tape_location_file[tape_location_file[:,5]==3]
    side_1_3 = np.vstack((side_1,side_3))
    left_right_ch_1_3 = np.hstack((np.expand_dims(side_1_3[:,1],axis=1),np.expand_dims(side_1_3[:,3],axis=1)))
    left_right_ch_sort_1_3 = np.sort(left_right_ch_1_3)
    couple_5_6 = np.where(left_right_ch_sort_1_3[:,0]==5) and np.where(left_right_ch_sort_1_3[:,1]==6)
    couple_6_7 = np.where(left_right_ch_sort_1_3[:,0]==6) and np.where(left_right_ch_sort_1_3[:,1]==7)
    couple_7_8 = np.where(left_right_ch_sort_1_3[:,0]==7) and np.where(left_right_ch_sort_1_3[:,1]==8)
    couple_8_9 = np.where(left_right_ch_sort_1_3[:,0]==8) and np.where(left_right_ch_sort_1_3[:,1]==9)

    couple_5_6_count = couple_5_6[0].shape[0]
    couple_6_7_count = couple_6_7[0].shape[0]
    couple_7_8_count_2 = couple_7_8[0].shape[0]
    couple_8_9_count_2  = couple_8_9[0].shape[0]

    fig, ax = plt.subplots(1, 2)
    x_axis_0_2 = np.arange(left_right_ch_sort_0_2.shape[0])
    #ax[0].scatter(x_axis_0_2,left_right_ch_sort_0_2[:,0],facecolors='none', edgecolors='r',label='first ch')
    #ax[0].scatter(x_axis_0_2, left_right_ch_sort_0_2[:,1],facecolors='none', edgecolors='b',label='sec ch')

    ax[0].scatter(left_right_ch_sort_0_2[:,0], left_right_ch_sort_0_2[:, 1], facecolors='none', edgecolors='b', label='0 2')

    ax[0].legend()

    x_axis_1_3 = np.arange(left_right_ch_sort_1_3.shape[0])
    #ax[1].scatter(x_axis_1_3,left_right_ch_sort_1_3[:,0],facecolors='none', edgecolors='r',label='first ch')
    #ax[1].scatter(x_axis_1_3, left_right_ch_sort_1_3[:,1],facecolors='none', edgecolors='b',label='sec ch')
    ax[1].scatter(left_right_ch_sort_1_3[:, 0], left_right_ch_sort_1_3[:, 1], facecolors='none', edgecolors='r',label='1 3')
    ax[1].legend()
    plt.show()


if __name__ == "__main__":
    for subdir, dirs, files in os.walk('C:/Projects/Python\Dataset/Tape_and_thread/Dataset3/new_sensor_04_2021/with_tape_m3_m4'):
        for file in files:
            with open("FilesWithTape.txt", "a") as myfile:
                #myfile.write(str(subdir[71:]) +str (file)+'\n')
                myfile.write(str(file) + '\n')
                myfile.close()
    #combine_folders()
    #check_tape_loaction()
    DV_TD = DV_Test_deck()
    #DV_TD.rearange_DV_test_dect()
    #DV_TD.move_to_folder_by_sn()
    # #DV_TD.search_and_copy_by_sn()

    #DV_TD.check_dv_test_deck_stat()
    #DV_TD.check_dv_test_deck_stat_2005()
    #DV_TD.check_values_above_threshold_per_network()
    #DV_TD.plot_tape_channels()
    #DV_TD.remove_FP_from_training_set()

