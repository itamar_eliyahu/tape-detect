import numpy as np
import matplotlib.pyplot as plt


X_test1 = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_regular_channels3.npy')
Y_test1 = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/Y_regular_channels3.npy')
part = 100000
Y_test = np.concatenate((Y_test1[-part:], Y_test1[:part]))
X_test = np.concatenate((X_test1[-part:], X_test1[:part]))

Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors_cnn2/tape_detect_in_regular_channels_network_outputs_8192_4096.npy')

ypred_true=Y_pred[np.where(Y_pred[:, 1] == 1)]
ypred_fasle=Y_pred[np.where(Y_pred[:, 1] == 0)]
ypred_true_sort = np.sort(ypred_true[:,0])
ypred_fasle_sort = np.sort(ypred_fasle[:,0])[::-1]
plt.plot(ypred_true_sort,color ='blue')
plt.plot(ypred_fasle_sort,color ='orange')
plt.show()



#suspect_true_ind = np.array([np.argwhere(Y_pred==591782)[0,0],np.argwhere(Y_pred==544904)[0,0],np.argwhere(Y_pred==537570)[0,0],np.argwhere(Y_pred==536402)[0,0],np.argwhere(Y_pred==533949)[0,0]])
#suspect_false_ind = np.array([np.argwhere(Y_pred==591782)[0,0],np.argwhere(Y_pred==544904)[0,0],np.argwhere(Y_pred==537570)[0,0],np.argwhere(Y_pred==536402)[0,0],np.argwhere(Y_pred==533949)[0,0]])
suspect_false_ind = np.array([np.argwhere(Y_pred==478407)[0,0],np.argwhere(Y_pred==477681)[0,0],np.argwhere(Y_pred==472185)[0,0],np.argwhere(Y_pred==471350)[0,0],np.argwhere(Y_pred==469822)[0,0]])


#X_suspect_true_ind=X_test[[suspect_true_ind],:]
X_suspect_false_ind=X_test[[suspect_false_ind],:]

#plt.plot(X_suspect_true_ind[0,:,:].T)
plt.plot(X_suspect_false_ind[0,:,:].T)

