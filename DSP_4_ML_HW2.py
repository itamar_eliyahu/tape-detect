import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse.linalg import eigs,eigsh
from scipy.optimize import minimize
from scipy.linalg import norm
from scipy.optimize import minimize_scalar
from sklearn.metrics.pairwise import euclidean_distances
from scipy.linalg import eigh
from itertools import combinations
import networkx as nx
import random

def normalize(x):
    fac = abs(x).max()
    x_n = x / x.max()
    return fac, x_n


def calc_diffusion_map(X, kernel_alpha=0.05,n_eign=2):

    # calc diffusion matrix acording to lecture slides
    # n_eign - Number of eigen vectors to return. This is effectively the dimensions to keep in diffusion space

    alpha = kernel_alpha

    dists = euclidean_distances(X, X)
    dists_by_Kernel = np.exp(-dists ** 2 / alpha)

    w_ij = np.sum(dists_by_Kernel, axis=0)

    D = np.diag(1 / w_ij)
    M = np.matmul(D, dists_by_Kernel)# Diffusion matrix

    D_right = np.diag((w_ij) ** 0.5)
    D_left = np.diag((w_ij) ** -0.5)
    S = np.matmul(D_right, np.matmul(M, D_left))# S is the symmetrized version of Diffusion Matrix M

    #Diffusin map calculation
    n_eign = n_eign

    eigenValues, eigenVectors = eigh(S)
    idx = eigenValues.argsort()[::-1]
    eigenValues = eigenValues[idx]
    eigenVectors = eigenVectors[:, idx]

    diffusion_coordinates = np.matmul(D_left, eigenVectors)
    diffusion_coordinates = diffusion_coordinates[:, :n_eign]
    return diffusion_coordinates



if __name__ == "__main__":


    Task_1 = 1
    Task_2 = 0
    Task_3 = 0
    Task_4 = 0

    if Task_1:
        # Power method

        #x = np.ones((1000, 1))#begin with an initial nonzero approximation
        A = np.random.randn(1000,1000)
        A = (A + A.T) / 2  # Symetric
        n=1000
        eigen_val_list = []
        eigen_vect_list = []

        eigval_check, eigvec_check = np.linalg.eig(A)
        v = eigvec_check[:, 0]
        x = np.ones(1000)
        #x = np.random.rand(n)
        x_axis=[]
        y_axis = []
        for t in range(n):
            P = np.dot(A, x)
            Nt = np.linalg.norm(P, ord=2)
            x = P / Nt
            e1 = np.linalg.norm(x - v) / np.linalg.norm(v)
            e2 = np.linalg.norm(-1 * x - v) / np.linalg.norm(v)
            e = min(e1, e2)
            #print(e)
            #print(np.argmin((e1, e2)))
            x_axis.append(t)
            y_axis.append(e)

        plt.plot(x_axis, y_axis)
        plt.ylabel('Relative error')
        plt.xlabel('t')
        plt.title('Power Method')
        plt.show()



    if Task_2:
        # generate x
        x= np.hstack((np.ones(10), np.zeros(40)))

        #generate  2000 random points in a circular distribution
        length = np.sqrt(np.random.uniform(0, 1,2000))
        angle = np.pi * np.random.uniform(0, 2,2000)
        sigma=0
        noise = np.random.normal(0, sigma)
        x = (length * np.cos(angle)) +sigma
        y = (length * np.sin(angle)) +sigma

        plt.scatter(x,y)
        plt.show()

        X=np.concatenate((np.expand_dims(x,axis=0),(np.expand_dims(y,axis=0))),axis=0).T
        o=calc_diffusion_map(X)

    if Task_3:
        Xnew = np.load('C:\\Projects\\DSP_ML\\HW\\Xnew.npy')
        Xnew2 = np.load('C:\\Projects\\DSP_ML\\HW\\Xnew2.npy')
        x = np.load('C:\\Projects\\DSP_ML\\HW\\x.npy')
        MaxCutClustering_1 = np.load('C:\\Projects\\DSP_ML\\HW\\MaxCutClustering_1.npy')
        err = np.zeros(MaxCutClustering_1.shape[1])
        print(err.shape)
        for i in range(0, MaxCutClustering_1.shape[1]):
            unique, counts = np.unique(MaxCutClustering_1[:, i], return_counts=True)
            max_vote = np.argmax(counts)
            err[i] = (int(MaxCutClustering_1.shape[0]) - counts[max_vote]) / MaxCutClustering_1.shape[1]

        N =40
        P_arr =np.arange( 0.1,0.5,0.008)

        G = nx.Graph()
        V = list([v for v in range(40)])#Vertices
        G.add_nodes_from(V)

        for P in P_arr:
            #nodes from the same set = Probability P
            G.add_edges_from([(u, v) for u, v in combinations(V, 2) if (((u<20 and v<20) or (u>=20 and v>=20)) and(np.random.uniform(0, 1) < P))])
            # nodes from the different set = Probability 1-P
            G.add_edges_from([(u, v) for u, v in combinations(V, 2) if (((u > 20 and v <= 20) or (u < 20 and v >= 20)) and (np.random.uniform(0, 1) < 1-P))])

            #Plot Graph
            pos = nx.spring_layout(G)
            nx.draw_networkx_nodes(G, pos=pos, nodelist=V, node_color='r')
            nx.draw_networkx_edges(G, pos=pos)
            plt.show()

            adjacency_matrix  = np.asarray(nx.convert_matrix.to_numpy_matrix(G))
            plt.imshow(adjacency_matrix)
            plt.show()

    if Task_4:
        rnd_ang = ([(np.random.uniform(0, 1) * 2 * np.pi) for i in range (100)])
        teta = np .array(rnd_ang)
        teta_conj = teta.conj()
        H = np.dot(teta.T,teta_conj)




