import sys
import tensorflow as tf
from keras.models import Model
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from cnn_model import  one_dim_cnn_2_layers_reduced_model
import numpy as np
from jinja2 import  FileSystemLoader, Environment
import h5py


class Model_optimization():
    flags = {'remove_iligeal_file': 0, 'debug_mode': 1, 'tape_detect_in_regular_channels_2005': 0,'tape_detect_in_watermark_channels_2005': 0,
             'save_network_outputs_to_file': 1, 'save_weights_to_file':1, 'check_model_classification':0, 'calc_threshold': 0,'float_mode':0,
             'tape_detect_next_thread_channels':0, 'tape_detect_in_watermark_channels':0, 'tape_detect_in_regular_channels':0, 'tape_detect_in_thread_channels':1, }

    def __init__(self):

        if (self.flags['tape_detect_in_thread_channels']):
            self.network_output_path = 'C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/network_output_thread_channel_only.npy'

            self.keras_model_path = "C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_thread_channels/best_model_2015.h5"
            self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_thread_channels_2015_avg_norm_without_ma_16.npy')
            self.Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_thread_channels_2015_avg_norm_without_ma_16.npy')
            #self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_thread_channels_2015_avg_norm_with_aug.npy')
            #self.X_test = self.X_test.reshape(int(self.X_test.shape[0]),int(self.X_test.shape[2]))
            #self.Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_thread_channels_2015_avg_norm_with_aug.npy')



        elif (self.flags['tape_detect_next_thread_channels']):
            self.network_output_path = 'C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/network_output_next_to_thread_channel_only.npy'

            self.keras_model_path = "C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_thread_channels/best_model_next_to_thread_2015.h5"
            self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_next_to_thread_channels_2015_avg_norm_without_ma_16.npy')
            self.Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_next_to_thread_channels_2015_avg_norm_without_ma_16.npy')

        elif (self.flags['tape_detect_in_regular_channels']):
            # self.network_output_path = 'C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/network_output_regular_2015.npy'
            self.network_output_path = 'C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/from_scratch_network_outputs.npy'
            self.tf_lite_input_model_file_path = "C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_regular_channels/quantized_tflite_model.tflite"

            self.keras_model_path = "C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_regular_channels/best_model_2015.h5"
            self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_next_to_thread_channels_2015_avg_norm_without_ma_16.npy')
            self.Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_next_to_thread_channels_2015_avg_norm_without_ma_16.npy')
            #self.X_test = self.X_test[105000:, :]
            #self.Y_test = self.Y_test[105000:]
            #part = 40000
            #section_start = 457000
            #section_end = section_start + part
            # self.X_test = np.concatenate((self.X_test[section_start:section_end, :100], self.X_test[:part, :100]))
            # self.Y_test = np.concatenate((self.Y_test[section_start:section_end], self.Y_test[:part]))



        elif (self.flags['tape_detect_in_watermark_channels']):

            # self.network_output_path = 'C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark/network_output_2015.npy'
            self.network_output_path = 'C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark/from_scratch_network_outputs.npy'

            self.tf_lite_input_model_file_path = "C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_watermark_channels/quantized_tflite_model.tflite"
            self.keras_model_path = "C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_watermark_channels/best_model_2015.h5"

            self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_watermark_channels_2015_avg_norm_without_ma_16.npy')
            self.Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_watermark_channels_2015_avg_norm_without_ma_16.npy')
            #self.X_test = self.X_test[31500:, :]
            #self.Y_test = self.Y_test[31500:]

            part = 20
            section_start = 20
            section_end = section_start + part
            # self.X_test = np.concatenate((self.X_test[section_start:section_end, :100], self.X_test[:part, :100]))
            # self.Y_test = np.concatenate((self.Y_test[section_start:section_end], self.Y_test[:part]))

        elif (self.flags['tape_detect_in_regular_channels_2005']):
            self.network_output_path = 'C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular_2005/network_output_regular.npy'
            self.keras_model_path = "C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_regular_channels_2005/best_model_2005.h5"
            self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/X_regular_2005.npy')
            self.Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Y_regular_2005.npy')
            part = 10000
            self.X_test = np.concatenate((self.X_test[-part:, :100], self.X_test[:part, :100]))
            self.Y_test = np.concatenate((self.Y_test[-part:], self.Y_test[:part]))

        elif (self.flags['tape_detect_in_watermark_channels_2005']):
            self.network_output_path = 'C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark_2005/network_output_2005_2ksamp.npy'
            self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/X_watermark_2005.npy')
            self.Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Y_watermark_2005.npy')

        self.X_test = self.X_test.reshape(self.X_test.shape[0], 1, self.X_test.shape[1])
        self.Y_test = np.expand_dims(self.Y_test, axis=1)
        # self.X_test = self.X_test[:5,:,:]
        # self.Y_test = self.Y_test[:5]

        if (self.flags['calc_threshold']):
            self.second_classifier_fc_layer_arr = np.load(self.network_output_path)[:, 0]
            self.second_classifier_fc_layer_arr = np.expand_dims(self.second_classifier_fc_layer_arr, axis=1)
            self.plot_prediction()
            sys.exit(0)

    def load_weights(self):

        if (self.flags['tape_detect_in_watermark_channels']):

            self.c_array_file_base_path = 'C:/Projects/Python/Tape_detect/Weights_cnn2/tape_detect_in_watermark_channels/Watermark_'
            self.conv_1d_layer_1_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_watermark_channels/parameters_vector_for_section_13.npy')
            self.conv_1d_layer_1_weights = self.conv_1d_layer_1_weights.reshape(8, 16)  # .T
            self.conv_1d_layer_1_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_watermark_channels/parameters_vector_for_section_1.npy')
            self.save_weight_c_format(self.conv_1d_layer_1_weights, self.conv_1d_layer_1_biases, 'Filter1_weights')

            self.conv_1d_layer_2_weigths = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_watermark_channels/parameters_vector_for_section_15.npy')
            self.conv_1d_layer_2_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_watermark_channels/parameters_vector_for_section_2.npy')

            w_0 = self.conv_1d_layer_2_weigths[0, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_0, 'Filter2_weights_vector0')
            w_0 = np.expand_dims(w_0, axis=0)

            w_1 = self.conv_1d_layer_2_weigths[1, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_1, 'Filter2_weights_vector1')
            w_1 = np.expand_dims(w_1, axis=0)

            w_2 = self.conv_1d_layer_2_weigths[2, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_2, 'Filter2_weights_vector2')
            w_2 = np.expand_dims(w_2, axis=0)

            w_3 = self.conv_1d_layer_2_weigths[3, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_3, 'Filter2_weights_vector3')
            w_3 = np.expand_dims(w_3, axis=0)
            self.conv_1d_layer_2_weights = np.concatenate((w_0, w_1, w_2, w_3))

            np.savetxt('C:/Projects/Python/Tape_detect/Weights_cnn2/Tape_detect_in_watermark_channels/Regular_Filter2_biases.txt',self.conv_1d_layer_2_biases, fmt='%d')

            self.cls_dense1_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_watermark_channels/parameters_vector_for_section_10.npy')
            self.cls_dense1_weights = self.cls_dense1_weights.T
            self.cls_dense1_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_watermark_channels/parameters_vector_for_section_4.npy')
            self.save_weight_c_format_one_arr(self.cls_dense1_weights.T,'Filter3_weights')  # the transpose is just for the SW matrix multiplication
            np.savetxt('C:/Projects/Python/Tape_detect/Weights_cnn2/Tape_detect_in_watermark_channels/Regular_Filter3_biases.txt',self.cls_dense1_biases, fmt='%d')

            self.cls_dense2_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_watermark_channels/parameters_vector_for_section_11.npy')
            self.cls_dense2_weights = self.cls_dense2_weights.T  # reshape(num_of_channles,kernel_length)  # .T # even if it looks like transposed , the result is equale to the model results withou transpose it
            self.cls_dense2_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_watermark_channels/parameters_vector_for_section_5.npy')
            self.save_weight_c_format(self.cls_dense2_weights.T, self.cls_dense2_biases, 'Filter4_weights')

        elif (self.flags['tape_detect_in_regular_channels']):

            self.c_array_file_base_path = 'C:/Projects/Python/Tape_detect/Weights_cnn2/tape_detect_in_regular_channels/Regular_'
            self.conv_1d_layer_1_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_regular_channels/parameters_vector_for_section_13.npy')
            self.conv_1d_layer_1_weights = self.conv_1d_layer_1_weights.reshape(8, 16)  # .T
            self.conv_1d_layer_1_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_regular_channels/parameters_vector_for_section_1.npy')
            self.save_weight_c_format(self.conv_1d_layer_1_weights, self.conv_1d_layer_1_biases, 'Filter1_weights')

            self.conv_1d_layer_2_weigths = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_regular_channels/parameters_vector_for_section_15.npy')
            self.conv_1d_layer_2_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_regular_channels/parameters_vector_for_section_2.npy')

            w_0 = self.conv_1d_layer_2_weigths[0, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_0, 'Filter2_weights_vector0')
            w_0 = np.expand_dims(w_0, axis=0)

            w_1 = self.conv_1d_layer_2_weigths[1, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_1, 'Filter2_weights_vector1')
            w_1 = np.expand_dims(w_1, axis=0)

            w_2 = self.conv_1d_layer_2_weigths[2, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_2, 'Filter2_weights_vector2')
            w_2 = np.expand_dims(w_2, axis=0)

            w_3 = self.conv_1d_layer_2_weigths[3, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_3, 'Filter2_weights_vector3')
            w_3 = np.expand_dims(w_3, axis=0)
            self.conv_1d_layer_2_weights = np.concatenate((w_0, w_1, w_2, w_3))

            np.savetxt('C:/Projects/Python/Tape_detect/Weights_cnn2/tape_detect_in_regular_channels/Regular_Filter2_biases.txt',self.conv_1d_layer_2_biases, fmt='%d')

            self.cls_dense1_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_regular_channels/parameters_vector_for_section_10.npy')
            self.cls_dense1_weights = self.cls_dense1_weights.T
            self.cls_dense1_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_regular_channels/parameters_vector_for_section_4.npy')
            self.save_weight_c_format_one_arr(self.cls_dense1_weights.T,'Filter3_weights')  # the transpose is just for the SW matrix multiplication
            np.savetxt('C:/Projects/Python/Tape_detect/Weights_cnn2/tape_detect_in_regular_channels/Regular_Filter3_biases.txt',self.cls_dense1_biases, fmt='%d')

            self.cls_dense2_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_regular_channels/parameters_vector_for_section_11.npy')
            self.cls_dense2_weights = self.cls_dense2_weights.T  # reshape(num_of_channles,kernel_length)  # .T # even if it looks like transposed , the result is equale to the model results withou transpose it
            self.cls_dense2_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_regular_channels/parameters_vector_for_section_5.npy')
            self.save_weight_c_format(self.cls_dense2_weights.T, self.cls_dense2_biases, 'Filter4_weights')

        elif (self.flags['tape_detect_in_thread_channels']):

            self.c_array_file_base_path = 'C:/Projects/Python/Tape_detect/Weights_cnn2/tape_detect_in_thread_channels/Thread_'
            self.conv_1d_layer_1_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_13.npy')
            self.conv_1d_layer_1_weights = self.conv_1d_layer_1_weights.reshape(8, 16)
            self.conv_1d_layer_1_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_1.npy')
            self.save_weight_c_format(self.conv_1d_layer_1_weights, self.conv_1d_layer_1_biases, 'Filter1_weights')

            self.conv_1d_layer_2_weigths = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_15.npy')
            self.conv_1d_layer_2_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_2.npy')

            w_0 = self.conv_1d_layer_2_weigths[0, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_0, 'Filter2_weights_vector0')
            w_0 = np.expand_dims(w_0, axis=0)

            w_1 = self.conv_1d_layer_2_weigths[1, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_1, 'Filter2_weights_vector1')
            w_1 = np.expand_dims(w_1, axis=0)

            w_2 = self.conv_1d_layer_2_weigths[2, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_2, 'Filter2_weights_vector2')
            w_2 = np.expand_dims(w_2, axis=0)

            w_3 = self.conv_1d_layer_2_weigths[3, :, :].reshape(-1, 8).T
            self.save_weight_c_format_one_arr(w_3, 'Filter2_weights_vector3')
            w_3 = np.expand_dims(w_3, axis=0)
            self.conv_1d_layer_2_weights = np.concatenate((w_0, w_1, w_2, w_3))

            np.savetxt('C:/Projects/Python/Tape_detect/Weights_cnn2/tape_detect_in_thread_channels/Thread_Filter2_biases.txt',self.conv_1d_layer_2_biases, fmt='%d')

            self.cls_dense1_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_10.npy')
            self.cls_dense1_weights = self.cls_dense1_weights.T
            self.cls_dense1_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_4.npy')
            self.save_weight_c_format_one_arr(self.cls_dense1_weights.T,'Filter3_weights')  # the transpose is just for the SW matrix multiplication
            np.savetxt('C:/Projects/Python/Tape_detect/Weights_cnn2/tape_detect_in_thread_channels/Thread_Filter3_biases.txt',self.cls_dense1_biases, fmt='%d')

            self.cls_dense2_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_11.npy')
            self.cls_dense2_weights = self.cls_dense2_weights.T  # reshape(num_of_channles,kernel_length)  # .T # even if it looks like transposed , the result is equale to the model results withou transpose it
            self.cls_dense2_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_5.npy')
            self.save_weight_c_format(self.cls_dense2_weights.T, self.cls_dense2_biases, 'Filter4_weights')
        return ()

    def save_weight_c_format(self, weights_arr, biases_arr, text_file_name):
        biases_arr = np.expand_dims(biases_arr, axis=1)
        data_arr = np.hstack((weights_arr, biases_arr)).astype(int)
        rows, cols = data_arr.shape
        c_file_path = str(self.c_array_file_base_path + str(text_file_name) + '.txt')
        c_file = open(c_file_path, 'w')
        c_file.write('{')
        for i in range(0, rows):
            c_file.write('{')
            for j in range(0, cols):
                if (j != cols - 1):
                    c_file.write(str((data_arr[i, j])) + ',')
                else:
                    c_file.write(str(data_arr[i, j]))
            if (i != rows - 1):
                c_file.write('},' + '\n')
            else:
                c_file.write('}')
        c_file.write('};')
        c_file.close()
        return ()

    def save_weight_c_format_one_arr(self, weights_arr, text_file_name):
        # biases_arr = np.expand_dims(biases_arr, axis=1)
        data_arr = weights_arr  # np.hstack((weights_arr, biases_arr)).astype(int)
        rows, cols = data_arr.shape
        c_file_path = str(self.c_array_file_base_path + str(text_file_name) + '.txt')
        c_file = open(c_file_path, 'w')
        c_file.write('{')
        for i in range(0, rows):
            c_file.write('{')
            for j in range(0, cols):
                if (j != cols - 1):
                    c_file.write(str((data_arr[i, j])) + ',')
                else:
                    c_file.write(str(data_arr[i, j]))
            if (i != rows - 1):
                c_file.write('},' + '\n')
            else:
                c_file.write('}')
        c_file.write('};')
        c_file.close()
        return ()

    def convert_array_to_c_format(self):
        data_arr = self.X_test
        data_arr = data_arr.reshape(data_arr.shape[0], data_arr.shape[2])
        rows, cols = data_arr.shape
        c_file_path = 'C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_watermark_channels_network_inputs.txt'
        c_file = open(c_file_path, 'w')
        c_file.write('{')
        for i in range(0, rows):
            c_file.write('{')
            for j in range(0, cols):
                if (j != cols - 1):
                    c_file.write(str(('%d' % data_arr[i, j])) + ',')
                else:
                    c_file.write(str('%d' % data_arr[i, j]))
            if (i != rows - 1):
                c_file.write('}' + '\n')
            else:
                c_file.write('}')
        c_file.write('}')
        c_file.close()
        return ()

    '''
    def scale_param(self,array):
        min_wt = array.min()
        #print('min',min_wt)
        max_wt = array.max()
        div = (abs(min_wt) + abs(max_wt))/255
        #print('div',div)
        zero_point = -127
        z= zero_point - (min_wt/div)
        #print('max',max_wt)
        #print('z',z)
        int_bits = int(np.ceil(np.log2(max(abs(min_wt), abs(max_wt)))))
        #print(int_bits)
        frac_bits =9#7 - int_bits
        #return (np.round(array * (2 ** frac_bits)))
        return (np.round((array /div) +z))
    '''

    def scale_param3(self, array):
        return (np.round(array * (2 ** 8)))

    def scale_param2(self, array):
        min_wt = array.min()
        max_wt = array.max()
        int_bits = int(np.ceil(np.log2(max(abs(min_wt), abs(max_wt)))))  # 31.63 --> 5 bits
        frac_bits = 7 - int_bits  # remaining bits are fractional bits (1-bit for sign), 7-5 = 2 bits
        # floating point weights are scaled and rounded to [-128,127], which are used in
        # the fixed-point operations on the actual hardware (i.e., microcontroller)
        quant_weight = np.round(array * (2 ** frac_bits))  # 31 * 2^(2 bits frac) = 124
        # To quantify the impact of quantized weights, scale them back to
        # original range to run inference using quantized weights
        recovered_weight = quant_weight / (2 ** frac_bits)

        return (np.round(array * (2 ** 8)))#16 13
        #return (np.round(array * (2 ** frac_bits)))

    def scale_param(self, array):
        min_wt = array.min()
        max_wt = array.max()
        # find number of integer bits to represent this range
        int_bits = int(np.ceil(np.log2(max(abs(min_wt), abs(max_wt)))))  # 31.63 --> 5 bits
        frac_bits = 7 - int_bits  # remaining bits are fractional bits (1-bit for sign), 7-5 = 2 bits
        # floating point weights are scaled and rounded to [-128,127], which are used in
        # the fixed-point operations on the actual hardware (i.e., microcontroller)
        quant_weight = np.round(array * (2 ** frac_bits))  # 31 * 2^(2 bits frac) = 124
        # To quantify the impact of quantized weights, scale them back to
        # original range to run inference using quantized weights
        recovered_weight = quant_weight / (2 ** frac_bits)
        #print('quantization format: \t Q' + str(int_bits) + '.' + str(frac_bits))
        # print('Orginal weights:  ', array)
        # print('Quantized weights:', quant_weight)
        # print('Recovered weights:', recovered_weight)

        #return(np.round(array * (2 ** frac_bits)))
        return  (np.round(array * (2 ** 8)))#8

    def load_keras_model_weights(self):
        n_kernel_1 = 16
        n_outputs = 1

        if (self.flags['tape_detect_in_watermark_channels']):
            self.c_array_file_base_path = 'C:/Projects/Python/Tape_detect/Quant_weights/tape_detect_in_watermark_channels/Watermark_'
        if (self.flags['tape_detect_in_thread_channels']):
            self.c_array_file_base_path = 'C:/Projects/Python/Tape_detect/Quant_weights/tape_detect_in_thread_channels/Thread_'
        if (self.flags['tape_detect_in_regular_channels']):
            self.c_array_file_base_path = 'C:/Projects/Python/Tape_detect/Quant_weights/tape_detect_in_regular_channels/Regular_'

        self.keras_model = one_dim_cnn_2_layers_reduced_model(n_kernel_1, n_outputs)

        self.keras_model.load_weights(self.keras_model_path)
        self.kears_model_weights = []
        for layer in self.keras_model.layers:
            layer_w = layer.get_weights()
            self.kears_model_weights.append(layer_w)

        extractor = Model(inputs=self.keras_model.inputs, outputs=[layer.output for layer in self.keras_model.layers])
        Layers = [layer.output for layer in self.keras_model.layers]

        self.features = extractor(self.X_test[:, :, :])
        #input_temp =np.loadtxt('C:/Projects/Python/Tape_detect/temp.txt')
        #input_temp = input_temp[7:107]
        #input_temp = input_temp.reshape(1,1,100)
        #self.features = extractor(input_temp)
        self.keras_model_l1_outputs = self.features[1].numpy()
        #self.keras_model_l1_outputs = self.keras_model_l1_outputs[0, :, :].T

        self.keras_model_maxpool1_outputs = self.features[2].numpy()
        #self.keras_model_maxpool1_outputs = self.keras_model_maxpool1_outputs[0, :, :].T

        self.keras_model_l2_outputs = self.features[3].numpy()
        #self.keras_model_l2_outputs = self.keras_model_l2_outputs[0, :, :].T

        self.keras_model_maxpool2_outputs = self.features[4].numpy()
        #self.keras_model_maxpool2_outputs = self.keras_model_maxpool2_outputs[0, :, :].T

        self.keras_model_flatten_layer_outputs = self.features[5].numpy()

        self.keras_model_dense1_outputs = self.features[6].numpy()
        # np.save('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread_predict_from_keras_model.npy',self.keras_model_dense2_outputs)

        self.keras_model_dense2_outputs = self.features[7].numpy()
        # np.save('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread_predict_from_keras_model.npy',self.keras_model_dense2_outputs)

        self.keras_model_dense3_outputs = self.features[8].numpy()
        # np.save('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread_predict_from_keras_model.npy',self.keras_model_dense3_outputs)


        self.conv_1d_layer_1_weights = np.array(self.kears_model_weights[1][0])
        self.conv_1d_layer_1_weights = self.conv_1d_layer_1_weights.reshape(16, 4)
        self.conv_1d_layer_1_weights = self.conv_1d_layer_1_weights.T
        if (self.flags['float_mode'] ==0):
            self.conv_1d_layer_1_weights = self.scale_param2(self.conv_1d_layer_1_weights)
            self.conv_1d_layer_1_weights = self.conv_1d_layer_1_weights.astype(np.int32)

        self.conv_1d_layer_1_biases = np.array(self.kears_model_weights[1][1])
        if (self.flags['float_mode'] == 0):
            self.conv_1d_layer_1_biases = self.scale_param2(self.conv_1d_layer_1_biases)
            self.conv_1d_layer_1_biases = self.conv_1d_layer_1_biases.astype(np.int32)
        ## self.save_weight_c_format(self.conv_1d_layer_1_weights, self.conv_1d_layer_1_biases, 'Filter1_weights')

        self.conv_1d_layer_2_weights = np.array(self.kears_model_weights[3][0])
        conv_1d_layer_2_weights = np.array(self.kears_model_weights[3][0])
        self.conv_1d_layer_2_biases = np.array(self.kears_model_weights[3][1])
        if (self.flags['float_mode'] == 0):
            self.conv_1d_layer_2_biases = self.scale_param(self.conv_1d_layer_2_biases)*2**8
        ## np.savetxt(self.c_array_file_base_path +str('Filter2_biases.txt'),self.conv_1d_layer_2_biases, fmt='%d')

        w_0 = self.conv_1d_layer_2_weights[:, :, 0].reshape(-1, 4).T
        if (self.flags['float_mode'] == 0):
            w_0 = self.scale_param(w_0)
            w_0 = w_0.astype(np.int32)
        ## self.save_weight_c_format(w_0, np.zeros(w_0.shape[0]), 'Filter2_weights_vector0')
        w_0 = np.expand_dims(w_0, axis=0)

        w_1 = self.conv_1d_layer_2_weights[:, :, 1].reshape(-1, 4).T
        if (self.flags['float_mode'] == 0):
            w_1 = self.scale_param(w_1)
            w_1 = w_1.astype(np.int32)
        ## self.save_weight_c_format(w_1, np.zeros(w_1.shape[0]), 'Filter2_weights_vector1')
        w_1 = np.expand_dims(w_1, axis=0)


        self.conv_1d_layer_2_weights = np.concatenate((w_0, w_1))

        self.cls_dense1_weights = np.array(self.kears_model_weights[6][0])
        if (self.flags['float_mode'] == 0):
            self.cls_dense1_weights = self.scale_param(self.cls_dense1_weights)
            self.cls_dense1_weights = self.cls_dense1_weights.astype(np.int64)

        self.cls_dense1_biases = np.array(self.kears_model_weights[6][1])
        if (self.flags['float_mode'] == 0):
            self.cls_dense1_biases = self.scale_param(self.cls_dense1_biases)
            self.cls_dense1_biases = self.cls_dense1_biases.astype(np.int64)*2**8

        self.cls_dense2_weights = np.array(self.kears_model_weights[7][0])

        if (self.flags['check_model_classification'] == 0):
            if (self.flags['float_mode'] == 0):
                self.cls_dense2_weights = self.scale_param(self.cls_dense2_weights)
                self.cls_dense2_weights = self.cls_dense2_weights.astype(np.int64)

        self.cls_dense2_biases = np.array(self.kears_model_weights[7][1])

        if (self.flags['check_model_classification'] == 0):
            if (self.flags['float_mode'] == 0):
                self.cls_dense2_biases = self.scale_param(self.cls_dense2_biases)
                self.cls_dense2_biases = self.cls_dense2_biases.astype(np.int64)*2**8


        self.cls_dense3_weights = np.array(self.kears_model_weights[8][0])

        if (self.flags['check_model_classification'] == 0):
            if (self.flags['float_mode'] == 0):
                self.cls_dense3_weights = self.scale_param(self.cls_dense3_weights)
                self.cls_dense3_weights = self.cls_dense3_weights.astype(np.int64)

        self.cls_dense3_biases = np.array(self.kears_model_weights[8][1])

        if (self.flags['check_model_classification'] == 0):
            if (self.flags['float_mode'] == 0):
                self.cls_dense3_biases = self.scale_param(self.cls_dense3_biases)
                self.cls_dense3_biases = self.cls_dense3_biases.astype(np.int64)*2**8


        # 2015
        if (self.flags['save_weights_to_file']):
            if (self.flags['tape_detect_next_thread_channels']):
                self.save_header_file('TapeHeader_thread_reduced_cnn.h', w_0, w_1, int(8e12),'C:/Projects/Python/Tape_detect/Quant_weights/TapeHeader_thread_reduced_cnn.h')
            if (self.flags['tape_detect_in_thread_channels']):
                self.save_header_file('TapeHeader_thread_ch_id_13_reduced_cnn.h', w_0, w_1, int(1.1e13),'C:/Projects/Python/Tape_detect/Quant_weights/TapeHeader_thread_ch_id_13_reduced_cnn.h')
            if (self.flags['tape_detect_in_watermark_channels']):
                self.save_header_file('TapeHeader_watermark_reduced_cnn.h', w_0, w_1, int(9e12),'C:/Projects/Python/Tape_detect/Quant_weights/TapeHeader_Watermark_reduced_cnn.h')
            if (self.flags['tape_detect_in_regular_channels']):
                self.save_header_file('TapeHeader_regular_reduced_cnn.h', w_0, w_1, int(8e12),'C:/Projects/Python/Tape_detect/Quant_weights/TapeHeader_Regular_reduced_cnn.h')
            sys.exit(0)

        # calculate keras model outputs
        if (self.flags['check_model_classification']):
            self.X_test = self.X_test.reshape(self.X_test.shape[0], 100)
            self.second_classifier_fc_layer_arr = np.zeros((self.X_test.shape[0], 1))  # 2 classes
            # self.second_classifier_fc_layer_arr = np.zeros((self.X_test.shape[0], 3))# 3 classes
            for i in range(0, self.X_test.shape[0]):
                self.second_classifier_fc_layer = np.dot(self.keras_model_dense2_outputs[i],self.cls_dense3_weights.astype(np.float)) + self.cls_dense3_biases.astype(np.float)
                # fc_outouts_calc = np.dot(TFlite_model_first_conv1d_outputs, self.dense1_weights) + self.dense1_biases
                # self.second_classifier_fc_layer = self.second_classifier_fc_layer.astype(np.float)
                # np.savetxt('C:/Projects/Python/Tape_detect/test_vectors_cnn2/network_outputs_after_filter4.txt',self.second_classifier_fc_layer, fmt='%i')
                # self.second_classifier_fc_layer =  1 / (1 + np.exp(-self.second_classifier_fc_layer))#sigmoid
                self.second_classifier_fc_layer_arr[i, :] = self.second_classifier_fc_layer
            # self.Y_test = np.expand_dims(self.Y_test, axis=1)
            if (self.flags['tape_detect_next_thread_channels']):
                np.save('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/next_thread_channel_model_dense1_by_dense2_wo_sigmoid.npy',np.hstack((self.second_classifier_fc_layer_arr, self.Y_test)))
            if (self.flags['tape_detect_in_watermark_channels']):
                np.save('C:/Projects/Python/Tape_detect/test_vectors_cnn2/watermark/watermark_model_dense1_by_dense2_wo_sigmoid.npy',np.hstack((self.second_classifier_fc_layer_arr, self.Y_test)))
            if (self.flags['tape_detect_in_regular_channels']):
                np.save('C:/Projects/Python/Tape_detect/test_vectors_cnn2/regular/regular_model_dense1_by_dense2_wo_sigmoid.npy',np.hstack((self.second_classifier_fc_layer_arr, self.Y_test)))
            if (self.flags['tape_detect_in_thread_channels']):
                np.save('C:/Projects/Python/Tape_detect/test_vectors_cnn2/thread/thread_channel_model_dense1_by_dense2_wo_sigmoid.npy',np.hstack((self.second_classifier_fc_layer_arr, self.Y_test)))
            self.plot_prediction()
            sys.exit(0)
        return ()

    def plot_prediction(self):
        fig, ax = plt.subplots(1, 2, figsize=(8, 10))
        ax[0].plot(self.third_classifier_fc_layer_arr[np.where(self.Y_test[:, :] == 0)])
        ax[0].plot(self.third_classifier_fc_layer_arr[np.where(self.Y_test[:, :] == 1)])
        ax[0].set_title('Y_Pred')

        Y_pred_after_threshold = np.zeros_like(self.Y_test)
        thresh = 0.51

        tol = self.Y_test.shape[0]
        for i in range(0, tol):
            if (self.third_classifier_fc_layer_arr[i] >= thresh):
                Y_pred_after_threshold[i] = 1

        matrix = confusion_matrix(self.Y_test, Y_pred_after_threshold)
        tn, fp, fn, tp = matrix.ravel()
        labels = ['With_tape', 'Wo_tape']
        ax[1].matshow(matrix)
        # ax[1].set_title('Confusion matrix of the classifier')
        # fig.colorbar(matrix)
        # ax[1].set_xtick ([''] + labels)
        # ax.set_yticklabels([''] + labels)
        ax[1].set_xlabel('Predicted')
        ax[1].set_ylabel('True')
        plt.show()

        print("True negative", tn)
        print('False negative', fn)
        print(((fn) / (fn + tp)) * 100, '% false negative Fails', '\n')

        print("True positive", tp)
        print("False positive", fp)
        print(((fp) / (fp + tn)) * 100, '% false positive Fails', '\n')

    def save_header_file(self, template, w0, w1,  class_threshold, output_file_path):
        file_loader = FileSystemLoader('C:/Projects/Python/Tape_detect/header_template')
        env = Environment(loader=file_loader)
        # f = open('C:/Projects/Python/Tape_detect/Quant_weights/TapeHeader_test2.h', "w+")
        f = open(output_file_path, "w+")
        template_reg = env.get_template(template)
        conv_1d_layer_1_weights = self.conv_1d_layer_1_weights
        conv_1d_layer_1_biases = self.conv_1d_layer_1_biases
        conv_1d_layer_1_biases = np.expand_dims(conv_1d_layer_1_biases, axis=0)
        conv_1d_layer_1_w_b = np.hstack((conv_1d_layer_1_weights, conv_1d_layer_1_biases.T))

        w_0 = w0[0, :, :]
        w_0 = np.hstack((w_0, np.zeros((w_0.shape[0], 1)))).astype(np.int)
        w_1 = w1[0, :, :]
        w_1 = np.hstack((w_1, np.zeros((w_1.shape[0], 1)))).astype(np.int)

        conv_1d_layer_2_biases = self.conv_1d_layer_2_biases.astype(np.int)
        cls_dense1_weights = self.cls_dense1_weights
        cls_dense1_weights = cls_dense1_weights.T
        cls_dense1_biases = self.cls_dense1_biases
        cls_dense1_biases_for_sw = np.expand_dims(cls_dense1_biases, axis=0)
        dense1_w_b = np.hstack((cls_dense1_weights, cls_dense1_biases_for_sw.T)).astype(int)

        cls_dense2_weights = self.cls_dense2_weights
        cls_dense2_weights = cls_dense2_weights.T
        cls_dense2_biases = self.cls_dense2_biases
        cls_dense2_biases_for_sw = np.expand_dims(cls_dense2_biases, axis=1)
        dense2_w_b = np.hstack((cls_dense2_weights, cls_dense2_biases_for_sw)).astype(int)

        cls_dense3_weights = self.cls_dense3_weights
        cls_dense3_weights = cls_dense3_weights.T
        cls_dense3_biases = self.cls_dense3_biases
        cls_dense3_biases_for_sw = np.expand_dims(cls_dense3_biases, axis=0)
        dense3_w_b = np.hstack((cls_dense3_weights, cls_dense3_biases_for_sw)).astype(int)

        f.write(template_reg.render(filt1_w_b=conv_1d_layer_1_w_b, filt2_v1_w=w_0, filt2_v2_w=w_1,
                                    filt2_b=conv_1d_layer_2_biases, filt3_w_b=dense1_w_b,filt4_w_b=dense2_w_b,
                                    filt5_w_b=dense3_w_b[0, :], class_threshold=class_threshold))
        f.close()
        return ()

    def model_quantization(self):  # run in colab only
        # convert Keras model to 8bit weights and operations.
        # then save the model
        # https: // www.tensorflow.org / lite / performance / post_training_quantization
        # quantization to int8 done with calculate the dynamic range of the inputs and then estimate the quantization to all parameters in the model.
        # there are few option for quantization :
        # 1 - quantize the parameter to 8bit after training , then , while inference , recast the 8bit vakues to float32 and calculate the CNN with float32
        # 2 - quantize the parameters after training to int 8bit , then , while inference , continue calculate the CNN with 8bit integer
        # 3 - train Keras model , and then convert the model to Tensorflow lite with the required quantzation
        # Load keras model

        self.load_keras_model_weights()

        # convert model to Tensorflow lite format
        def representative_dataset_gen():
            for input_value in tf.data.Dataset.from_tensor_slices(self.X_test).batch(1).take(1000):
                yield [input_value]

        converter = tf.lite.TFLiteConverter.from_keras_model(self.keras_model)
        # converter.optimizations = [tf.lite.Optimize.OPTIMIZE_FOR_SIZE]
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
        converter.representative_dataset = representative_dataset_gen
        # converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
        # Set the input and output tensors to uint8 (APIs added in r2.3)
        converter.inference_input_type = tf.uint8
        converter.inference_output_type = tf.uint8
        tflite_quant_model = converter.convert()
        open('/content/drive/My Drive/Tape_detect/best_model/quantized_tflite_model.tflite', "wb").write(tflite_quant_model)  # Save the TF Lite model.
        return ()

    def model_inference(self):
        # batching tensorflow lite for inference
        # Load the TFLite model and allocate tensors.
        interpreter = tf.lite.Interpreter(model_path=self.tf_lite_input_model_file_path)
        # interpreter = tf.lite.Interpreter(model_path="C:/Projects/Python/Tape_detect/best_model_cnn2/Tape_detect_in_thread_channels/quantized_tflite_model_1_filt.tflite")

        interpreter.allocate_tensors()
        # Get input and output tensors.
        input_details = interpreter.get_input_details()
        # input_type = interpreter.get_input_details()[0]['dtype']
        output_details = interpreter.get_output_details()
        # output_type = interpreter.get_output_details()[0]['dtype']
        l1 = interpreter.get_tensor_details()
        if (self.flags['debug_mode']):
            all_layers_details = interpreter.get_tensor_details()

        input_shape = [self.X_test.shape[0], self.X_test.shape[1], self.X_test.shape[2]]
        interpreter.resize_tensor_input(input_details[0]['index'], input_shape)
        input_details_2 = interpreter.get_input_details()
        input_data = self.X_test.astype(np.uint8)
        interpreter.allocate_tensors()
        interpreter.set_tensor(input_details[0]['index'], input_data)
        interpreter.invoke()
        self.y_pred = interpreter.get_tensor(output_details[0]['index'])
        '''
        self.Layer_13_outputs = interpreter.get_tensor(13)
        self.Layer_13_outputs = self.Layer_13_outputs.reshape(-1, 8)
        self.Layer_22_outputs = interpreter.get_tensor(22)
        self.Layer_24_outputs = interpreter.get_tensor(24)
        self.Layer_25_outputs = interpreter.get_tensor(25)
        self.Layer_11_outputs = interpreter.get_tensor(1)
        self.Layer_0_outputs = interpreter.get_tensor(0)
        self.Layer_4_outputs = interpreter.get_tensor(4)
        self.Layer_22_outputs = interpreter.get_tensor(22)
        self.Layer_23_outputs = interpreter.get_tensor(23)
        self.Layer_24_outputs = interpreter.get_tensor(24)
        self.Layer_25_outputs_relu = interpreter.get_tensor(25)
        '''
        return ()

    def calculate_layers_outputs(self):
        # self.load_weights()
        self.load_keras_model_weights()

        self.first_layer_outputs_arr = np.zeros((self.X_test.shape[0], 4, 100))##.astype(np.int32)
        self.first_layer_inputs_arr = np.zeros((self.X_test.shape[0], 116))

        self.maxpool1_layer_outputs_arr = np.zeros((self.X_test.shape[0], 4, 50))

        self.fact_arr = np.zeros((self.X_test.shape[0], 1))
        self.second_layer_output_arr = np.zeros((self.X_test.shape[0], 2, 50))#.astype(np.int32)

        self.first_classifier_fc_layer_arr = np.zeros((self.X_test.shape[0], 32))  # .astype(np.int64)
        self.second_classifier_fc_layer_arr = np.zeros((self.X_test.shape[0], 16))#.astype(np.int64)
        self.third_classifier_fc_layer_arr = np.zeros((self.X_test.shape[0], 1))

        filt_1_max = 0
        filt_2_max = 0
        filt_3_max = 0

        div1 = int(2 ** 0)#16
        div2 = int(2 ** 0)
        div3 = int(2 ** 0)

        # self.X_test = self.Layer_0_outputs
        for j in range(0, self.X_test.shape[0]):
            input_vect = np.zeros((1, 115))#.astype(np.int32)
            input_vect[0, 7:107] = self.X_test[j, :100]  # 7:107 input_ch
            # input_vect[0, 7:107] = self.X_test[37817,:]
            # input_vect[0, :] = np.loadtxt('C:/Projects/Python/Tape_detect/debug/temp.txt')
            # plt.plot(input_vect.T)

            first_layer_outputs = np.zeros(100)#.astype(np.int32)

            for ch in range(0, 4):
                for stride in range(0, 100):
                    # print(stride,stride+16)
                    data_section = input_vect[0, stride:stride + 16]
                    # print(data_section)
                    cnv_plus_bias = ((np.sum(data_section * self.conv_1d_layer_1_weights[ch, :])) +self.conv_1d_layer_1_biases[ch])#.astype(np.int32)
                    first_layer_outputs[stride] = (np.maximum(0, cnv_plus_bias))#.astype(np.int32)  # After Relu
                assert (first_layer_outputs.max() < 2 ** 31), f'Wrong fixed point - Filter1 upper limit'
                assert (first_layer_outputs.min() > -2 ** 31), f'Wrong fixed point - Filter1 down limit'
                first_layer_outputs = first_layer_outputs / div1
                first_layer_outputs = (first_layer_outputs).astype(np.int32)

                self.first_layer_outputs_arr[j, ch, :] = first_layer_outputs
                #if (filt_1_max < (self.first_layer_outputs_arr[j, ch, :].max())):
                    #filt_1_max = self.first_layer_outputs_arr[j, ch, :].max()
                    #print("filt_1_max", filt_1_max)

            # np.savetxt('C:/Projects/Python/Tape_detect/test_vectors_cnn2/network_outputs_after_Filter1.txt',self.first_layer_outputs_arr[0,:, :], fmt='%i')
            # np.savetxt('C:/Projects/Python/Tape_detect/test_vectors_cnn2/network_input.txt', input_vect[0, 7:107], fmt='%i')

            maxpool1_layer_output = np.zeros((4, 50))#.astype(np.int32)
            for ch in range(0, 4):
                index = 0
                for i in range(0, 100, 2):
                    maxpool1_layer_output[ch, index] = np.maximum(self.first_layer_outputs_arr[j, ch, i],self.first_layer_outputs_arr[j, ch, i + 1])
                    index += 1

            # np.savetxt('C:/Projects/Python/Tape_detect/test_vectors_cnn2/network_outputs_after_maxpool1.txt',maxpool1_layer_output, fmt='%i')

            second_layer_input = maxpool1_layer_output  # TFlite_model_maxpool1_layer_outputs[0,:,:]#.reshape(-1,50)#maxpool1_layer_output
            num_of_filters_in_second_layer = 2
            sub_filt_reg = np.zeros(50)###.astype(np.int32)
            kernel_length = 16

            # sub filt by inp and then sum
            sub_filt_by_inp = np.zeros((4, 50))#.astype(np.int32)
            sub_filt_by_inp_total = np.zeros((2, 50))#.astype(np.int32)
            sub_filt_by_inp_sum_and_relu = np.zeros(50)#.astype(np.int32)
            for filt in range(0, num_of_filters_in_second_layer):
                # for sub_filt in range(0,8):# sub filters
                for inp in range(0, 4):  # inpt an dsub filt are the same
                    input_vector2 = np.zeros(65)#.astype(np.int32)
                    input_vector2[7:57] = second_layer_input[inp, :]  # 7:57
                    for stride in range(0, 50):
                        data_section = input_vector2[stride:stride + kernel_length]
                        # if(data_section.shape[0] != 16):
                        cnv_plus_bias = np.sum(data_section * self.conv_1d_layer_2_weights[filt, inp, :])
                        sub_filt_reg[stride] = cnv_plus_bias  # WO RELU
                    sub_filt_by_inp[inp, :] = sub_filt_reg

                sub_filt_by_inp_sum_and_relu = (np.sum(sub_filt_by_inp, axis=0)) + self.conv_1d_layer_2_biases[filt]
                assert (sub_filt_by_inp_sum_and_relu.max() < 2 ** 31), f'Wrong fixed point - Filter2 upper limit'
                assert (sub_filt_by_inp_sum_and_relu.min() > -2 ** 31), f'Wrong fixed point - Filter2 dowm limit'
                sub_filt_by_inp_sum_and_relu[sub_filt_by_inp_sum_and_relu < 0] = 0  # After Relu
                sub_filt_by_inp_total[filt, :] = sub_filt_by_inp_sum_and_relu / div2

            second_layer_output = sub_filt_by_inp_total#.astype(np.int32)
            self.second_layer_output_arr[j, :, :] = second_layer_output
            # np.savetxt('C:/Projects/Python/Tape_detect/test_vectors_cnn2/network_outputs_after_filter2.txt',second_layer_output, fmt='%i')

            maxpool2_layer_output = np.zeros((2, 25))#.astype(np.int64)

            for ch in range(0, 2):
                index = 0
                for i in range(0, 50, 2):
                    maxpool2_layer_output[ch, index] = np.maximum(second_layer_output[ch, i],second_layer_output[ch, i + 1])
                    index += 1

            # np.savetxt('C:/Projects/Python/Tape_detect/test_vectors_cnn2/network_outputs_after_maxpool2.txt',maxpool2_layer_output, fmt='%i')
            flatten = maxpool2_layer_output.T.flatten()
            # np.savetxt('C:/Projects/Python/Tape_detect/test_vectors_cnn2/network_outputs_after_flatten.txt',flatten, fmt='%i')

            # 32X50
            self.first_classifier_fc_layer = np.dot(flatten, self.cls_dense1_weights) + self.cls_dense1_biases
            # fc_outouts_calc = np.dot(np.around(np.average(TFlite_model_first_conv1d_16_outputs[j,:,:], axis=0)).astype(np.uint32), self.cls_dense1_weights) + self.cls_dense1_biases
            self.first_classifier_fc_layer = np.maximum(self.first_classifier_fc_layer, 0)  # Relu
            assert (self.first_classifier_fc_layer.max() < 2 ** 63), f'Wrong fixed point - Filter3 upper limit'
            assert (self.first_classifier_fc_layer.min() > -2 ** 63), f'Wrong fixed point - Filter3 dowm limit'
            self.first_classifier_fc_layer = (self.first_classifier_fc_layer / div3)#.astype(np.int64)
            self.first_classifier_fc_layer_arr[j, :] = self.first_classifier_fc_layer
            # np.savetxt('C:/Projects/Python/Tape_detect/test_vectors_cnn2/network_outputs_after_filter3.txt',self.first_classifier_fc_layer, fmt='%i')

            # 32X16
            #if (self.flags['float_mode']):
            self.second_classifier_fc_layer = np.dot(self.first_classifier_fc_layer, self.cls_dense2_weights) + self.cls_dense2_biases
            #else:
                #self.second_classifier_fc_layer = np.dot(self.first_classifier_fc_layer, self.cls_dense2_weights.astype(np.int64)) + self.cls_dense2_biases.astype(np.int64)

            self.second_classifier_fc_layer = np.maximum(self.second_classifier_fc_layer, 0)  # Relu


            assert (self.second_classifier_fc_layer.max() < 2 ** 63), f'Wrong fixed point - Filter4 upper limit'
            assert (self.second_classifier_fc_layer.min() > -2 ** 63), f'Wrong fixed point - Filter4 dowm limit'
            self.second_classifier_fc_layer = (self.second_classifier_fc_layer / div3)  # .astype(np.int64)
            self.second_classifier_fc_layer_arr[j,:] = (self.second_classifier_fc_layer)#.astype(np.int64)

            # 16X1
            #if (self.flags['float_mode']):
            self.third_classifier_fc_layer = np.dot(self.second_classifier_fc_layer, self.cls_dense3_weights) + self.cls_dense3_biases
            #else:
                #self.second_classifier_fc_layer = np.dot(self.first_classifier_fc_layer, self.cls_dense2_weights.astype(np.int64)) + self.cls_dense2_biases.astype(np.int64)

            assert (self.third_classifier_fc_layer.max() < 2 ** 63), f'Wrong fixed point - Filter4 upper limit'
            assert (self.third_classifier_fc_layer.min() > -2 ** 63), f'Wrong fixed point - Filter4 dowm limit'
            self.third_classifier_fc_layer_arr[j] = (self.third_classifier_fc_layer)#.astype(np.int64)


        if (self.flags['save_network_outputs_to_file']):
            np.save(self.network_output_path, np.hstack((self.third_classifier_fc_layer_arr, self.Y_test)))
            self.plot_prediction()

    def calculate_network_outputs_for_batch_input(self):
        # self.X_test = self.X_test[:10, :, :]
        network_outoputs_results = np.zeros((self.X_test.shape[0], 16))
        second_layer_outputs = np.zeros((16, 10))
        for i in range(0, self.X_test.shape[0]):
            for ch in range(0, 16):
                for stride in range(0, 10):
                    data_section = self.X_test[i, :, stride:stride + 16]
                    cnv_plus_bias = np.sum(data_section * self.conv_1d_layer_2_weights[ch]) + \
                                    self.conv_1d_layer_2_biases[ch]
                    # cnv_plus_bias = np.dot(data_section ,self.conv_1d_layer_2_weights[ch]) + self.conv_1d_layer_2_biases[ch]
                    second_layer_outputs[ch, stride] = (np.max([0, cnv_plus_bias]))  # After Relu
            second_layer_outputs_transpose = (second_layer_outputs.T / 256).astype(np.uint16)

            fc_outputs_calc = np.dot(second_layer_outputs_transpose, self.dense1_weights) + self.dense1_biases
            fc_outputs_calc = np.maximum(fc_outputs_calc, 0)
            fc_outputs_calc = fc_outputs_calc.T

            network_outoputs_results[i, :] = np.average(fc_outputs_calc, axis=1)
        np.save('C:/Projects/Python/Tape_detect/test_vectors/network_outputs_for_batch_input.npy',np.hstack((network_outoputs_results, self.Y_test)))
        print("L1 Conv1d output", second_layer_outputs)

    def calculate_and_plot_confusion_matrix(self):
        self.model_inference()
        thresh = 155
        self.y_pred[self.y_pred < thresh] = 0
        self.y_pred[self.y_pred >= thresh] = 1

        # diff_indices = np.where(outputs != y_pred)
        # diff_array = np.vstack((diff_indices[0],outputs[diff_indices],y_pred[diff_indices])).T

        self.confusion_matrix_calc(self.Y_test, self.y_pred)
        return ()

    def extract_weights_from_model(self):  # run in Colab
        # ecxtract model weights
        # Load TFLite model and allocate tensors.
        interpreter = tf.lite.Interpreter(model_path=self.input_model_file_path)
        interpreter.allocate_tensors()
        # Get input and output tensors.
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        # get details for each layer
        all_layers_details = interpreter.get_tensor_details()

        f = h5py.File("mobilenet_v3_weights_infos.hdf5", "w")
        i = 0
        for layer in all_layers_details[:20]:
            # to create a group in an hdf5 file
            grp = f.create_group(str(layer['index']))
            # to store layer's metadata in group's metadata
            grp.attrs["name"] = layer['name']
            grp.attrs["shape"] = layer['shape']
            # grp.attrs["dtype"] = all_layers_details[i]['dtype']
            grp.attrs["quantization"] = layer['quantization']
            # to store the weights in a dataset
            print(i)
            print(interpreter.get_tensor(layer['index']))
            np.save('checkpoint_org_des.%d.npy' % (i), interpreter.get_tensor(layer['index']))
            i += 1
            grp.create_dataset("weights", data=interpreter.get_tensor(layer['index']))
        f.close()
        return ()

    def confusion_matrix_calc(self, Y_test, Yp_pred):
        matrix = confusion_matrix(Y_test, Yp_pred)
        tn, fp, fn, tp = matrix.ravel()

        print("True negative", tn)
        print('False negative', fn)
        print(((fn) / (fn + tn)) * 100, '% false negative Fails', '\n')

        print("True positive", tp)
        print("False positive", fp)
        print(((fp) / (fp + tp)) * 100, '% false positive Fails', '\n')

        return ()


if __name__ == "__main__":

    MO = Model_optimization()
    MO.load_keras_model_weights()
    # MO.convert_array_to_c_format()
    # MO.model_inference()
    # MO.load_weights()
    # MO.calculate_and_plot_confusion_matrix()
    #MO.calculate_layers_outputs()
    # MO.calculate_network_outputs_for_batch_input()
    # MO.calculate_classification()
    # MO.inference()
    # MO.classification()

print("Stop")