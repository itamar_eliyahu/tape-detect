import numpy as np
from keras.utils import to_categorical
import tensorflow as tf
from keras.layers import  Dropout, MaxPooling1D, Flatten, Input, Dense, MaxPool2D, Lambda, Bidirectional,Conv1D,GlobalAveragePooling1D,Reshape
from keras.models import Model,Sequential
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint,TensorBoard,EarlyStopping,LearningRateScheduler
import time
from sklearn.metrics import classification_report, confusion_matrix,precision_recall_curve,auc,accuracy_score
from keras.utils.vis_utils import plot_model
import pydot
import graphviz
import timeit
import matplotlib.pyplot as plt
from cnn_model import one_dim_cnn_model
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
import re
import shutil
import h5py
import seaborn as sns

class Model_optimization():

    flags = {'remove_iligeal_file':0,'debug_mode':1,'save_network_outputs':1,'tape_detect_in_thread_channels':0,'tape_detect_in_watermark_channels':1,'tape_detect_in_regular_channels':0}

    def __init__(self, tf_lite_input_model_file_path,X_test,Y_test,keras_model_path,X_test_for_inference,Y_test_for_inference):
        self.tf_lite_input_model_file_path = tf_lite_input_model_file_path

        self.Y_test = Y_test
        self.X_test = X_test[:, :100]
        self.keras_model_path = keras_model_path

        self.X_test_for_inference = X_test_for_inference#[:, :, :100]
        self.Y_test_for_inference = Y_test_for_inference


    def load_weights(self):

        if (self.flags['tape_detect_in_watermark_channels']):

            self.divider_for_first_layer = 185#209
            self.divider_for_second_layer = 299#293

            self.c_array_file_base_path = 'C:/Projects/Python/Tape_detect/Weights/tape_detect_in_watermark_channels/Watermark_'

            self.conv_1d_1channel_weights = np.load('C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/parameters_vector_for_section_13.npy')
            self.conv_1d_1channel_weights = self.conv_1d_1channel_weights.reshape(1, 15)
            self.conv_1d_1channel_biases = np.load('C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/parameters_vector_for_section_2.npy')
            self.save_weight_c_format(self.conv_1d_1channel_weights, self.conv_1d_1channel_biases, 'Vector1_weights')

            num_of_channles = 8
            kernel_length = 16
            self.conv_1d_16channels_weights = np.load('C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/parameters_vector_for_section_15.npy')
            self.conv_1d_16channels_weights = self.conv_1d_16channels_weights.reshape(num_of_channles,kernel_length)  # .T # even if it looks like transposed , the result is equale to the model results withou transpose it
            self.conv_1d_16channels_biases = np.load('C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/parameters_vector_for_section_3.npy')
            self.save_weight_c_format(self.conv_1d_16channels_weights, self.conv_1d_16channels_biases, 'Filter1_weights')

            num_of_channles = 8
            kernel_length = 8
            self.cls_dense1_weights = np.load('C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/parameters_vector_for_section_10.npy')
            self.cls_dense1_weights = self.cls_dense1_weights.reshape(num_of_channles,kernel_length).T
            self.cls_dense1_biases= np.load('C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/parameters_vector_for_section_5.npy')
            self.save_weight_c_format(self.cls_dense1_weights, self.cls_dense1_biases, 'Filter2_weights')

            self.cls_dense2_weights = np.load('C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/parameters_vector_for_section_11.npy')
            self.cls_dense2_weights = self.cls_dense2_weights.T#reshape(num_of_channles,kernel_length)  # .T # even if it looks like transposed , the result is equale to the model results withou transpose it
            self.cls_dense2_biases = np.load('C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/parameters_vector_for_section_6.npy')
            self.save_weight_c_format(self.cls_dense2_weights.T, self.cls_dense2_biases, 'Filter3_weights')

        elif (self.flags['tape_detect_in_regular_channels']):

            self.divider_for_first_layer  =420#911
            self.divider_for_second_layer =115#244


            self.c_array_file_base_path = 'C:/Projects/Python/Tape_detect/Weights/tape_detect_in_regular_channels/Regular_'
            self.conv_1d_1channel_weights = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_regular_channels/parameters_vector_for_section_13.npy')
            self.conv_1d_1channel_weights = self.conv_1d_1channel_weights.reshape(1, 15)
            self.conv_1d_1channel_biases = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_regular_channels/parameters_vector_for_section_2.npy')
            self.save_weight_c_format(self.conv_1d_1channel_weights, self.conv_1d_1channel_biases, 'Vector1_weights')

            num_of_channles = 8
            kernel_length = 16
            self.conv_1d_16channels_weights = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_regular_channels/parameters_vector_for_section_15.npy')
            self.conv_1d_16channels_weights = self.conv_1d_16channels_weights.reshape(num_of_channles,kernel_length)  # .T # even if it looks like transposed , the result is equale to the model results withou transpose it
            self.conv_1d_16channels_biases = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_regular_channels/parameters_vector_for_section_3.npy')
            self.save_weight_c_format(self.conv_1d_16channels_weights, self.conv_1d_16channels_biases,'Filter1_weights')


            num_of_channles = 8
            kernel_length = 8
            self.cls_dense1_weights = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_regular_channels/parameters_vector_for_section_10.npy')
            self.cls_dense1_weights = self.cls_dense1_weights.reshape(num_of_channles,kernel_length).T
            self.cls_dense1_biases= np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_regular_channels/parameters_vector_for_section_5.npy')
            self.save_weight_c_format(self.cls_dense1_weights, self.cls_dense1_biases, 'Filter2_weights')

            self.cls_dense2_weights = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_regular_channels/parameters_vector_for_section_11.npy')
            self.cls_dense2_weights = self.cls_dense2_weights.T#reshape(num_of_channles,kernel_length)  # .T # even if it looks like transposed , the result is equale to the model results withou transpose it
            self.cls_dense2_biases = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_regular_channels/parameters_vector_for_section_6.npy')
            self.save_weight_c_format(self.cls_dense2_weights.T, self.cls_dense2_biases, 'Filter3_weights')

        elif (self.flags['tape_detect_in_thread_channels']):

            self.divider_for_first_layer =340#218
            self.divider_for_second_layer =232# 223


            self.c_array_file_base_path = 'C:/Projects/Python/Tape_detect/Weights/tape_detect_in_thread_channels/Thread_'
            self.conv_1d_1channel_weights = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_thread_channels/parameters_vector_for_section_13.npy')
            self.conv_1d_1channel_weights = self.conv_1d_1channel_weights.reshape(1, 15)
            self.conv_1d_1channel_biases = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_thread_channels/parameters_vector_for_section_2.npy')
            self.save_weight_c_format(self.conv_1d_1channel_weights, self.conv_1d_1channel_biases, 'Vector1_weights')

            num_of_channles = 8
            kernel_length = 16
            self.conv_1d_16channels_weights = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_thread_channels/parameters_vector_for_section_15.npy')
            self.conv_1d_16channels_weights = self.conv_1d_16channels_weights.reshape(num_of_channles,kernel_length)  # .T # even if it looks like transposed , the result is equale to the model results withou transpose it
            self.conv_1d_16channels_biases = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_thread_channels/parameters_vector_for_section_3.npy')
            self.save_weight_c_format(self.conv_1d_16channels_weights, self.conv_1d_16channels_biases,'Filter1_weights')


            num_of_channles = 8
            kernel_length = 8
            self.cls_dense1_weights = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_thread_channels/parameters_vector_for_section_10.npy')
            self.cls_dense1_weights = self.cls_dense1_weights.reshape(num_of_channles,kernel_length).T
            self.cls_dense1_biases= np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_thread_channels/parameters_vector_for_section_5.npy')
            self.save_weight_c_format(self.cls_dense1_weights, self.cls_dense1_biases, 'Filter2_weights')

            self.cls_dense2_weights = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_thread_channels/parameters_vector_for_section_11.npy')
            self.cls_dense2_weights = self.cls_dense2_weights.T#reshape(num_of_channles,kernel_length)  # .T # even if it looks like transposed , the result is equale to the model results withou transpose it
            self.cls_dense2_biases = np.load('C:/Projects/Python/Tape_detect/best_model/tape_detect_in_thread_channels/parameters_vector_for_section_6.npy')
            self.save_weight_c_format(self.cls_dense2_weights.T, self.cls_dense2_biases, 'Filter3_weights')

        return()
    def save_weight_c_format(self,weights_arr,biases_arr,text_file_name):
        biases_arr = np.expand_dims(biases_arr, axis=1)
        data_arr = np.hstack((weights_arr,biases_arr )).astype(int)
        rows, cols = data_arr.shape
        c_file_path = str(self.c_array_file_base_path + str(text_file_name)+'.txt')
        c_file = open(c_file_path, 'w')
        c_file.write('{')
        for i in range(0, rows):
            c_file.write('{')
            for j in range(0, cols):
                if (j != cols - 1):
                    c_file.write(str((data_arr[i, j])) + ',')
                else:
                    c_file.write(str(data_arr[i, j]))
            if (i != rows - 1):
                c_file.write('},' + '\n')
            else:
                c_file.write('}')
        c_file.write('};')
        c_file.close()
        return()

    def convert_array_to_c_format(self):
        data_arr = self.X_test
        data_arr = data_arr.reshape(data_arr.shape[0],data_arr.shape[2])
        rows, cols = data_arr.shape
        c_file_path =  'C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_watermark_channels_network_inputs.txt'
        c_file = open(c_file_path, 'w')
        c_file.write('{')
        for i in range(0, rows):
            c_file.write('{')
            for j in range(0, cols):
                if (j != cols - 1):
                    c_file.write(str(('%d'%data_arr[i, j])) + ',')
                else:
                    c_file.write(str('%d'%data_arr[i, j]))
            if (i != rows - 1):
                c_file.write('}' + '\n')
            else:
                c_file.write('}')
        c_file.write('}')
        c_file.close()
        return ()

    def load_keras_model_weights(self):
        n_kernel_1 =16
        n_outputs =1

        self.keras_model = one_dim_cnn_model(n_kernel_1, n_outputs)
        self.keras_model.load_weights(self.keras_model_path)
        self.kears_model_weights = []
        for layer in self.keras_model.layers:
            layer_w = layer.get_weights()
            self.kears_model_weights.append(layer_w)

        return()
    def model_quantization(self): # run in colab only
        # convert Keras model to 8bit weights and operations.
        # then save the model
        #https: // www.tensorflow.org / lite / performance / post_training_quantization
        # quantization to int8 done with calculate the dynamic range of the inputs and then estimate the quantization to all parameters in the model.
        # there are few option for quantization :
        # 1 - quantize the parameter to 8bit after training , then , while inference , recast the 8bit vakues to float32 and calculate the CNN with float32
        # 2 - quantize the parameters after training to int 8bit , then , while inference , continue calculate the CNN with 8bit integer
        # 3 - train Keras model , and then convert the model to Tensorflow lite with the required quantzation
        # Load keras model

        self.load_keras_model_weights()
        # convert model to Tensorflow lite format
        def representative_dataset_gen():
            for input_value in tf.data.Dataset.from_tensor_slices(self.X_test).batch(1).take(1000):
                yield [input_value]

        converter = tf.lite.TFLiteConverter.from_keras_model(self.keras_model)
        # converter.optimizations = [tf.lite.Optimize.OPTIMIZE_FOR_SIZE]
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
        converter.representative_dataset = representative_dataset_gen
        # converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
        # Set the input and output tensors to uint8 (APIs added in r2.3)
        converter.inference_input_type = tf.uint8
        converter.inference_output_type = tf.uint8
        tflite_quant_model = converter.convert()
        open('/content/drive/My Drive/Tape_detect/best_model/quantized_tflite_model.tflite', "wb").write(tflite_quant_model)  # Save the TF Lite model.
        return()

    def model_inference(self):
        # batching tensorflow lite for inference
        # Load the TFLite model and allocate tensors.
        interpreter = tf.lite.Interpreter(model_path=self.tf_lite_input_model_file_path)
        #interpreter = tf.lite.Interpreter(model_path='C:/Projects/Python/Tape_detect/best_model/tflite_debug_model.tflite')

        interpreter.allocate_tensors()
        # Get input and output tensors.
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()

        if (self.flags['debug_mode']):
            self.load_keras_model_weights()
            all_layers_details = interpreter.get_tensor_details()
            #for layer in all_layers_details:
                #data = interpreter.get_tensor(layer['index'])
        #self.X_test = self.X_test_for_inference
        input_shape = [self.X_test.shape[0], self.X_test.shape[1]]
        interpreter.resize_tensor_input(input_details[0]['index'],input_shape)
        input_details_2 = interpreter.get_input_details()
        input_data = self.X_test.astype(np.uint8)
        interpreter.allocate_tensors()
        interpreter.set_tensor(input_details[0]['index'], input_data)
        interpreter.invoke()
        self.y_pred = interpreter.get_tensor(output_details[0]['index'])
        self.temp= interpreter.get_tensor(14)
        return ()


    def calculate_layers_outputs(self):
        self.load_weights()
        #first layer for 1 channel
        if (self.flags['tape_detect_in_watermark_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/debug_model_conv_1D_1ch_results.tflite'
        elif (self.flags['tape_detect_in_regular_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_regular_channels/debug_model_conv_1D_1ch_results.tflite'
        elif (self.flags['tape_detect_in_thread_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_thread_channels/debug_model_conv_1D_1ch_results.tflite'

        self.model_inference()
        TFlite_model_first_conv1d_1ch_outputs = self.y_pred.reshape(self.y_pred.shape[0],self.y_pred.shape[1])

        # first layer for 16 channel
        if (self.flags['tape_detect_in_watermark_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/debug_model_conv_1D_16ch_results.tflite'
        elif (self.flags['tape_detect_in_regular_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_regular_channels/debug_model_conv_1D_16ch_results.tflite'
        elif (self.flags['tape_detect_in_thread_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_thread_channels/debug_model_conv_1D_16ch_results.tflite'

        self.model_inference()
        TFlite_model_first_conv1d_16_outputs = self.y_pred#[0,:,:]

        # Output layer AVG
        if (self.flags['tape_detect_in_watermark_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/debug_model_AVG_results.tflite'
        elif (self.flags['tape_detect_in_regular_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_regular_channels/debug_model_AVG_results.tflite'
        elif (self.flags['tape_detect_in_thread_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_thread_channels/debug_model_AVG_results.tflite'

        self.model_inference()
        TFlite_model_pool_avg_layer_outputs = self.y_pred.reshape(self.y_pred.shape[0],self.y_pred.shape[2])
        #np.save('C:/Projects/Python/Tape_detect/test_vectors/network_outputs.npy',TFlite_model_pool_avg_layer_outputs)


        # first layer of classifier
        if (self.flags['tape_detect_in_watermark_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/debug_model_first_cls_fc_layer_results.tflite'
        elif (self.flags['tape_detect_in_regular_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_regular_channels/debug_model_first_cls_fc_layer_results.tflite'
        elif (self.flags['tape_detect_in_thread_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_thread_channels/debug_model_first_cls_fc_layer_results.tflite'

        #self.model_inference()
        #TFlite_model_first_fc_layer_of_cls_outputs = self.y_pred#[0,:,:]

        # second layer of classifier
        if (self.flags['tape_detect_in_watermark_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/debug_model_second_cls_fc_layer_results.tflite'
        elif (self.flags['tape_detect_in_regular_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_regular_channels/debug_model_second_cls_fc_layer_results.tflite'
        elif (self.flags['tape_detect_in_thread_channels']):
            self.tf_lite_input_model_file_path = 'C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_thread_channels/debug_model_second_cls_fc_layer_results.tflite'

        self.model_inference()
        TFlite_model_second_fc_layer_of_cls_outputs = self.y_pred#[0,:,:]
        #self.Y_test = np.expand_dims(self.Y_test, axis=1)
        #np.save('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_regular_channels_network_outputs_from_TF.npy',np.hstack((TFlite_model_second_fc_layer_of_cls_outputs, self.Y_test)))

        self.network_outputs_features = np.zeros((self.X_test.shape[0], 8))
        self.first_layer_outputs_arr = np.zeros((self.X_test.shape[0], 100))
        self.second_layer_outputs_arr = np.zeros((self.X_test.shape[0],10,8))
        self.first_layer_inputs_arr = np.zeros((self.X_test.shape[0], 114))
        self.fact_arr = np.zeros((self.X_test.shape[0], 1))
        self.second_classifier_fc_layer_arr= np.zeros((self.X_test.shape[0], 1))

        #self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_regular_channels_with_tape_samples_after_start_peak_bmp.npy')#dsptst117_2_reject
        #self.X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_watermark_channels_with_tape_samples_after_start_peak_bmp.npy')#dsptst117_2_reject
        #self.X_test =np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_regular_channels_wo_tape_bmp_ng_05_15.npy')
        #self.X_test =self.X_test.reshape(self.X_test.shape[0],self.X_test.shape[2])
        #self.X_test = self.X_test[:30000,:]
        #self.X_test = self.X_test[195000:, :]

        for j in range(0, self.X_test.shape[0]):
            input_vect = np.zeros((1,114))
            input_vect[0,7:107] =  self.X_test[j,:100]
            self.first_layer_inputs_arr[j,:]=input_vect
            first_layer_outputs = np.zeros((1,100))
            for stride in range(0, 100):
                #print(stride,stride+15)
                data_section = input_vect[0, stride:stride + 15]
                #print(data_section.shape)
                cnv_plus_bias = np.sum(data_section * self.conv_1d_1channel_weights) + self.conv_1d_1channel_biases
                first_layer_outputs[0,stride]=(np.maximum(0,cnv_plus_bias))#After Relu
            first_layer_outputs=first_layer_outputs/self.divider_for_first_layer
            first_layer_outputs = np.around(first_layer_outputs).astype(np.uint32)
            #first_layer_outputs = (first_layer_outputs).astype(np.uint32)
            self.first_layer_outputs_arr[j,:]=first_layer_outputs
            np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/network_outputs_after_vector1_and_division.txt',first_layer_outputs, fmt='%i')
            np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/network_input.txt',input_vect[0,7:107], fmt='%i')

            '''
            fact = first_layer_outputs / TFlite_model_first_conv1d_1ch_outputs[j,:]
            fact = fact.flatten()
            fact = fact[~np.isnan(fact)]
            fact = fact[~np.isinf(fact)]
            fact = fact.mean()
            self.fact_arr[j,:] =fact
            '''

            avg_pool_layer_output = np.around(np.mean(first_layer_outputs.reshape(-1, 4), axis=1)).astype(np.uint32)
            np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/network_outputs_after_avg1.txt',avg_pool_layer_output, fmt='%i')

            second_layer_input =avg_pool_layer_output
            num_of_channels_in_second_layer = 8
            second_layer_outputs = np.zeros((num_of_channels_in_second_layer, 10))
            kernel_length = 16

            for i in range(0, 1):#X_test.shape[0]):
                for ch in range(0, num_of_channels_in_second_layer):
                    for stride in range(0, 10):
                        data_section = second_layer_input[stride:stride + kernel_length]
                        cnv_plus_bias = np.sum(data_section * self.conv_1d_16channels_weights[ch]) + self.conv_1d_16channels_biases[ch]
                        #cnv_plus_bias = np.dot(data_section ,self.conv_1d_16channels_weights[ch]) + self.conv_1d_16channels_biases[ch]
                        second_layer_outputs[ch, stride] = (np.max([0, cnv_plus_bias]))# After Relu
            second_layer_outputs = (second_layer_outputs.T)/self.divider_for_second_layer
            second_layer_outputs = np.around(second_layer_outputs).astype(np.uint32)
            self.second_layer_outputs_arr[j,:,:] = second_layer_outputs

            '''
            fact = second_layer_outputs / TFlite_model_first_conv1d_16_outputs[j,:,:]
            fact = fact.flatten()
            fact = fact[~np.isnan(fact)]
            fact = fact[~np.isinf(fact)]
            fact = fact.mean()
            self.fact_arr[j,:] =fact
            '''


            second_layer_outputs = np.around(np.average(second_layer_outputs, axis=0)).astype(np.uint32)
            np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/network_outputs_after_filter1.txt',second_layer_outputs, fmt='%i')

            #8X8
            self.first_classifier_fc_layer = np.dot(second_layer_outputs,self.cls_dense1_weights) + self.cls_dense1_biases
            #fc_outouts_calc = np.dot(np.around(np.average(TFlite_model_first_conv1d_16_outputs[j,:,:], axis=0)).astype(np.uint32), self.cls_dense1_weights) + self.cls_dense1_biases
            self.first_classifier_fc_layer = np.maximum(self.first_classifier_fc_layer, 0)#Relu
            self.first_classifier_fc_layer = (self.first_classifier_fc_layer)
            self.first_classifier_fc_layer = np.around(self.first_classifier_fc_layer).astype(np.float)
            np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/network_outputs_after_filter2.txt',self.first_classifier_fc_layer, fmt='%i')


            # 8X1
            self.second_classifier_fc_layer = np.dot(self.first_classifier_fc_layer,self.cls_dense2_weights.astype(np.float)) + self.cls_dense2_biases.astype(np.float)
            # fc_outouts_calc = np.dot(TFlite_model_first_conv1d_outputs, self.dense1_weights) + self.dense1_biases
            self.second_classifier_fc_layer = self.second_classifier_fc_layer.astype(np.float)
            np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/network_outputs_after_filter3.txt',self.second_classifier_fc_layer, fmt='%i')
            #self.second_classifier_fc_layer =  1 / (1 + np.exp(-self.second_classifier_fc_layer))#sigmoid
            self.second_classifier_fc_layer_arr[j,:] = self.second_classifier_fc_layer

            '''
            #self.network_outputs_features[j, :] = np.around(np.average(second_layer_outputs, axis=0)).astype(np.uint32)
            '''
        #print("Fact",np.mean(self.fact_arr,axis=0))
        #np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/network_outputs.txt',network_outoputs_results,fmt='%i')

        if (self.flags['save_network_outputs']):
            self.Y_test = np.expand_dims(self.Y_test, axis=1)
            #features_and_labels_arr = np.hstack((self.second_classifier_fc_layer_arr, self.Y_test))
            #comp = np.apply_along_axis(lambda x: features_and_labels_arr[:, 0] == features_and_labels_arr[:, 1], 0,features_and_labels_arr)
            #acc = np.sum(comp[:, 0]) / features_and_labels_arr.shape[0] * 100
            #print("Accuracy:", acc)
            #self.confusion_matrix_calc(self.Y_test.astype(np.uint8),self.second_classifier_fc_layer_arr.astype(np.uint8))

            #np.save('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_regular_channels_network_outputs2.npy',np.hstack((self.second_classifier_fc_layer_arr,self.Y_test)))
            np.save('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_watermark_channels_network_outputs2.npy',np.hstack((self.second_classifier_fc_layer_arr,self.Y_test)))
            #np.save('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_thread_channels_network_outputs.npy',np.hstack((self.second_classifier_fc_layer_arr, self.Y_test)))

            #np.save('C:/Projects/Python/Tape_detect/test_vectors/first_layer_outputs.npy',self.first_layer_outputs_arr)
            #np.save('C:/Projects/Python/Tape_detect/test_vectors/second_layer_outputs.npy', self.second_layer_outputs_arr)
            #np.save('C:/Projects/Python/Tape_detect/test_vectors/first_layer_inputs.npy',self.first_layer_inputs_arr)

            #self.network_features_and_labels = np.hstack((self.network_outputs_features,self.Y_test))
            #np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/network_outputs.txt',np.hstack((TFlite_model_pool_avg_layer_outputs,self.Y_test)),fmt='%i')
            #np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_network_outputs.txt',self.network_outputs_features,fmt='%i')


    def calculate_network_outputs_for_batch_input(self):
        #self.X_test = self.X_test[:10, :, :]
        network_outoputs_results = np.zeros((X_test.shape[0],16))
        second_layer_outputs = np.zeros((16, 10))
        for i in range(0,self.X_test.shape[0]):
            for ch in range(0, 16):
                for stride in range(0, 10):
                    data_section = self.X_test[i, :, stride:stride + 16]
                    cnv_plus_bias = np.sum(data_section * self.conv_1d_16channels_weights[ch]) + self.conv_1d_16channels_biases[ch]
                    #cnv_plus_bias = np.dot(data_section ,self.conv_1d_16channels_weights[ch]) + self.conv_1d_16channels_biases[ch]
                    second_layer_outputs[ch, stride] = (np.max([0, cnv_plus_bias]))# After Relu
            second_layer_outputs_transpose = (second_layer_outputs.T/256).astype(np.uint16)

            fc_outputs_calc = np.dot(second_layer_outputs_transpose, self.dense1_weights) + self.dense1_biases
            fc_outputs_calc = np.maximum(fc_outputs_calc, 0)
            fc_outputs_calc = fc_outputs_calc.T

            network_outoputs_results[i,:] = np.average(fc_outputs_calc, axis=1)
        np.save('C:/Projects/Python/Tape_detect/test_vectors/network_outputs_for_batch_input.npy',np.hstack((network_outoputs_results,self.Y_test)))
        print("L1 Conv1d output", second_layer_outputs)



    def calculate_and_plot_confusion_matrix(self):
        self.model_inference()
        thresh =155
        self.y_pred[self.y_pred < thresh] = 0
        self.y_pred[self.y_pred >= thresh] = 1

        #diff_indices = np.where(outputs != y_pred)
        #diff_array = np.vstack((diff_indices[0],outputs[diff_indices],y_pred[diff_indices])).T

        self.confusion_matrix_calc(self.Y_test, self.y_pred)
        return()

    def extract_weights_from_model(self):
        # ecxtract model weights
        # Load TFLite model and allocate tensors.
        interpreter = tf.lite.Interpreter(model_path=self.input_model_file_path)
        interpreter.allocate_tensors()
        # Get input and output tensors.
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        # get details for each layer
        all_layers_details = interpreter.get_tensor_details()

        f = h5py.File("mobilenet_v3_weights_infos.hdf5", "w")
        i = 0
        for layer in all_layers_details[:20]:
            # to create a group in an hdf5 file
            grp = f.create_group(str(layer['index']))
            # to store layer's metadata in group's metadata
            grp.attrs["name"] = layer['name']
            grp.attrs["shape"] = layer['shape']
            # grp.attrs["dtype"] = all_layers_details[i]['dtype']
            grp.attrs["quantization"] = layer['quantization']
            # to store the weights in a dataset
            print(i)
            print(interpreter.get_tensor(layer['index']))
            np.save('checkpoint_org_des.%d.npy' % (i), interpreter.get_tensor(layer['index']))
            i += 1
            grp.create_dataset("weights", data=interpreter.get_tensor(layer['index']))
        f.close()
        return()

    def calculate_classification(self):



        #self.calculate_layers_outputs()

        #features_and_labels_arr = np.load('C:/Projects/Python/Tape_detect/test_vectors/network_outputs.npy').astype(np.uint32)#regular
        #features_and_labels_arr = np.load('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_watermark_channels_network_outputs.npy').astype(np.uint32)
        features_and_labels_arr = np.load('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_watermark_channels_network_outputs.npy')
        #features_and_labels_arr = np.load('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_regular_channels_network_outputs.npy').astype(np.uint32)
        features_and_labels_arr2 = np.load('C:/Projects/Python/Tape_detect/test_vectors/network_outputs.npy')

        self.confusion_matrix_calc(features_and_labels_arr[:,1].astype(np.uint8), features_and_labels_arr[:,0].astype(np.uint8))

        self.confusion_matrix_calc(self.second_classifier_fc_layer_arr, self.Y_test)
        #self.network_outputs_features = features_and_labels_arr[:,:-1].astype(np.uint8)
        #self.Y_test = self.Y_test[:, -1]
        comp1 = np.where(features_and_labels_arr[:,0] != features_and_labels_arr[:,1] )
        comp = np.apply_along_axis(lambda x: features_and_labels_arr[:, 0] == features_and_labels_arr[:, 1], 0, features_and_labels_arr)
        acc = np.sum(comp[:, 0]) / features_and_labels_arr.shape[0] * 100
        print("LR Accuracy:", acc)

        #X_from_c_design = np.loadtxt('C:/Projects/Python/Tape_detect/test_vectors/c_design_output.txt')
        #self.network_outputs_features = X_from_c_design
        #self.Y_test = self.Y_test[:, -1][:-1]
        # np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/Thread_detect_features_and_lables.txt',np.hstack((X_features,np.expand_dims(Y_features,axis=1))),fmt='%i')
        fp_total = 0
        fn_total = 0

        cycles = 1
        for i in range(0,cycles):
            X_train, X_test, y_train, y_test = train_test_split(self.network_outputs_features, self.Y_test, test_size=0.25, shuffle=True)

            corr = np.corrcoef(self.network_outputs_features)
            mask = np.zeros_like(corr)
            mask[np.triu_indices_from(mask)] = True
            with sns.axes_style("white"):
                ax = sns.heatmap(corr, mask=mask, vmax=.3, square=True, cmap="YlGnBu")
                plt.show()
            #from sklearn.mixture import GMM

            #from sklearn.neural_network import MLPClassifier
            #mlp =  MLPClassifier(alpha=1, max_iter=1000).fit(X_train, y_train)
            #print("mlp prediction accuracy is: ", mlp.score(X_test, y_test) * 100, "%")

            from sklearn.neighbors import KNeighborsClassifier
            KNN = KNeighborsClassifier(n_neighbors=3,algorithm='brute')
            KNN.fit(X_train, y_train)
            print("KNN prediction accuracy is: ", KNN.score(X_test, y_test) * 100, "%")
            Y_pred_knn = KNN.predict(X_test)
            matrix = confusion_matrix(y_test, Y_pred_knn)
            tn, fp, fn, tp = matrix.ravel()

            print('False negative', ((fn) / (fn + tn)) * 100)
            print('False positive', ((fp) / (fp + tp)) * 100)

            # Desicion tree
            '''
            Desicion_Tree = DecisionTreeClassifier(criterion='entropy').fit(X_train, y_train)
            Y_pred_dt = Desicion_Tree.predict(X_test)
            print("Desicion Tree prediction accuracy is: ", Desicion_Tree.score(X_test, y_test) * 100, "%")
            #plt.plot(Desicion_Tree.feature_importances_)
            #plt.show()
            
            # Cross validation
            fold =5
            scores = cross_val_score(Desicion_Tree,self.network_outputs_features, self.Y_test, cv=fold)
            print("Desicion_Tree cross_val Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
            '''

            # logistic regression
            #logist_regression = LogisticRegression(random_state=0,solver='saga',warm_start=True,max_iter=100).fit(X_train , y_train)# FN 2.4 , FP 0.28

            #logist_regression = LogisticRegression(random_state=6, solver='saga',warm_start=True,multi_class='multinomial').fit(X_train, y_train)  # FN 2.4 , FP 0.28

            #good thresh 0.94
            #logist_regression = LogisticRegression(random_state=0, solver='saga',warm_start=True,multi_class='multinomial',penalty='l1').fit(X_train, y_train)  # FN 2.4 , FP 0.28
            #good  - thresh 0.8 for watermark
            logist_regression = LogisticRegression(random_state=0, solver='saga',warm_start=True,multi_class='multinomial',penalty='l1').fit(X_train, y_train)  # FN 2.4 , FP 0.28

            #99.5
            #logist_regression = LogisticRegression(C=1,random_state=0, solver='sag',warm_start=False,dual=False,multi_class='multinomial',penalty='l2').fit(X_train, y_train)  # FN 2.4 , FP 0.28


            #logist_regression = LogisticRegression(random_state=0).fit(X_train, y_train)  # FN 2.4 , FP 0.28
            #logist_regression = LogisticRegression(C=1, multi_class='multinomial', penalty= 'l2', solver= 'saga',dual=False,warm_start=True).fit(X_train, y_train)

            #logist_regression = LogisticRegression(random_state=0,solver='liblinear',warm_start=True,dual=True,penalty='l2',C=2).fit(X_train , y_train)#thresh 0.999 FN 1.2 , FP 0.56

            #logist_regression = LogisticRegression(random_state=0,solver='sag',warm_start=True,multi_class='ovr',penalty='l2',C=10).fit(X_train , y_train)#thresh 0.999 FN 1.2 , FP 0.56

            # for regular channels after grid search
            #logist_regression = LogisticRegression(C=0.01,solver='lbfgs',warm_start=True,multi_class='multinomial',penalty='l2',dual=False).fit(X_train , y_train)#thresh 0.999 FN 1.2 , FP 0.56
            logist_regression = LogisticRegression(C=0.05,solver='lbfgs',warm_start=False,multi_class='multinomial',penalty='l2',dual=False).fit(X_train , y_train)#thresh 0.999 FN 1.2 , FP 0.56


            logist_regression = LogisticRegression(C=1,solver='liblinear',warm_start=True,multi_class='ovr',penalty='l2',dual=False).fit(X_train , y_train)#thresh 0.999 FN 1.2 , FP 0.56

            print("logistic regression prediction accuracy is: ", logist_regression.score(X_test, y_test) * 100, "%")

            Y_pred_lr = logist_regression.predict(X_test)
            Y_pred_lr = Y_pred_lr.T
            Y_pred_score = logist_regression.score(X_test, y_test) * 100
            print("logistic regression prediction accuracy is: ", logist_regression.score(X_test, y_test) * 100, "%")


            # for test only
            # weights = np.array([2.02974178, -2.02827026, -1.66486965,  3.7358174 , -0.32187554,1.56361319, -3.57341289,  2.51069765, -2.09967019, -0.23454262,0.06776431,  2.55470682,  0.17940604, -0.3072654 , -2.41347976,4.531925  ])
            #input_vector = np.array([0, 0.22340667, 0.29714434, 0.31255781, 0, 0.24365507, 0, 0, 0, 0, 0, 2.01794781, 0, 0, 0, 1.32968738])
            # z = (np.dot(input_vector, weights.T))
            # thresh = 0.9

            weights = logist_regression.coef_
            z = (np.dot(X_test, weights.T))


            thresh = 0.99#0.98
            Y_pred_lr_np_calculated = 1 / (1 + np.exp(-z))
            #Y_pred_lr_np_calculated2 = np.exp(z) / (1 + np.exp(z))
            Y_pred_lr_np_calculated[Y_pred_lr_np_calculated < thresh] = 0
            Y_pred_lr_np_calculated[Y_pred_lr_np_calculated >= thresh] = 1

            #x = np.arange(Y_pred_lr_np_calculated.shape[0])
            #plt.scatter(x,Y_pred_lr_np_calculated,marker='2')
            #plt.scatter(x,Y_pred_lr,marker='3')
            #plt.show()

            # compare between Y_pred_lr_np_calculated and sclearn Y_pred
            res = np.hstack((Y_pred_lr_np_calculated, np.expand_dims(Y_pred_lr, axis=1)))
            comp = np.apply_along_axis(lambda x: res[:, 0] == res[:, 1], 0, res)
            acc = np.sum(comp[:, 0]) / X_test.shape[0] * 100
            print("LR Accuracy:", acc)




            # Cross validation
            #scores = cross_val_score(logist_regression, self.network_outputs_features, self.Y_test, cv=fold)
            #print("logistic regression cross_val Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

            #matrix = confusion_matrix(y_test, Y_pred_lr_np_calculated)
            matrix = confusion_matrix(y_test, Y_pred_lr)
            #matrix = confusion_matrix(y_test, Y_pred_dt)


            tn, fp, fn, tp = matrix.ravel()
            fp_total += ((fp) / (fp + tp)) * 100
            fn_total += ((fn) / (fn + tn)) * 100

        print('False negative', fn_total/cycles)
        print('False positive', fp_total/cycles)
        print("Stop")

        '''
        print("True negative", tn)
        print('False negative', fn)
        print(((fn) / (fn + tn)) * 100, '% false negative Fails', '\n')

        print("True positive", tp)
        print("False positive", fp)
        print(((fp) / (fp + tp)) * 100, '% false positive Fails', '\n')
        '''

    def inference(self):

        X_test_temp = self.X_test
        Y_test_temp = self.Y_test

        self.X_test = self.X_test_for_inference
        self.Y_test = self.Y_test_for_inference





        features_and_labels_arr = np.load('C:/Projects/Python/Tape_detect/test_vectors/network_outputs.npy').astype(np.uint32)#[:,:-1]
        X_train, X_test, y_train, y_test = train_test_split(features_and_labels_arr, self.Y_test, test_size=0.25,shuffle=True)
        logist_regression = LogisticRegression(random_state=0, solver='saga', warm_start=True,multi_class='multinomial', penalty='l1').fit(X_train,y_train)  # FN 2.4 , FP 0.28
        print("logistic regression prediction accuracy is: ", logist_regression.score(X_test, y_test) * 100, "%")
        Y_pred_lr = logist_regression.predict(self.network_outputs_features)
        weights = logist_regression.coef_
        '''
        #watermark
        self.flags['tape_detect_in_watermark_channels'] = 1
        self.flags['tape_detect_in_regular_channels'] = 0
        self.load_weights()
        self.calculate_layers_outputs()
        '''
        weights_for_watermark = np.array([-0.15135212,  0.19325516,  0.14670378,  0.56882031,  0.56708015, 0  ,  0   ,  0   ])
        z = (np.dot(self.network_outputs_features, weights_for_watermark.T))
        thresh = 0.8
        Y_pred_lr_watermark = 1 / (1 + np.exp(-z))
        Y_pred_lr_watermark[Y_pred_lr_watermark < thresh] = 0
        Y_pred_lr_watermark[Y_pred_lr_watermark >= thresh] = 1



        #regular
        self.flags['tape_detect_in_watermark_channels'] = 0
        self.flags['tape_detect_in_regular_channels'] = 1
        self.load_weights()
        self.calculate_layers_outputs()
        weights_for_regular = np.array([-0.37744106,  0,  0.30320288,  0.31395254, -0.04615127,0.37799562,  0.08017115,  0])
        z = (np.dot(self.network_outputs_features, weights_for_regular.T))
        thresh = 0.99#0.997
        Y_pred_lr_regular = 1 / (1 + np.exp(-z))
        Y_pred_lr_regular[Y_pred_lr_regular < thresh] = 0
        Y_pred_lr_regular[Y_pred_lr_regular >= thresh] = 1

        self.X_test = X_test_temp
        self.Y_test = Y_test_temp

        print("Done")
        '''
        res = np.hstack((Y_pred_lr_np_calculated, np.expand_dims(self.Y_test_for_inference, axis=1)))
        comp = np.apply_along_axis(lambda x: res[:, 0] == res[:, 1], 0, res)
        acc = np.sum(comp[:, 0]) / X_test.shape[0] * 100
        print("LR Accuracy:", acc)
        '''
    def confusion_matrix_calc(self,Y_test,Yp_pred):
        matrix = confusion_matrix(Y_test, Yp_pred)
        tn, fp, fn, tp = matrix.ravel()

        print("True negative", tn)
        print('False negative', fn)
        print(((fn) / (fn + tn)) * 100, '% false negative Fails', '\n')

        print("True positive", tp)
        print("False positive", fp)
        print(((fp) / (fp + tp)) * 100, '% false positive Fails', '\n')

        return()

if __name__ == "__main__":

    #Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/Y_wo_norm_regular_channels.npy')
    #Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_regular_channels_network_outputs2.npy')

    Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/Y_watermark_channels.npy')
    Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_watermark_channels_network_outputs.npy')
    #Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_watermark_channels_network_outputs2.npy')
    #Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/Y_thread_channels.npy')
    #Y_pred = np.load('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_thread_channels_network_outputs.npy')

    #plt.plot(Y_pred[np.where(Y_pred[:, 1] == 0)])
    #plt.plot(Y_pred[np.where(Y_pred[:, 1] == 1)])
    #plt.show()
    ypred_2 = np.zeros_like(Y_pred[:,0])
    thresh = -1000# for regular-35000#-20000 for WM# for TH 0
    tol = Y_pred.shape[0]
    for i in range (0,tol):
        if (Y_pred[i,0] > thresh):
            ypred_2[i]=1

    matrix = confusion_matrix(Y_test, ypred_2)
    tn, fp, fn, tp = matrix.ravel()

    print("True negative", tn)
    print('False negative', fn)
    print(((fn) / (fn + tn)) * 100, '% false negative Fails', '\n')

    print("True positive", tp)
    print("False positive", fp)
    print(((fp) / (fp + tp)) * 100, '% false positive Fails', '\n')

    print("Stop")
    
    ''''''

    best_TFlite_model_file_path = "C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/quantized_tflite_model.tflite"
    keras_model_file_path = "C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_watermark_channels/best_model.h5"
    X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_watermark_channels.npy').clip(0, 255).astype(np.uint8)  # clip(0, 255).astype(np.uint8)
    Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/Y_watermark_channels.npy')
    ''''''

    '''
    best_TFlite_model_file_path = "C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_regular_channels/quantized_tflite_model.tflite"
    keras_model_file_path = "C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_regular_channels/best_model.h5"
    X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_wo_norm_regular_channels.npy').clip(0, 255).astype(np.uint8)#clip(0, 255).astype(np.uint8)
    Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/Y_wo_norm_regular_channels.npy')#.clip(0, 255).astype(np.uint8)
 

    best_TFlite_model_file_path = "C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_thread_channels/quantized_tflite_model.tflite"
    keras_model_file_path = "C:/Projects/Python/Tape_detect/best_model/Tape_detect_in_thread_channels/best_model.h5"
    X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_thread_channels.npy').clip(0, 255).astype(np.uint8)  # clip(0, 255).astype(np.uint8)
    Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/Y_thread_channels.npy')  # .clip(0, 255).astype(np.uint8)
    '''
    #X_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/X_short_tape_new_sensor_09_2020.npy').clip(0, 255).astype(np.uint8)#clip(0, 255).astype(np.uint8)
    #Y_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/Y_short_tape_new_sensor_09_2020.npy')#.clip(0, 255).astype(np.uint8)

    # NO fit
    #X_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/X_rejected_folder8.npy').clip(0, 255).astype(np.uint8)#clip(0, 255).astype(np.uint8)
    #Y_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/Y_rejected_folder8.npy')#.clip(0, 255).astype(np.uint8)

    # super fit
    #X_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/X_channels_wo_Tape_bmp_format_batch2.npy').clip(0, 255).astype(np.uint8)#clip(0, 255).astype(np.uint8)
    #Y_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/Y_channels_wo_Tape_bmp_format_batch2.npy')#.clip(0, 255).astype(np.uint8)

    #X_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/X_channels_new_sensor_tape_in_the_beginning.npy').clip(0, 255).astype(np.uint8)#clip(0, 255).astype(np.uint8)
    #Y_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/Y_channels_new_sensor_tape_in_the_beginning.npy')#.clip(0, 255).astype(np.uint8)

    #X_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/X_channels_new_sensor_tape_in_the_end.npy').clip(0, 255).astype(np.uint8)#clip(0, 255).astype(np.uint8)
    #Y_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/Y_channels_new_sensor_tape_in_the_end.npy')#.clip(0, 255).astype(np.uint8)

    X_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_DSPTST117_rejected_channels.npy').clip(0, 255).astype(np.uint8)#clip(0, 255).astype(np.uint8)
    Y_test_for_inference = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_DSPTST117_rejected_channels.npy')#.clip(0, 255).astype(np.uint8)


    MO=Model_optimization(best_TFlite_model_file_path,X_test,Y_test,keras_model_file_path,X_test_for_inference,Y_test_for_inference)
    #MO.load_keras_model_weights()
    #MO.convert_array_to_c_format()
    MO.model_inference()
    #MO.load_weights()
    #MO.calculate_and_plot_confusion_matrix()
    MO.calculate_layers_outputs()
    #MO.calculate_network_outputs_for_batch_input()
    #MO.calculate_classification()
    #MO.inference()

print("Stop")