import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image

class Image_preprocessing():

    flags = {'plot_IRP':0,'plot_RP':0,'plot_wp':0,'plot_gp':0,'plot_input_image':0,'clean_image':0,'plot_output_image':0,'debug_mode':1,
             'plot_all_sub_images':1,
             'plot_IRP':1}

    def __init__(self, input_file_path,output_file_path):
        self.input_file_path = input_file_path
        self.output_file_path = output_file_path

    def read_image (self):
        self.input_image = cv2.imread(self.input_file_path, 0)
        # bill_img = cv2.cvtColor(bill_img, cv2.COLOR_BGR2GRAY)
        if (self.flags['plot_input_image']):
            plt.imshow(self.input_image, cmap='gray', vmin=0, vmax=255)
            plt.title("input_image")
            plt.show()
        return()

    def parse_input_image(self):
        #bill_img_arr = np.array(bill_img)
        self.input_image_rows, self.input_image_cols = self.input_image.shape
        num_of_line_per_sub_image = int(self.input_image_rows / 8)
        self.chunk_of_img = np.zeros((8, num_of_line_per_sub_image, self.input_image_cols))
        chunk_of_img_idx = 0
        for row in range(0, 8):
            chunk_of_img_idx = 0
            for i in range(row, self.input_image_rows, 8):
                self.chunk_of_img[row, chunk_of_img_idx, :] = self.input_image[i, :]
                chunk_of_img_idx += 1

        if (self.flags['plot_all_sub_images']):
            fig, axs = plt.subplots(8, 1)
            #fig.suptitle('        Divided frame ', fontsize=12)

            axs[0].imshow(self.chunk_of_img[0, :, :], cmap='gray', vmin=0, vmax=255)
            axs[0].set_title('Output 0 - IRP', fontsize=10)

            axs[1].imshow(self.chunk_of_img[1, :, :], cmap='gray', vmin=0, vmax=255)
            axs[1].set_title('Output 1 - Red_P', fontsize=10)

            axs[2].imshow(self.chunk_of_img[2, :, :], cmap='gray', vmin=0, vmax=255)
            axs[2].set_title('Output 2 - WP', fontsize=10)

            axs[3].imshow(self.chunk_of_img[3, :, :], cmap='gray', vmin=0, vmax=255)
            axs[3].set_title('Output 3 - Green_P', fontsize=10)

            axs[4].imshow(self.chunk_of_img[4, :, :], cmap='gray', vmin=0, vmax=255)
            axs[4].set_title('Output 4 - IRP', fontsize=10)

            axs[5].imshow(self.chun , cmap='gray', vmin=0, vmax=255)
            axs[5].set_title('Output 5 - Red_P', fontsize=10)

            axs[6].imshow(self.chunk_of_img[6, :, :], cmap='gray', vmin=0, vmax=255)
            axs[6].set_title('Output 6 - UVR', fontsize=10)

            axs[7].imshow(self.chunk_of_img[7, :, :], cmap='gray', vmin=0, vmax=255)
            axs[7].set_title('Output 7 - Blue P', fontsize=10)

            fig.tight_layout()

            print("mean 0" ,np.mean(self.chunk_of_img[0, :, :]) )
            print("mean 1", np.mean(self.chunk_of_img[1, :, :]))
            print("mean 2", np.mean(self.chunk_of_img[2, :, :]))
            print("mean 3", np.mean(self.chunk_of_img[3, :, :]))
            print("mean 4", np.mean(self.chunk_of_img[4, :, :]))
            print("mean 5", np.mean(self.chunk_of_img[5, :, :]))
            print("mean 6", np.mean(self.chunk_of_img[6, :, :]))
            print("mean 7", np.mean(self.chunk_of_img[7, :, :]))



            plt.show()

            # Debug
            img = self.chunk_of_img[2, :, :]
            img = np.expand_dims(img,axis=2)
            img1 = ((img - img.min()) / (img.max() - img.min())) * 255 #*0.6
            #rgb_img[:,:, :0] =img

            img = self.chunk_of_img [3,:, :]
            img = np.expand_dims(img, axis=2)
            img2 = ((img - img.min()) / (img.max() - img.min())) * 255 #*0.3

            #rgb_img[:,:, 2] =img

            img = self.chunk_of_img [5,:, :]
            img = np.expand_dims(img, axis=2)
            img3 = ((img - img.min()) / (img.max() - img.min())) * 255 #*0.1
            #rgb_img[:,:, 3] =img

            img_rgb = np.concatenate((img1,img2,img3), axis=2)
            ##backtorgb = cv2.cvtColor(img_rgb, cv2.COLOR_GRAY2RGB)
            #gray_three = cv2.merge([img1, img2, img3])
            #cv2.imshow('image', gray_three)
            #plt.imshow(gray_three, cmap='hot')
            #plt.imshow(img_rgb, cmap='hot')

            rgb_sum = np.sum(img_rgb,axis=2)
            plt.imshow(rgb_sum, cmap='gray')
            plt.show()
            print(np.mean(rgb_sum))
            ''''''
            # Low resolution
            fig = plt.figure(frameon=False)
            img = self.chunk_of_img[7, :, :]
            scale_percent = 16  # percent of original size
            width = int(img.shape[1] * scale_percent / 100)
            height = int(img.shape[0] * scale_percent / 100)
            dim = (width, height)
            # resize image
            resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
            plt.imshow(resized, cmap='gray')
            plt.show()


        return()


    def concatenate_sub_images_to_new_image(self):
        self.new_image_arr = np.zeros_like(self.input_image)
        if (self.flags['clean_image']):
            for row in range(0, 8):
                cv2.imwrite(path_out, self.chunk_of_img[row, :, :])
                image = cv2.imread(path_out)
                ing_row_idx = 0
                clean = cv2.fastNlMeansDenoisingColored(image, None, 20, 20, 7, 21)
                for j in range(0, 124):
                    ing_row_idx = row + 8 * j
                    self.new_image_arr[ing_row_idx, :] = clean[j, :, 1]
        else:
            for chunk in range(0, 8):
                for j in range(0, 124):
                    ing_row_idx = chunk + 8 * j
                    self.new_image_arr[ing_row_idx, :] = self.chunk_of_img[chunk, j, :]

        if (self.flags['plot_output_image']):
            fig, axarr = plt.subplots(1, 2)
            axarr[0].imshow(self.input_image, cmap='gray', vmin=0, vmax=255)
            axarr[0].set_title("input_image")
            axarr[1].imshow(self.new_image_arr, cmap='gray', vmin=0, vmax=255)
            axarr[1].set_title("output_image")
            plt.show()


        if (self.flags['debug_mode']):
            print('Debug mode')
        else:
            cv2.imwrite(self.output_file_path, self.chunk_of_img[row, :, :])

        if (self.flags['plot_all_sub_images']):
            fig, axs = plt.subplots(8, 1)
            fig.suptitle('        Divided frame ', fontsize=12)

            axs[0].imshow(self.chunk_of_img[0, :, :], cmap='gray', vmin=0, vmax=255)
            axs[0].set_title('Output 0 - IRP', fontsize=10)

            axs[1].imshow(self.chunk_of_img[1, :, :], cmap='gray', vmin=0, vmax=255)
            axs[1].set_title('Output 1 - RP', fontsize=10)

            axs[2].imshow(self.chunk_of_img[2, :, :], cmap='gray', vmin=0, vmax=255)
            axs[2].set_title('Output 2 - WP', fontsize=10)

            axs[3].imshow(self.chunk_of_img[3, :, :], cmap='gray', vmin=0, vmax=255)
            axs[3].set_title('Output 3 - GP', fontsize=10)

            axs[4].imshow(self.chunk_of_img[4, :, :], cmap='gray', vmin=0, vmax=255)
            axs[4].set_title('Output 4 - IRP', fontsize=10)

            axs[5].imshow(self.chunk_of_img[5, :, :], cmap='gray', vmin=0, vmax=255)
            axs[5].set_title('Output 5 - RP', fontsize=10)

            axs[6].imshow(self.chunk_of_img[6, :, :], cmap='gray', vmin=0, vmax=255)
            axs[6].set_title('Output 6 - UVR', fontsize=10)

            axs[7].imshow(self.chunk_of_img[7, :, :], cmap='gray', vmin=0, vmax=255)
            axs[7].set_title('Output 7 - RP', fontsize=10)

            fig.tight_layout()

            plt.show()

        if (self.flags['plot_IRP']):
            plt.imshow(self.chunk_of_img[0, :, :], cmap='gray', vmin=0, vmax=255)
            plt.title('Output 0 - IRP', fontsize=10)
            F = plt.gcf()
            fig_size = F.get_size_inches()
            #F.set_size_inches(30, 8)
            sizefactor = 2  # Set a zoom factor
            # Modify the current size by the factor
            plt.gcf().set_size_inches(sizefactor * fig_size)
            #F.set_size_inches(fig_size[0] * 16, fig_size[1] * 16,forward=True)  # Set forward to True to resize window along with plot in figure.
            #plt.subplots_adjust(left=0.16, bottom=0.19, top=0.82)
            plt.show()


        return()

    def concatenate_sub_images_for_ocr(self):
        return

    def run(self):
        self.read_image()
        self.parse_input_image()
        self.concatenate_sub_images_to_new_image()
        return()

if __name__ == "__main__":
    input_filename ='C:/Projects/Python/Dataset/INR/inr_100_side1.bmp'
    input_filename = 'C:/Projects/Python/Dataset/Tape_and_thread/Dataset3/New_sensor_12_2020/0002.bmp'
    path_out ='C:/Projects/Python/Dataset/INR/00_clean.bmp'
    path_out2 ='C:/Projects/Python/Dataset/INR/0_clean.bmp'

    clean_img = Image_preprocessing(input_filename,path_out)
    clean_img.run()





'''
# Image for OCR
Buff_RP  = new_image_arr[994:1135]
buff_GP  = new_image_arr[426:567]
buff_UVR = new_image_arr[852:993]
buff_WP  = new_image_arr[284:425]

OCR_FRAME = np.zeros((4*Buff_RP.shape[0],Buff_RP.shape[1]))
for i in range(0,Buff_RP.shape[0]):
    OCR_FRAME[i*4,:] = Buff_RP[i,:]
    OCR_FRAME[i*4+1,:] = buff_GP[i,:]
    OCR_FRAME[i*4+2,:] = buff_UVR[i,:]
    OCR_FRAME[i*4+3,:] = buff_WP[i,:]

cv2.imwrite('RP_frame.bmp', Buff_RP)
plt.imshow(Buff_RP,cmap='gray', vmin=0, vmax=255)
plt.title("OCR_FRAME")
plt.show()




# extract additional data
additional_data_buff = np.zeros((1136,64))
byte_count = 0
word_count = 0
additional_data_buff_row =0
additional_data_buff_col =0
row_idx = 0
for row in bill_img:
    #print("line 1584" ,line[1584])
    #print("line 1585", line[1585]/16)
    byte_count = row[1648]
    if (byte_count != 255):
        print('byte_count',byte_count)
        word_count = word_count + byte_count
        print('word_count', word_count)
        if (word_count ==64):
            word_count = 0
            additional_data_buff_row +=1
            print('additional_data_buff_row',additional_data_buff_row)

        for i in range(1649,(1649+byte_count)):
            print("additional_data_buff_col", additional_data_buff_col)
            additional_data_buff[additional_data_buff_row,additional_data_buff_col]= bill_img_arr[row_idx,i]
            if (additional_data_buff_col == 63):
                additional_data_buff_col = 0
            else:
                additional_data_buff_col +=1


            print("i", i)
            print ("row_idx",row_idx)
            print ("bill_img_arr",bill_img_arr[row_idx,i])
    row_idx += 1
'''