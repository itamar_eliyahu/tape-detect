# # SURF (Speeded-Up Robust Features)


# ## Import resources and display image

import cv2
import matplotlib.pyplot as plt
import numpy as np

# Load the image
img = cv2.imread('C:/Projects/Python/Dataset/0205.bmp',0)


sift = cv2.xfeatures2d.SIFT_create()
surf = cv2.xfeatures2d.SURF_create()
orb = cv2.ORB_create(nfeatures=1500)