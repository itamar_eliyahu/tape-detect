import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches


def txt_to_list(input_file_path):
    lines_list = []
    with open(input_file_path, 'r') as path:
        lines = [line.strip().split(' ') for line in path]
    for line in lines:
        lines_list.append(line)
    return(np.array(lines_list))

def txt_to_image(input_file_path):# txt with various images dimension
    images = []
    with open(input_file_path, 'r') as path:
        lines = [line.strip().split(' ') for line in path]
    for line in lines:
        # High res
        h = int(line[-1])
        w = int(line[-2])
        # Low res
        #w = int(line[-1])
        #h = int(line[-2])
        img_flat = np.array(line[:-2], dtype=np.int32)
        img = img_flat.reshape(h, w)
        img = ((img - img.min()) / (img.max() - img.min())) * 255
        img = img.astype(np.int32)
        #img = np.rot90(img).astype(np.int32)
        #img = img[:90, :70]
        #if ((img.shape[0] != 90) or (img.shape[1] != 70)):
            #img = np.pad(img, ((0, 90 - img.shape[0]),(0, 70 - img.shape[1])), 'constant',constant_values=(0, 0))
        images.append(img)

    return(np.array(images))

if __name__ == "__main__":


    input_file_side1 = 'C:/Projects/Python/debug/High_res/tape.txt'
    #input_file_side1 = 'C:/Projects/Python/debug/debug_high_res_images_500_side1_with_h_w_s0.txt'

    imges_side1 = txt_to_image(input_file_side1)
    img1 = imges_side1[-1]
    img2 = imges_side1[-2]
    fig, ax = plt.subplots(2, 1)
    ax[0].imshow(img1,cmap='gray')
    ax[0].set_title('with')
    ax[1].imshow(img2,cmap='gray')
    ax[1].set_title('without')
    plt.show()
    plt.plot(img1[:, 200])
    plt.plot(img2[:, 200])