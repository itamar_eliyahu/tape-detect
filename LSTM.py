import torch
#from torch import nn
#import numpy as np
from torch.utils import data
#import torchvision.transforms as transforms
#from torch import utils



class tape_ch_dataset_gen(data.Dataset):
  'Characterizes a dataset for PyTorch'
  def __init__(self, samples_list, targets_list,transform =transforms.ToTensor() ):
        'Initialization'
        self.targets_list = targets_list
        self.samples_list = samples_list
        self.transform = transform

  def __len__(self):
        'Denotes the total number of samples'
        return len(self.samples_list)

  def __getitem__(self, index):
        'Generates one sample of data'

        return self.samples_list[index,:,:], self.targets_list[index]  #for SoftmxWith3D loss



