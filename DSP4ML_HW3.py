import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as LA

L=15
n=10000

x = np.random.normal(loc=0.0 , scale=np.sqrt(1./L), size=L)
x = x/ np.linalg.norm(x,ord=1)

#
circular_shift_rho = np.random.uniform(low=0,high=1,size=L)
rho_norm = circular_shift_rho / np.linalg.norm(circular_shift_rho,ord=1)
l_i = np.random.choice(L,n,p=rho_norm)

sigma_list = np.linspace(0.1,10,5)

y_sigma_list=[]
for sigma in sigma_list:
    y= np.zeros((n,L))
    for i in range(n):
        eps = np.random.normal(loc=0.0,scale=sigma,size=L)
        y[i,:] = np.roll(x,l_i[i]) + eps
    y_sigma_list.append(y)


#EM
weights = np.random.uniform(low=0,high=1,size=(n,L))
weights = weights/np.sum(weights,axis=1)[:,np.newaxis]
rho_hat=np.ones(L)/L
Xt=np.random.uniform(low=0,high=1,size=L)

iterations=3
for o in range(iterations):
    #E step
    y_nrm2 = np.linalg.norm(y,axis=1)
    for l in range(L):
        Rl_dot_Xt_nrm2= np.linalg.norm(np.roll(Xt,l))
        for i in range(n):
            mse_cross_term = np.linalg.norm(2*np.dot(y[i,:],np.roll(Xt,l)))
            mse= y_nrm2[l]* Rl_dot_Xt_nrm2 - mse_cross_term
            weights[i,l]=np.exp(-1 * 0.5 * (1./sigma**2)*mse)*rho_hat[l]
    weights_normalized = weights/np.sum(weights,axis=1)[:,np.newaxis]

    # M step
    xt_i_l=0
    rho_hat = np.zeros(L)
    for l in range(L):
        for i in range(n):
            rho_hat +=  weights[i,l]
            xt_i_l += np.dot(weights_normalized[i,l],np.roll(y[i,:],-1*l))
    rho_hat = rho_hat / np.linalg.norm(rho_hat,ord=1)

    #final error
    error_iter_l=[]
    for l in range(L):
        error_iter_l.append(np.linalg.norm(np.roll(Xt,l)-x)**2)
    error_iter = np.min(error_iter_l) * np.linalg.norm(x)
    print(error_iter)


#Moments

m1 = np.mean(y,axis=0)
m2 = np.zeros((L,L))

for i in range (n):
    m2 +- np.matmul(np.reshape(y[i,:],(L,1)) , np.reshape(y[i,:],(L,1)).T)
m2 = m2/ n+(sigma**2)*np.ones((L,L))

from scipy.linalg import dft
F = dft(L)
P = L * np.diagonal(np.matmul(np.matmul(F,m2),1/F))
p = P**(-0.5)
Dp = np.diag(p)
Q=np.real(np.matmul(np.matmul(1/F,Dp),F))
M2=np.matmul(np.matmul(Q,m2),np.conj(Q))

from sympy import *
M2_Matrix = Matrix(M2)
eigenvals = M2_Matrix.eigenvals()