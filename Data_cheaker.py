import numpy as np
import matplotlib.pyplot as plt


class Data_checker():
    flags = {'plot_sample':0,'save_dataframe_to_file':1,}

    def __init__(self, cnannels_file_path,path_for_clean_file):
        self.channels_file = np.load(cnannels_file_path)#[:,:,:100]
        #self.channels_file =  self.channels_file.reshape(self.channels_file.shape[0],self.channels_file.shape[2])

        #self.X = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_regular_channels_clean.npy')
        #self.Y = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/Y_regular_channels_clean.npy')
        self.path_for_clean_file = path_for_clean_file


    def check_data_with_corr(self):
        '''
        samples_file = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/da6_X_regular_channels_wo_tape.npy')
        samples_file =samples_file.reshape(samples_file.shape[0], samples_file.shape[2])
        kernel = samples_file[295][:100]
        '''

        samples_file = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/da6_X_watermark_channels_wo_tape.npy')
        samples_file =samples_file.reshape(samples_file.shape[0], samples_file.shape[2])
        samples_file_peak= samples_file[38:53,:]
        kernel = np.mean(samples_file_peak, axis=0)[:100]

        cnv = np.zeros((self.channels_file.shape[0],1))
        corr =np.zeros((self.channels_file.shape[0],1))
        for i in range(0,self.channels_file.shape[0]):
            cnv[i, 0] = np.convolve(kernel, self.channels_file[i], 'valid')
            corr[i, 0] = np.correlate(kernel, self.channels_file[i])
        cnv -= np.mean(cnv)
        cnv /=1e6
        cnv_thresh =np.where(cnv>0.45)

        corr -= np.mean(corr)
        corr /=1e6
        corr_thresh =np.where(corr>0.85)

        self.channels_file = np.delete(self.channels_file,[cnv_thresh[0]], axis=0)
        #np.save(self.path_for_clean_file,self.channels_file)

        return()

    def check_data_with_area(self):
        channels_area = np.cumsum(self.channels_file, axis=1)[:, -1]
        channels_area_avg = np.mean(channels_area)

        channels_area_diff_from_avg = channels_area - channels_area_avg
        channels_area_diff_from_avg = np.ma.array(channels_area_diff_from_avg, mask=False)

        num_of_checks = 5000
        diff_and_ind = np.zeros((num_of_checks, 2))
        i=0
        max_diff=10000
        #area_thresh = 2700#for WM channels
        #area_thresh = 1100  # for Thread channels
        area_thresh = 1400#for regular channels
        while  (max_diff > area_thresh) :
        #for i in range(0,num_of_checks)   :
            max_diff = channels_area_diff_from_avg.max()
            max_diff_index = np.where(channels_area_diff_from_avg == max_diff)
            channels_area_diff_from_avg.mask[max_diff_index] = True
            diff_and_ind[i, 0] = max_diff
            diff_and_ind[i, 1] = max_diff_index[0][0]
            i+=1
        ind = (diff_and_ind[:, 1])[:i].astype(np.int)
        self.channels_file = np.delete(self.channels_file, [ind],axis=0)
        np.save(self.path_for_clean_file,self.channels_file)
        return()

    def check_data_with_area2(self):
            #Area below the signal
            channels_area = np.cumsum(self.channels_file, axis=1)[:, -1]
            channels_area_avg = np.mean(channels_area)
            channels_area_diff_from_avg = channels_area - channels_area_avg

            min_sort_indices = np.argsort(channels_area_diff_from_avg)
            channels_area_diff_from_avg =channels_area_diff_from_avg[min_sort_indices]
            diff_and_ind = np.vstack((min_sort_indices,channels_area_diff_from_avg)).T
            ind_to_clean_low =np.where(diff_and_ind[:,1]>=1000)
            ind_to_clean_low = diff_and_ind[ind_to_clean_low[0]][:,0]
            ind_to_clean_high = np.where(diff_and_ind[:, 1] <= -1300)
            ind_to_clean_high = diff_and_ind[ind_to_clean_high[0]][:, 0]
            ind_to_clean = np.concatenate((ind_to_clean_low.astype(np.int),ind_to_clean_high.astype(np.int)))
            self.channels_file = np.delete(self.channels_file, ind_to_clean,axis=0)
            np.save(self.path_for_clean_file, self.channels_file)
            #plt.plot(diff_and_ind[:,1])
            #plt.plot(self.channels_file[40655])
            '''
            samples_file = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/regular_channels_with_narror_gd_tape.npy')
            samples_file = samples_file.reshape(samples_file.shape[0], samples_file.shape[2])
            kernel = samples_file[8,26:34]
            match_filt = np.zeros((self.channels_file.shape[0], 1))
            for i in range(0, self.channels_file.shape[0]):
                match_filt[i, 0] = np.correlate(kernel, self.channels_file[i]).max()
                #match_filt[i, 0] = np.convolve(kernel, self.channels_file[i], 'valid').max()
            sort_max_arg = np.argsort(match_filt,axis=0)[::-1]
            short_tape = self.channels_file[sort_max_arg]
            np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/filtered_short_tape.npy',short_tape)
            '''
            '''
            fig, ax = plt.subplots()
            for i in range(0,self.channels_file.shape[0]):
                ax.cla()
                ax.plot(self.channels_file[i])
                #ax.plot(short_tape[i])
                ax.set_title("frame {}".format(i))
                #plt.show()
                plt.pause(0.3)
                iyi=0
                #plt.colse()
                #input_key2 = input("Press Y to save image , N to rename , X to delete : ")
                #if (input_key2 == 'Y'):
                    #continue
            '''

            return ()

def  calc_conf_matrix(sum_with,sum_wo):
    from sklearn.metrics import confusion_matrix

    x_wo = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/regular_channels_wo_tape_norm.npy')
    x_with = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/regular_channels_tape_in_the_middle_norm.npy')

    #sum_with = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/sum_with_thr_50_shift_22.npy')
    y_with = np.ones((sum_with.shape[0], 1))
    #sum_wo = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/sum_wo_thr_50_shift_22.npy')
    y_wo = np.zeros((sum_wo.shape[0], 1))
    x_test = np.concatenate((sum_with, sum_wo))
    y_test = np.concatenate((y_with, y_wo))
    y_pred = np.zeros_like(y_test)
    thresh = 400
    fp_total = 1
    while (fp_total > 0.2):
        y_pred[x_test < thresh] = 0
        y_pred[x_test >= thresh] = 1

        matrix = confusion_matrix(y_test, y_pred)
        print(matrix)
        tn, fp, fn, tp = matrix.ravel()
        fp_total = (fp / (tn + fp)) * 100
        fn_total = (fn / (fn + tp)) * 100
        if (fp_total > 0.2):
            thresh +=100
        else:
            thresh -= 100
        print("False positive %",fp_total )
        print("False Negative %",fn_total)
        print(thresh)
    print(((tn + tp) / (fn + fp + tn + tp)), '% Accuracy/n')
    return fp_total, fn_total

if __name__ == "__main__":

    #img = np.genfromtxt('C:/Projects/Python/Tape_detect/ch_as_image.txt')
    #plt.imshow(img,cmap='gray')
    #plt.show()

    x_wo = np.genfromtxt('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/sw_records/watermark_channels_2005_without_tape_raw_16.txt')
    x_wo = (x_wo[:, :100])
    x_zeros = np.zeros((x_wo.shape[0], 1))
    xwo = np.hstack([x_wo, x_zeros])
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Datasets/X_watermark_channels_2005_without_tape_raw_16.npy',xwo[:, :-1])
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Datasets/Y_watermark_channels_2005_without_tape_raw_16.npy',xwo[:, -1])

    x_wo = np.genfromtxt('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/sw_records/regular_channels_2005_without_tape_raw_16.txt')
    x_wo = (x_wo[:, :100])
    x_zeros = np.zeros((x_wo.shape[0], 1))
    xwo = np.hstack([x_wo, x_zeros])
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Datasets/X_regular_channels_2005_without_tape_raw_16.npy',xwo[:, :-1])
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Datasets/Y_regular_channels_2005_without_tape_raw_16.npy',xwo[:, -1])



    #x=np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Datasets/X_regular_channels_new_norm_2005_2.npy')
    #y=np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Datasets/Y_regular_channels_new_norm_2005_2.npy')


'''    x_with = np.genfromtxt('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/sw_records/regular_channels_2005_with_tape_raw_11.txt')
    x_with = (x_with[:, :100])
    x_ones = np.ones((x_with.shape[0], 1))
    xwith = np.hstack([x_with, x_ones])

    x_wo = np.genfromtxt('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/sw_records/regular_channels_2005_without_tape_raw_11.txt')
    x_wo = (x_wo[:, :100])
    x_zeros = np.zeros((x_wo.shape[0], 1))
    xwo = np.hstack([x_wo, x_zeros])

    ds = np.vstack([xwith, xwo])

    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Datasets/X_regular_channels_new_norm_2005_2.npy',ds[:, :-1])
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2005/Datasets/Y_regular_channels_new_norm_2005_2.npy',ds[:, -1])
'''
'''
    x_with = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/regular_channels_with_tape_gmm_norm_128.npy')
    # x_with = x_with.reshape(x_with.shape[0],x_with.shape[2])
    x_with = (x_with[:, :100]).astype(np.int32)
    # x_with = np.hstack((x_with[:, :100],np.zeros((x_with.shape[0],5))))
    x_ones = np.ones((x_with.shape[0], 1))
    xwith = np.hstack([x_with, x_ones])

    x_wo = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/regular_channels_without_tape_gmm_norm_128.npy')
    # x_wo = x_wo.reshape(x_wo.shape[0],x_wo.shape[2])
    x_wo = (x_wo[:, :100]).astype(np.int32)
    # x_wo = np.hstack((x_wo[:, :100], np.zeros((x_wo.shape[0], 5))))
    x_zeros = np.zeros((x_wo.shape[0], 1))
    xwo = np.hstack([x_wo, x_zeros])

    ds = np.vstack([xwith, xwo])
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_regular_channels_new_norm_128.npy',ds[:, :-1])
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_new_norm_128.npy',ds[:, -1])'''