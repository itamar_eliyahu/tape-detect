import numpy as np
import matplotlib.pyplot as plt
import os

def plot_step_size_per_channel(txt_file_path):
    x = np.genfromtxt(txt_file_path)
    fig, axarr = plt.subplots(2, 9 ,figsize=(8, 10))
    j=0
    thresholds = np.zeros(18)
    for i in range(0,18):
        temp = x[x[:,1]==i]
        if j==9:
            j=0
        n,b = np.histogram(temp[:,0],bins=50)
        thresholds[i]=(((b[-1])))
        axarr[int(i/9),int(j)].hist(temp[:,0],bins=50)
        axarr[int(i / 9), int(j)].set_title('Ch:{},{},{},{}'.format(i,temp.shape[0],int(b[-1]),int(n[-1])))
        j += 1
    plt.show()
    np.savetxt('C:/Projects/Python/Tape_detect/debug/vertical_thresholds.txt',(thresholds),fmt='%i')

def plot_above_avg_per_channel():
    x =np.genfromtxt('C:/Projects/Python/Tape_detect/debug/vertical_tape/debug_file_vertical_tape.txt')
    np.save('C:/Projects/Python/Tape_detect/debug/vertical_tape/debug_file_vertical_tape.npy',x)

    #x =  np.load('C:/Projects/Python/Tape_detect/debug/vertical_tape/debug_file_vertical_tape.npy')
    #x =  np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_ver_tape_avg_with_vert.txt')

    values_per_ch = np.zeros((x.shape[0],18))# step and  step above step_avg per channel
    fig, axarr = plt.subplots(2, 9 ,figsize=(8, 10))
    j=0
    index_per_ch = np.zeros(18).astype(np.int)
    for i in range(0,x.shape[0]):
        ch = int(x[i,0])
        values_per_ch[index_per_ch[ch],ch] = x[i,1]  / x[i,2]
        index_per_ch[ch] += 1
    for i in range(0,9):
        temp = np.where(values_per_ch[:,i] !=0)[0]
        num_of_active_val= temp.shape[0]
        axarr[0,int(i)].plot(values_per_ch[:num_of_active_val,i])
        axarr[0, int(i)].set_title('Ch:{}'.format(i))
    for i in range(9,18):
        temp = np.where(values_per_ch[:,i] !=0)[0]
        num_of_active_val= temp.shape[0]
        axarr[1,int(i-9)].plot(values_per_ch[:num_of_active_val,i])
        axarr[1, int(i-9)].set_title('Ch:{}'.format(i))
    plt.show()
    return ()

def calc_threshold():
    ratio_list =[]
    x_stat = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/vertical_tape/debug_file_vertical_tape_stat_wo.txt',delimiter=' ', dtype=np.str)
    x_raw = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/vertical_tape/debug_file_vertical_tape_wo.txt')
    for i in range (0,x_stat.shape[0]):
        #ratio_list.append( (x_stat[i,7].astype(np.int)/100)*  x_stat[i,3].astype(np.int))
        ratio_list.append((x_stat[i, 3].astype(np.int) ) / x_stat[i, 7].astype(np.int))
    plt.plot(ratio_list)
    plt.show()

def plot_vert_tape():
    #x_stat = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_thread.txt',delimiter=',', dtype=np.str)

    x_raw = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/vertical_tape/debug_file_vertical_tape.txt')
    #np.save('C:/Projects/Python/Tape_detect/debug/vertical_tape/debug_file_vertical_tape_raw.npy',x_raw)
    x_stat = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/vertical_tape/debug_file_vertical_tape_stat_wo.txt',delimiter=' ', dtype=np.str)
    #thresh = x_stat[:,3]
    #plt.hist(thresh)
    #plt.show()
    #np.save('C:/Projects/Python/Tape_detect/debug/vertical_tape/debug_file_vertical_tape.npy',x_stat)
    for i in range(0,x_raw.shape[0]):
        plt.plot(x_raw[i])
        #plt.title( 'Pixels above threshold:  ' + str(x_stat[i,3])+ ' SN: '+ str(x_stat[i,5]))
        plt.savefig((os.path.join('C:/Projects/Python/Tape_detect/debug/vertical_tape/vertical_tape_test',str(i))))
        plt.close()

    #x_raw = np.load('C:/Projects/Python/Tape_detect/debug/vertical_tape/debug_file_vertical_tape_raw.npy')
    #x_stat = np.load('C:/Projects/Python/Tape_detect/debug/vertical_tape/debug_file_vertical_tape.npy')
    #x_fact=x_stat[:,1] / x_stat[:,2]
    #idx = np.where(x_fact>1.0)
    #x_above_thresh_raw = x_raw[idx,:]
    #x_above_thresh_stat = x_stat[idx, :]
    #for i in range(0,x_above_thresh_raw.shape[1]):
        #plt.plot(x_above_thresh_raw[0, i, :])
        #plt.title('curren AVG: ' + str(x_above_thresh_stat[0,i,2]) + ' Current Step: ' + str(x_above_thresh_stat[0,i,1])  + '  AVG factor = 30%')
        #plt.savefig((os.path.join('C:/Projects/Python/Tape_detect/debug/vertical_tape/vertical_tape_test',str(i))))
        #plt.close()
        #plt.show()


def plot_step_size():
    x_wo = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_ver_tape_without.txt')
    x_with = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_ver_tape_with.txt')

    plt.hist(x_wo,bins=50,label='vertical tape')
    plt.hist(x_with,bins=50,label='without tape')
    plt.legend()
    plt.show()

def calc_min_acceptable_area_per_channel(txt_file_path):
    #step_raw = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_step_raw.txt')
    #np.save('C:/Projects/Python/Tape_detect/debug/debug_file_step_raw.npy',step_raw)
    step_raw = np.load('C:/Projects/Python/Tape_detect/debug/debug_file_step_raw.npy' )
    raw_arr = np.zeros((18,step_raw.shape[0],step_raw.shape[1]))
    #x = np.genfromtxt(txt_file_path).astype(np.int)
    #np.save('C:/Projects/Python/Tape_detect/debug/x.npy', x)
    x = np.load('C:/Projects/Python/Tape_detect/debug/x.npy')
    area_per_ch = np.zeros((x.shape[0],18))
    step_per_ch = np.zeros((x.shape[0], 18))
    row_index_per_col=np.zeros(18).astype(np.int)
    raw_arr_line_index = np.zeros(step_raw.shape[0]).astype(np.int)
    for i in range(0,x.shape[0]):
        ch = x[i,0]
        area_per_ch[row_index_per_col[ch],ch]= x[i,1]
        step_per_ch[row_index_per_col[ch],ch]= x[i,2]
        raw_arr[ch,row_index_per_col[ch],:] = step_raw[i,:]
        row_index_per_col[ch] +=1
        #raw_arr_line_index[ch] +=1
    col =6
    temp = np.where(area_per_ch[:, col] != 0)[0]
    num_of_active_lines = temp.shape[0]
    arr_sort=np.argsort(area_per_ch[:num_of_active_lines,col])
    pp= area_per_ch[arr_sort,col]
    plt.plot(raw_arr[col,17,:])
    return()


def check_vert():
    vert_file = np.genfromtxt('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_with_tape_raw_vertical.txt')

    #vert_file = np.genfromtxt('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_without_tape_raw_3.txt')
    #vert_file = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_without_tape_raw_3.npy')


    thresh_val = 150
    thresh_amount = 65#60
    lst=[]
    std_list =[]
    for i in range (0,vert_file.shape[0]):
        num_of_points_above_thresh =0
        for j in range (0,100):
            if (vert_file[i,j] > thresh_val):
                num_of_points_above_thresh +=1
        if (num_of_points_above_thresh >  thresh_amount):
            #std_list.append(np.mean(vert_file[i,:]))
            #plt.plot(vert_file[i,:])
            #plt.title('i: '+str(i)+' num_of point above thresh:'+str(num_of_points_above_thresh))
            #plt.show()
            #plt.savefig((os.path.join('C:/Projects/Python/Tape_detect/debug/vertical_tape/without_tape/',str(i))))
            #plt.close()
            lst.append(vert_file[i,:])
    vert_sig=np.array(lst)
    std_list_arr = np.array(std_list)
    #np.save('C:/Projects/Python/Tape_detect/debug/vertical_tape/vert_sig.npy', vert_sig)
    ker = np.average(vert_sig,axis=0)
    #np.save('C:/Projects/Python/Tape_detect/debug/vertical_tape/kernel.npy',ker)
    #ker2 = np.poly1d(vert_sig)
    plt.plot(ker)
    #plt.plot(ker2)
    plt.show()

def match_filt():
    kernel = np.load('C:/Projects/Python/Tape_detect/debug/vertical_tape/kernel.npy').astype(np.int)
    vert_file = np.genfromtxt('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_with_tape_raw_wide_vert.txt')
    #x_wo = np.genfromtxt('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_without_tape_raw_3.txt')
    #np.save('C:/Projects/Python/Tape_detect/debug/vertical_tape/x_wo.npy',x_wo)
    x_wo = np.load('C:/Projects/Python/Tape_detect/debug/vertical_tape/x_wo.npy')
    vert_sig=np.load('C:/Projects/Python/Tape_detect/debug/vertical_tape/vert_sig.npy')

    x= x_wo
    step_cnv1 = np.zeros((x.shape[0],1))
    for i in range(0, x.shape[0]):
        step_cnv1[i,:] = np.convolve(x[i , :105], kernel, mode='valid')
        t=0
        #for j in range(0,105):
            #t += x[i,j] * kernel[j]
        #step_cnv1[i, :] = t
    np.save('C:/Projects/Python/Tape_detect/debug/vertical_tape/step_cnv_x_wo.npy',step_cnv1)

    x= vert_file
    step_cnv2 = np.zeros((x.shape[0],1))
    for i in range(0, x.shape[0]):
        step_cnv2[i,:] = np.convolve(x[i , :105], kernel, mode='valid')
        #t=0
        #for j in range(0,105):
            #t += x[i,j] * kernel[j]
        #step_cnv2[i, :] = t
    np.save('C:/Projects/Python/Tape_detect/debug/vertical_tape/step_cnv_x_with_vert.npy',step_cnv2)

    x= vert_sig
    step_cnv3 = np.zeros((x.shape[0],1))
    for i in range(0, x.shape[0]):
        step_cnv3[i,:] = np.convolve(x[i , :105], kernel, mode='valid')
        #t=0
        #for j in range(0,105):
            #t += x[i,j] * kernel[j]
        #step_cnv3[i, :] = t
    np.save('C:/Projects/Python/Tape_detect/debug/vertical_tape/step_cnv_x_with_only_vert.npy',step_cnv3)


if __name__ == "__main__":

    #match_filt()
    #check_vert()
    calc_threshold()
    #plot_vert_tape()
    #plot_above_avg_per_channel()
    #calc_min_acceptable_area_per_channel('C:/Projects/Python/Tape_detect/debug/debug_file_ver_tape_area.txt')
    # plot_step_size()
    #plot_step_size_per_channel('C:/Projects/Python/Tape_detect/debug/step_wo_tape_M3.txt')



    #algo_1 = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_step_algo_1.txt')
    #algo_2 = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_step_algo_2.txt')
    #step_raw = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_step_raw_2.txt')
    #plt.plot(algo_1,label="Algo 1")
    #plt.plot(algo_2,label="Algo 2")
    #plt.legend()
    #plt.show()
    ''' '''


    '''
    #x_wo = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_ver_tape_avg.txt')
    x_wo = np.genfromtxt('C:/Projects/Python/Tape_detect/debug/debug_file_ver_tape_thr_ch_8.txt')
    precent = x_wo[:,0] / x_wo[:,1]
    print("precent min",precent.min())
    print("precent max", precent.max())
    plt.plot(precent)
    plt.show()
    '''