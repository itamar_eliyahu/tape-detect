import numpy as np
import matplotlib.pyplot as plt
import os



data = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_regular_channels2.npy')

for i in range(0,data.shape[0]):
    a = data[9]
    b = a.copy()
    c = np.zeros(96)
    d = np.zeros(92)
    for j in range(1,100):
        b[j] =b[j-1]+a[j]
    for l in range(0,96):
        c[l] = (np.mean(b[l:l+5]))
    for k in range(0,92):
        d[k] = (c[k + 4] - c[k]) / 4

    fig, axarr = plt.subplots(2, 2)
    axarr[0,0].set_title("A")
    axarr[0,0].plot(a)

    axarr[0,1].set_title("B")
    axarr[0,1].plot(b)

    axarr[1,0].set_title("C")
    axarr[1,0].plot(c)

    axarr[1,1].set_title("D")
    axarr[1,1].plot(d)

    plt.show()
generated_data_arr = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/generated_channels_with_middle_tape.npy')
#for i in range(0,generated_data_arr.shape[0]):
#    generated_data_arr[i, :] = (generated_data_arr[i, :] - np.mean(generated_data_arr[i, :])).clip(0, 255).astype(np.uint8)
#np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/generated_narrow_pulse.npy',generated_data_arr)
fig, ax = plt.subplots()
for i in range(0, generated_data_arr.shape[0]):
    ax.cla()
    # ax.plot(self.channels_file[int(diff_and_ind[i,1]),:])
    ax.plot(generated_data_arr[i])
    ax.set_title("frame {}".format(i))
    # plt.show()
    plt.pause(0.3)

