import numpy as np
from keras.utils import np_utils
import keras.backend as K
from keras.losses import binary_crossentropy, categorical_crossentropy
import tensorflow as tf
import shutil
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
import matplotlib.pyplot as plt
from keras.preprocessing.sequence import pad_sequences
import tensorboard as tensorboard
import os
import numpy as np
from random import shuffle
from keras.callbacks import ModelCheckpoint,TensorBoard,EarlyStopping,LearningRateScheduler
from cnn_model import  one_dim_cnn_2_layers_reduced_model

def f1(y_true, y_pred,Beta=6):
    def recall(y_true, y_pred):
        """Recall metric.

        Only computes a batch-wise average of recall.

        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        """Precision metric.

        Only computes a batch-wise average of precision.

        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision
    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    #return 2*((precision*recall)/(precision+recall+K.epsilon()))
    return (1+ Beta**2)*((precision*recall)/( (Beta**2 * precision) +recall+K.epsilon()))

def f1_loss(predict, target):
    loss = 0
    lack_cls = tf.reduce_sum(target,axis=0) == 0
    #if lack_cls.any():
    #loss += binary_crossentropy(predict[:, lack_cls], target[:, lack_cls])
    loss += binary_crossentropy(predict, target)
    #predict = torch.sigmoid(predict)
    #predict = torch.clamp(predict * (1-target), min=0.01) + predict * target
    tp = predict * target
    #print(tp)
    tp = tf.reduce_sum(tp,axis=0)

    precision = tp / (tf.reduce_sum(predict,axis=0) + 1e-8)
    recall = tp / (tf.reduce_sum(target,axis=0) + 1e-8)

    #precision = tp / (predict.sum(dim=0) + 1e-8)
    #recall = tp / (target.sum(dim=0) + 1e-8)
    f1 = 2 * (precision * recall / (precision + recall + 1e-8))

    #print(tp , precision , recall , f1.mean())
    #np.savetxt('/content/drive/MyDrive/Tape_detect/dataset/f1.txt',f1,fmt="%f2")
    return 1 - tf.reduce_mean(f1) + loss

if __name__ == "__main__":
    X = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/X_regular_channels_2015_avg_norm.npy')
    Y = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015_avg_norm.npy')
    X = X.reshape(X.shape[0], 1, X.shape[1])
    print("Y.shape", Y.shape)
    print("X.shape", X.shape)

    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, shuffle=True)
    print(X_train.shape)

    n_kernel_1 = 16
    n_kernel_2 = 16
    n_outputs = 1

    model = one_dim_cnn_2_layers_reduced_model(n_kernel_1, n_outputs)
    # model=one_dim_cnn_model_3_cls(n_kernel_1,n_outputs)

    # model to be used at training time
    load = False

    # 2005
    # best_model_file_path =  '/content/drive/My Drive/Tape_detect/best_model/Tape_detect_in_regular_channels_2005/best_model_2005.h5'

    # 2015
    best_model_file_path = '/content/drive/My Drive/Tape_detect/best_model/Tape_detect_in_regular_channels/best_model_2015.h5'  #

    model_to_load_file_path = best_model_file_path
    if load:
        model = tf.keras.models.load_model(model_to_load_file_path, compile=False)
        # else:
        # model = Model(inputs=[inputs, labels, input_length, label_length], outputs=loss_out)

    verbose, epochs, batch_size = 1, 1500, 16
    TRAIN_EVALUATION_INTERVAL = int(X_train.shape[0] / batch_size)

    Learning_Rate = 0.8e-4  # 0.2e-4 ,lr = Learninig_Rate
    opt = tf.keras.optimizers.Adam(learning_rate=Learning_Rate)  # ,beta_1=0.7,beta_2=0.79,epsilon=1e-09,amsgrad=False)
    # opt = tf.keras.optimizers.SGD(learning_rate=0.8e-4, momentum=0.99)

    # model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
    model.compile(loss=[f1_loss], optimizer=opt, metrics=[f1])

    # fit network
    if (load == False):
        if (os.path.exists('/content/drive/My Drive/Tape_detect/Tensorboard_logs/train')):
            shutil.rmtree('/content/drive/My Drive/Tape_detect/Tensorboard_logs/train')
        if (os.path.exists('/content/drive/My Drive/Tape_detect/Tensorboard_logs/validation')):
            shutil.rmtree('/content/drive/My Drive/Tape_detect/Tensorboard_logs/validation')

    log_dir = '/content/drive/My Drive/Tape_detect/Tensorboard_logs/'

    callbacks_list = [ModelCheckpoint(filepath=best_model_file_path, monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=False, mode='auto'),
                      TensorBoard(log_dir, histogram_freq=1, profile_batch='500,520', write_graph=True,write_images=True),
                      EarlyStopping(monitor='val_loss', patience=5),
                      # LearningRateScheduler(StepDecay(initAlpha=3e-3, factor=0.8, dropEvery=7)),
                      # LearningRateScheduler(PolynomialDecay(maxEpochs=200, initAlpha=3e-3, power=2.0)),
                      ]

    model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size, verbose=verbose, validation_split=0.2,
              callbacks=callbacks_list, shuffle=True, workers=4)