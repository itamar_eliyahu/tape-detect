from sklearn.mixture import GaussianMixture
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy import ndimage, misc
from imblearn.over_sampling import ADASYN , SMOTE
from collections import Counter
from sklearn import cluster
from sklearn.preprocessing import StandardScaler
from scipy.signal import find_peaks

def plot_dataset(file1,file2,file3):
    raw_samples_file = np.load(file1)[:, :, :115]
    raw_samples_file = raw_samples_file.reshape(raw_samples_file.shape[0], raw_samples_file.shape[2])

    std_file = np.load(file2)

    avg_norm = np.load(file3)[:, :, :100]
    avg_norm = avg_norm.reshape(avg_norm.shape[0], avg_norm.shape[2])


    index=1266
    fig, ax = plt.subplots(1, 3)
    ax[0].plot(raw_samples_file[index],c='blue')
    ax[0].set_title('Original')

    ax[1].plot(std_file[index],c='orange')
    ax[1].set_title('STD window')

    avg_norm_scalled = np.zeros_like(avg_norm)
    for i in range (0,avg_norm.shape[0]):
        #avg_norm_scalled[i,:] = ((avg_norm[i, :] - np.min(avg_norm[i,:])) / (np.max(avg_norm[i])- np.min(avg_norm[i]))) * (255/ (np.max(avg_norm[i])))
        #avg_norm_scalled[i, :] = avg_norm[i, :] * (128/np.max(avg_norm[i]))
        avg_norm_scalled[i, :] = avg_norm[i, :]  / np.std(raw_samples_file[i])
        #avg_norm_scalled[i,:] = ((avg_norm_scalled[i, :] - np.min(avg_norm_scalled[i,:])) / (np.max(avg_norm_scalled[i])- np.min(avg_norm_scalled[i]))) * 255

    for p in range(0,30000,16):
        index = p+30000
        k=0
        fig, ax = plt.subplots(4, 4)
        for i in range(0,4):
            for j in range(0,4):
                ax[i,j].plot(avg_norm[k+index,:],c='blue')
                ax[i, j].plot(raw_samples_file[k + index, :],c='orange')
                #ax[i, j].plot(avg_norm_scalled[k + index, :],c='green')

                k+=1
        plt.show()
    ax[2].plot(avg_norm[index],c='green')
    ax[2].set_title('AVG window then subtract AVG')

    #temp= raw_samples_file[[0,1,2,80000,80001,80002,49000,49001,49002],:]
    #np.savetxt("C:/Projects/Python/Tape_detect/Tape_dataset/datasets/raw_signal_with_tape.csv", temp.T, delimiter=",",fmt='%d')
    return()


def median_norm(input_sampels_file_path):
    samples_file = np.load(input_sampels_file_path)  # [:, :100]
    temp_list = []
    samples_after_median_norm = np.zeros_like(samples_file)
    samples_minus_shaft = np.zeros_like(samples_after_median_norm)
    samples_minus_shaft_norm_with_hist = np.zeros_like(samples_minus_shaft)
    for i in range(0, samples_file.shape[0]):
        samples_minus_shaft[i, :]  = (samples_file[i, :]  - np.mean(samples_file[i, 85:])).clip(0, 255).astype(np.uint8)
        ch_median = np.median(samples_file[i, 10:85])
        if (np.median(samples_file[i, 15:85]) - np.mean(samples_file[i, 85:])):
            samples_after_median_norm[i, :] = (samples_minus_shaft[i, :] /( np.median(samples_file[i, 15:85]) - np.mean(samples_file[i, 85:])))*64
        else:
            samples_after_median_norm[i, :] = samples_minus_shaft[i, :] / (np.median(samples_file[i, :])) * 64

        valid_points = np.where(samples_after_median_norm[i, 15:40] > 64)
        Area = np.sum(samples_after_median_norm[i, valid_points[0]])
        step = np.median(samples_file[i, 15:85]) - np.mean(samples_file[i, 85:])
        plot =1
        if plot:
            fig, ax = plt.subplots(1, 3)
            ax[0].plot(samples_file[i, :])
            ax[0].set_title('Raw')
            ax[1].plot(samples_minus_shaft[i, :])
            ax[1].set_title('Raw Minus Shaft , Shaft:{}'.format (np.mean(samples_file[i, 85:])))
            ax[2].stem(samples_after_median_norm[i, :])
            ax[2].set_title('Median Norm, step:{} , Median{}., Step{}.'.format ( np.median(samples_file[i, :]) - np.mean(samples_file[i, 85:]),(ch_median),(step)))
            plt.show()
        temp_list.append(step)
    np.save('C:/Projects/Python/Tape_detect/debug/valid_points_with_tape.npy',np.array(temp_list))

def running_peak_with_thresh(input_sampels_file_path):
    plot =1
    if plot:
        wo_tape = np.load('C:/Projects/Python/Tape_detect/debug/wo_tape_12.npy')
        with_tape = np.load('C:/Projects/Python/Tape_detect/debug/wo_tape_12_td.npy')
        fig, ax = plt.subplots(1, 3)
        ax[0].scatter(wo_tape[:100000,0] ,wo_tape[:100000,1],facecolors='none', edgecolors='r',label='wo tape')
        ax[0].legend()
        ax[1].scatter(with_tape[:100000, 0],with_tape[:100000, 1],facecolors='none', edgecolors='b',label='with tape')
        ax[1].legend()
        ax[2].scatter(wo_tape[:100000,0] ,wo_tape[:100000,1],facecolors='none', edgecolors='r',label='wo tape')
        ax[2].legend()
        ax[2].scatter(with_tape[:100000, 0],with_tape[:100000, 1],facecolors='none', edgecolors='b',label='with tape')
        ax[2].legend()

        plt.show()
    else:
        #path = 'C:/Projects/Python/Tape_detect/debug/reg_with_tape.npy'
        #path = 'C:/Projects/Python/Tape_detect/debug/reg_without_tape.npy'
        path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_with_tape_raw_16_td.txt'
        samples_file = np.genfromtxt(path)
        #samples_file = np.load(path)
        #np.save('C:/Projects/Python/Tape_detect/debug/reg_without_tape.npy',samples_file)
        pulse_area_and_max_val = np.zeros((samples_file.shape[0],2))
        thresh = 12
        tol=5
        width_treshold = 3
        for i in range (0,samples_file.shape[0]):
            start_of_pulse = 0
            area = []
            max_pix_in_area = []
            pixels = []
            num_of_pix = 0
            pixels_idx=[]
            pulse_detected_in_signal = 0
            for j in range(20,samples_file.shape[1]):
                #peaks, _ = find_peaks(samples_file[i, :], height=150)
                if (samples_file[i, j] > thresh-tol):
                    start_of_pulse=1
                    pixels.append(samples_file[i, j])
                    pixels_idx.append(j)
                    num_of_pix+=1
                else:
                    if (start_of_pulse == 1): # end of pulse
                        pulse_detected_in_signal = 1
                        start_of_pulse =0
                        max_pix_in_area.append(max(pixels))
                        area.append(sum(pixels))# / len(pixels))
                        pixels = []
                        num_of_pix = 0
                        pixels_idx = []
            if (pulse_detected_in_signal):
                max_avg_idx=area.index(max(area))
                pulse_area_and_max_val[i,0] = area[max_avg_idx]
                pulse_area_and_max_val[i, 1] = max_pix_in_area[max_avg_idx]
        #ind_to_clean=np.where(peak_area_and_val_avg[:,1] < width_treshold)
        #peak_area_and_val_avg = np.delete(pulse_area_and_max_val, pulse_area_and_max_val, axis=0)
        np.save('C:/Projects/Python/Tape_detect/debug/wo_tape_12_td.npy',pulse_area_and_max_val)


def running_wind(input_sampels_file_path):
    samples_file = np.genfromtxt(input_sampels_file_path)[:, :100]
    #np.save('C:/Projects/Python/Tape_detect/debug/valid_points2.npy', samples_file)
    #samples_file = np.load('C:/Projects/Python/Tape_detect/debug/valid_points2.npy')
    #samples_file[1] = ((samples_file[1] - samples_file[1].min() ) / (samples_file[1].max() - samples_file[1].min() )) *255
    #samples_file[0] = ((samples_file[0] - samples_file[0].min()) / (samples_file[0].max() - samples_file[0].min())) * 255
    #samples_file = samples_file.reshape(samples_file.shape[0], samples_file.shape[2])
    #samples_file  = np.genfromtxt('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/temp1.txt')
    #samples_file = np.genfromtxt('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_without_tape_raw_from_first_peak.txt')

    expand_data =0
    show_wavefrom =0
    calc_gmm =0

    if expand_data: # double dataset with adding rnd noise
        samples_file_plus_rnd_noise2 = np.zeros_like(samples_file)
        for ch in range(0, int(samples_file_plus_rnd_noise2.shape[0])):
            for sample in range(0, int(samples_file_plus_rnd_noise2.shape[1])):
                rnd_noise = np.random.randint(-10, 10)
                samples_file_plus_rnd_noise2[ch, sample] = (rnd_noise + samples_file[ch, sample]).clip(0, 255)

        #samples_file_flipped= np.zeros_like(samples_file)
        #for ch in range(0, int(samples_file_plus_rnd_noise2.shape[0])):
            #samples_file_flipped[ch, :] = (np.flip (samples_file[ch, :])).clip(0, 255)

        samples_file = np.concatenate((samples_file, samples_file_plus_rnd_noise2))

    stride = 1
    wind_width = 15
    N = 6#9

    n_bins = 20 # histogram bins
    num_of_samp_in_std_form = int((samples_file.shape[1]-5 - (wind_width)) / stride)# 5 for padding
    num_of_cols = int(num_of_samp_in_std_form)
    samples_after_std = np.zeros((samples_file.shape[0],num_of_cols))

    temp_list =[]
    step_list=[]
    samples_after_ma = np.zeros((samples_file.shape[0],100))
    samples_minus_shaft = np.zeros_like(samples_after_ma)
    samples_minus_shaft_norm_with_hist = np.zeros_like(samples_minus_shaft)
    for i in range (0,samples_file.shape[0]):
        #print(i)

        #for j in range (0,samples_file.shape[1]-wind_width,stride):
            #samples_after_std[i,j] = np.std(samples_file[i,j:wind_width+j])
        #for j in range (0,samples_file.shape[1]-N):

        # samples_file[i, :] = np.loadtxt('C:/Projects/Python/Tape_detect/debug/temp2.txt')
        #MA
        #samples_after_ma[i, :] = np.convolve(samples_file[i, :], np.ones(N) / N, mode='valid')
        #samples_after_ma[i, :] = samples_after_ma[i, :].astype(np.uint8)
        #Median
        #samples_after_ma[i,:] =  ndimage.median_filter(samples_file[i,:], size=15)
        #samples_after_ma[i, :] = samples_after_ma[i, :].astype(np.uint8)
        #Raw
        #samples_after_ma[i, :] =samples_file[i, :]
        # AVG Reduction
        samples_after_ma[i, :] = samples_file[i, :] - samples_file[i, :].mean()
        samples_after_ma[i, :] = samples_after_ma[i, :].clip(0,255)
        second_peak_area_side_width = 4
        #Hist_255_val_ = np.histogram(samples_after_ma[i], bins=n_bins)[0]  # ,range=[0,255]
        #Hist_255_bins_ = np.histogram(samples_after_ma[i], bins=n_bins)[1][:-1]  # ,range=[0,255]

        #peak_1_loc = np.argmax(Hist_255_val_)
        #peak_1_bin = Hist_255_bins_[peak_1_loc]

        # Step normalization
        axis_med = np.median(np.hstack((samples_file[i, :10],samples_file[i, 80:])))
        bn_med = np.median(samples_file[i, 10:80])
        step_height = bn_med-axis_med
        not_minus_axis = samples_file[i, :] - axis_med
        note_div_by_step = (not_minus_axis / step_height)*128
        note_div_by_step -= 128
        note_div_by_step = note_div_by_step.clip(0, 255)
        step_list.append(step_height)

        '''if (peak_1_loc <= second_peak_area_side_width):
            peak_2_left_option_loc = 0
            peak_2_left_option_val =0
        else:
            second_peak_left_area_point = peak_1_loc- second_peak_area_side_width
            peak_2_left_option_loc = np.argmax(Hist_255_val_[:second_peak_left_area_point])
            peak_2_left_option_val = np.max(Hist_255_val_[:second_peak_left_area_point])

        if (peak_1_loc >= Hist_255_val_.shape[0]-second_peak_area_side_width):
            peak_2_right_option_loc = 0
        else:
            second_peak_right_area_point = peak_1_loc + second_peak_area_side_width
            peak_2_right_option_loc = np.argmax(Hist_255_val_[second_peak_right_area_point:]) + second_peak_right_area_point
            peak_2_right_option_val = np.max(Hist_255_val_[second_peak_right_area_point:])

        if (peak_2_right_option_val >peak_2_left_option_val ):
            peak_2_loc = peak_2_right_option_loc
        else:
            peak_2_loc = peak_2_left_option_loc
        '''
        '''
        if (peak_2_left_option_loc > second_peak_area_side_width):
            print(peak_2_loc)
            print(second_peak_right_area_point)
            print(Hist_255_bins_.shape[0])
            print()
            peak_2_bin =Hist_255_bins_[peak_2_loc+second_peak_right_area_point]
        else:
            '''
        #peak_2_bin = Hist_255_bins_[peak_2_loc]

        #histogram_wo_note_val = np.maximum(peak_2_bin,peak_1_bin)
        #histogram_shaft_val = np.minimum(peak_2_bin, peak_1_bin)

        #if ( (histogram_wo_note_val - histogram_shaft_val) <7):
            #plt.scatter(Hist_255_bins_,Hist_255_val_)
            #plt.show()


        if calc_gmm:
            X =  np.expand_dims(samples_after_ma[i, :], axis=1)#Hist_255_val#np.hstack((Hist_255_val,Hist_255_bins))
            #X =  np.expand_dims(Hist_255_val_[:10], axis=1)#Hist_255_val#np.hstack((Hist_255_val,Hist_255_bins))

            gmm = GaussianMixture(n_components=3, covariance_type='full', max_iter=200, random_state=0).fit(X)

            mean_1 = gmm.means_[0][0] # Shaft
            mean_2 = gmm.means_[1][0] # Area withut note
            mean_3 = gmm.means_[2][0]

            means = np.sort([mean_1,mean_2,mean_3])

            mean_1 = means[0] # Shaft
            mean_2 = means[1] # Area withut note
            mean_3 = means[2]
            #print(mean_1*256/n_bins,mean_2*256/n_bins,mean_3*256/n_bins)

            std_1 = np.sqrt(gmm.covariances_[0][0])[0]
            std_2 = np.sqrt(gmm.covariances_[1][0])[0]
            std_3 = np.sqrt(gmm.covariances_[2][0])[0]

            weight_1 = gmm.weights_[0]
            weight_2 = gmm.weights_[1]
            weight_3 = gmm.weights_[2]
        else:
            '''
            mean_1 = np.median(np.concatenate((samples_after_ma[i, :9],samples_after_ma[i, 85:]))) # Shaft
            mean_2 = np.median(samples_after_ma[i, 10:80]) # Area withut note
            #mean_1 = np.average(np.concatenate((samples_after_ma[i, :9],samples_after_ma[i, 85:]))) # Shaft
            #mean_2 = np.average(samples_after_ma[i, 10:80]) # Area withut note

            means = np.sort([mean_1,mean_2])

            mean_1 = means[0] # Shaft
            mean_2 = means[1] # Area withut note


        samples_minus_shaft[i,:] = (samples_after_ma[i,:] - mean_1).clip(0,255)
        #if (samples_minus_shaft[i,:].min() <0):
            #samples_minus_shaft[i, :] +=samples_minus_shaft[i,:].min()
        #samples_minus_shaft[i, :] = (samples_after_ma[i, :] - histogram_shaft_val).clip(0, 255)

        wo_note = np.abs(mean_2 - mean_1)
         

        # Standartization
        #ma_minus_avg =samples_after_ma[i,:] - np.mean(samples_after_ma[i,:])#.astype(np.uint8)
        #ma_minus_avg =  ma_minus_avg.clip(0,255)
        #ma_minus_avg_div_std =  ma_minus_avg / np.std(ma_minus_avg)#.astype(np.uint8)
        #ma_minus_avg_div_std =ma_minus_avg_div_std.astype(np.uint8)

        #ma_minus_avg_div_std = ((ma_minus_avg_div_std - ma_minus_avg_div_std.min()) / (ma_minus_avg_div_std.max() - ma_minus_avg_div_std.min())) * 255
        #samples_minus_shaft_norm_with_hist[i, :] = ma_minus_avg_div_std#ma_minus_avg#

        #GMM
        samples_minus_shaft_norm_with_hist[i, :] = (samples_minus_shaft[i, :] /wo_note)*128
        samples_minus_shaft_norm_with_hist[i, :] = (samples_minus_shaft_norm_with_hist[i, :]-128).clip(0,255)
        #samples_minus_shaft_norm_with_hist[i, :] = (samples_after_ma[i, :] - mean_2)

        #Histogram
        #samples_minus_shaft_norm_with_hist[i, :] = (samples_minus_shaft[i, :] / (histogram_wo_note_val - histogram_shaft_val)) * 255
        #Normalization
        #if (samples_minus_shaft_norm_with_hist[i, :].max() > 80 ):
            #samples_minus_shaft_norm_with_hist[i, :] = ((samples_minus_shaft_norm_with_hist[i, :] - samples_minus_shaft_norm_with_hist[i, :].min()) / (samples_minus_shaft_norm_with_hist[i, :].max() - samples_minus_shaft_norm_with_hist[i, :].min())) * 255

        # Signal minus mean only
        #samples_minus_shaft_norm_with_hist[i, :] = (samples_after_ma[i,:] - np.mean(samples_after_ma[i,:]).astype(np.uint8)).clip(0, 255)

        #
        #temp1 = samples_after_ma[i,:] - np.mean(samples_after_ma[i,85:]).astype(np.uint8)
        #temp1 = temp1.clip(0, 255)
        #diff = (np.mean(samples_after_ma[i,:85]).astype(np.uint8) - np.mean(samples_after_ma[i,85:]).astype(np.uint8))#.astype(np.uint8)

        
        if (diff==0):
            if (diff > 20):
                temp1 = (temp1 / np.mean(samples_after_ma[i,:85]).astype(np.uint8))
                temp1 *= 64
                temp_list.append(temp1.astype(np.int))
        elif (diff > 20):
            temp1 = (temp1 / diff)
            temp1 *= 64
            temp1=temp1.astype(np.int).clip(0,255)
            temp_list.append(temp1)'''



        #print(np.median(samples_after_ma[i,:85]).astype(np.uint8) , np.mean(samples_after_ma[i,:85]).astype(np.uint8), np.mean(samples_after_ma[i,85:]).astype(np.uint8))
        #plt.plot(samples_after_ma[i,:])
        #plt.plot(temp1)
        #plt.show()

        #samples_minus_shaft_norm_with_hist[i, :] = temp1

        #temp = samples_after_ma[i,:]  - params_file[i,0].clip(0, 255)
        #if ( params_file[i,1] -  params_file[i,0]):
            #temp =  (temp / ( params_file[i,1] -  params_file[i,0])) * 32
        #samples_minus_shaft_norm_with_hist[i, :] = temp


        #scaler = StandardScaler()
        #temp=scaler.fit(samples_after_ma[i,:].reshape(1, -1) )
        #temp  =scaler.transform(samples_after_ma[i,:].reshape(1, -1) )
        #samples_minus_shaft_norm_with_hist[i, :] =temp.reshape(100, -1)
        #samples_minus_shaft_norm_with_hist[i, :] /= np.std(samples_minus_shaft_norm_with_hist[i, :])
        #samples_minus_shaft_norm_with_hist[i, :] = (samples_minus_shaft_norm_with_hist[i, :]).astype(np.uint8)
        #np.savetxt("C:/Projects/Python/Tape_detect/Tape_dataset/datasets/raw_signal_1.csv", samples_file[i, :].T, delimiter=",",fmt='%d')

        if show_wavefrom:
            '''
            fig, ax = plt.subplots(2, 1)
            ax[0].plot(samples_file[i, :])
            ax[0].set_title('Raw')
            ax[1].plot(samples_minus_shaft_norm_with_hist[i, :])
            ax[1].set_title('Raw After MA')

            '''
            fig, ax = plt.subplots(1, 3)
            ax[0].plot(samples_file[i, :])
            ax[0].set_title('Raw,'+'Note: ' + str(bn_med)+ ' axis: '+str(axis_med) )

            ax[1].plot(samples_after_ma[i, :])
            ax[1].set_title('Raw Minus AVG')

            ax[2].set_title('Step Norm')
            #ax[2].hist(samples_file[i], bins=n_bins)#,range=[0,255]
            #ax[2].hist(samples_after_ma[i], bins=n_bins)#,range=[0,255]
            #ax[2].hist(samples_after_ma[i], bins=n_bins)
            ax[2].plot(note_div_by_step)

            Hist_255_val_  = np.histogram(samples_after_ma[i], bins=n_bins)[0]#,range=[0,255]
            Hist_255_bins_ = np.histogram(samples_after_ma[i], bins=n_bins)[1]#,range=[0,255]
            Hist_255_val = np.expand_dims(Hist_255_val_,axis=1)
            Hist_255_bins = np.expand_dims(Hist_255_bins_[:-1], axis=1)
            area = sum(np.diff(Hist_255_bins_) * Hist_255_val_) + sum(np.diff(Hist_255_bins_) * Hist_255_val_)

            if calc_gmm:
                y = np.arange(X.shape[0])
                xmin = Hist_255_bins_.min()
                xmax = Hist_255_bins_.max()
                x = np.linspace(xmin, xmax, 50)
                #ax[3].set_title('GMM 3 componenets')
                y1 = norm.pdf(x, mean_1, std_1) * weight_1 * area
                ax[3].plot(x, y1,c='green')

                y2 = norm.pdf(x, mean_2, std_2) * weight_2 * area
                ax[3].plot(x, y2,c='orange')

                y3 = norm.pdf(x, mean_3, std_3) * weight_3 * area
                ax[3].plot(x, y3,c='blue')
                ax[3].set_title('GMM')

                gmm_labels = gmm.predict(X)

                ax[4].scatter(y,X[:, 0], c=gmm_labels, s=40, cmap='viridis')
                ax[4].set_title('GMM 3 labels')

            '''
            n_clusters=3
            k_means = cluster.KMeans(n_clusters=n_clusters, n_init=4)
            k_means.fit(X)
            values = k_means.cluster_centers_.squeeze()
            values = np.sort(values)
            labels = k_means.labels_
            '''
            #ax[4].scatter(y,X[:, 0], c=labels, s=40, cmap='viridis')
            #ax[4].set_title('Kmeans 3 labels{}'.format(values[1]))
            ''''''
            ''''''
            #ax[5].plot(samples_minus_shaft[i,:])
            #ax[5].set_title('Raw minus shaft   {}'.format(histogram_shaft_val))
            #ax[5].plot((samples_after_ma[i, :] - np.mean(samples_after_ma[i, :]).astype(np.uint8)).clip(0, 255))
            #ax[5].set_title('MA minus mean')

            #ax[5].plot(gmm_labels_for_norm)
            #ax[5].set_title('GMM labels norm')

            #ax[6].plot(samples_minus_shaft_norm_with_hist[i,:])
            #ax[6].set_title('Normalized GMM')
            #ax[6].set_title('Normalized Med')
            #ax[3].set_title('Normalized param1 {} param1 {}'.format(params_file[i,0],params_file[i,1]))
            #ax[6].set_title('Normalized STD {}'.format (np.std(ma_minus_avg)))
            ''''''

            plt.show()

    samples_minus_shaft_norm_with_hist=np.delete(samples_minus_shaft_norm_with_hist,temp_list,axis=0)
    np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/regular_channels_with_tape_gmm_norm_128.npy',samples_minus_shaft_norm_with_hist)
    #np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/regular_channels_with_tape_new_norm.npy',np.array(temp_list))
    #np.save('C:/Projects/Python/Tape_detect/debug/step_list.npy',np.array(step_list))




if __name__ == "__main__":
    import seaborn as sns
    step_list = np.load('C:/Projects/Python/Tape_detect/debug/step_list.npy')
    fig, ax = plt.subplots(1, 2)
    ax[0].hist(step_list )

    plt.show()
    sns.distplot(
        step_list
    )
    '''
    g_ch_note_level =np.zeros(18)
    g_ch_cnt = np.zeros(18)
    step = 100#(note - shaft);
    ch_shift=0
    for i in range (0,100):
        for ch in range(0,18):
            if (g_ch_cnt[ch] == 0):
                g_ch_note_level[ch] = step;
                g_ch_cnt[ch + ch_shift] = g_ch_cnt[ch + ch_shift] + 1
            elif (g_ch_cnt[ch + ch_shift] < 100):
                g_ch_cnt[ch + ch_shift] = g_ch_cnt[ch + ch_shift] + 1
                g_ch_note_level[ch + ch_shift] = ((g_ch_note_level[ch + ch_shift] * g_ch_cnt[ch + ch_shift]) + step) / (g_ch_cnt[ch + ch_shift] + 1);
    print("stop")
    '''
    #FP
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/debug/dv_test_deck/debug_file_fp_tape.txt'
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/debug/dv_test_deck/debug_file_fp_tape_2005.txt'
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/debug/dv_test_deck/2005_FP.txt'
    #Raw_Middle
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/Raw_Middle.txt'

    input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_without_tape_raw_16_raw.txt'



    #WOregular_channels_2015_with_tape_raw_16_rawregular_channels_2015_with_tape_raw_16_raw
    #Regular
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_with_tape_raw_16.txt'
    #input_sampels_file_path_raw = np.genfromtxt(input_sampels_file_path_raw)
    #np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/regular_channels_2015_without_tape_raw_10.npy',input_sampels_file_path_raw)

    #WM
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/watermark_channels_2015_without_tape_raw_from_first_peak_4.npy'
    #Thread
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/thread_channels_without_tape_raw_from_first_peak_4.npy'
    #Next to Thread
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/next_to_thread_channels_without_tape_raw_from_first_peak_4.npy'



    #std_file = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015_std_wind.npy'
    #input_sampels_file_path_avg_norm = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/regular_channels_2015_wo_tape_avg_norm.npy'

    #With
    #Regular
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/regular_channels_2015_with_tape_raw_from_first_peak.txt'
    #input_sampels_file_path_raw = np.genfromtxt(input_sampels_file_path_raw)
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/regular_channels_2015_mid.npy'

    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/sw_records/temp1.txt'
    #input_sampels_file_path_raw = np.genfromtxt(input_sampels_file_path_raw)
    #np.save('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/regular_channels_2015_with_tape_raw_from_first_peak_wo_shift_6.npy',input_sampels_file_path_raw)

    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/reg_without_m4.npy'
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/reg_with_vertical_P6G3774225.npy'

    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/regular_channels_2015_mid.npy'
    #input_sampels_file_path_raw =('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/regular_channels_2015_with_tape_raw_from_first_peak.npy')

    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/regular_channels_2015_with_narrow_tape_raw_from_first_peak_4.npy'

    # WM
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/watermark_channels_2015_with_tape_raw_from_first_peak_4.npy'
    #Thread
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/thread_channels_with_tape_raw_from_first_peak_4.npy'
    #Next to Thread
    #input_sampels_file_path_raw = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/next_to_thread_channels_with_tape_raw_from_first_peak_4.npy'


    #std_file = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/Datasets/Y_regular_channels_2015_std_wind.npy'
    #input_sampels_file_path_avg_norm = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/npy/regular_channels_2015_with_tape_avg_norm.npy'

    #plot_dataset(input_sampels_file_path_raw,std_file,input_sampels_file_path_avg_norm)

    running_wind(input_sampels_file_path_raw)
    #running_peak_with_thresh(input_sampels_file_path_raw)

    #median_norm(input_sampels_file_path_raw)
    '''test =1
    if test:
        with_tape = np.load('C:/Projects/Python/Tape_detect/debug/valid_points_with_tape.npy')
        wo_tape = np.load('C:/Projects/Python/Tape_detect/debug/valid_points_without_tape.npy')
        plt.plot(wo_tape)
        plt.plot(with_tape)
        plt.show()'''