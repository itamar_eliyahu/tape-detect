import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from skimage.measure import EllipseModel
from matplotlib.patches import Ellipse
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.tree import DecisionTreeClassifier


class Running_window_STD_classifier():
    flags = {'plot_sample':0,'save_data_to_file':1,}

    def __init__(self, input_dataset_file_path):

        self.input_dataset_file  = np.load(input_dataset_file_path)[:,:-1]
        self.input_dataset_label = np.load(input_dataset_file_path)[:, -1]
        self.input_dataset_length = self.input_dataset_file.shape[0]
        self.input_dataset_width = self.input_dataset_file.shape[1]-1 # -1 to remove label column
        self.start_index = 50#99
        self.end_index = 20#20#
        self.wind_width = 8#10
        self.step_size=1
        self.num_of_wind = int (( (np.abs(self.end_index - self.start_index)-self.wind_width) /self.step_size)+1 )# ((input_len-Filter_len)/Stride) +1
        self.col_to_add = np.zeros((int(self.input_dataset_file.shape[0]),(self.num_of_wind+1)))# +1 for the class
        self.work_file = np.concatenate((self.input_dataset_file,self.col_to_add),axis=1)
        self.work_file2 = self.work_file.copy()
        self.std_threshold =50
    def std_calc(self):
        for i in range (0,self.input_dataset_length):
            for j in range(0,self.num_of_wind):
                #print(self.start_index+j,self.wind_width + self.start_index + j)
                #print(self.work_file[i,self.start_index+j:self.wind_width + self.start_index + j ])
                self.work_file[i,self.input_dataset_width+j+1] = np.std(self.work_file[i,self.start_index+j:self.wind_width + self.start_index + j ])
            #self.work_file[i, -1] = (self.work_file[i,self.input_dataset_width+1:-1]).max()
            #self.work_file[i, -1] = np.sum(self.work_file[i, 100:174]>=self.std_threshold)
            self.work_file[i, -1] =np.max (self.work_file[i, 140:170])

            '''
            if ((self.work_file[i,self.input_dataset_width+1:-1]).max() > self.std_threshold):
                self.work_file[i,-1]=1
            else:
                self.work_file[i, -1] = 0
            '''
        plt.hist(self.work_file[:4588, -1], bins='auto')
        #plt.hist(self.work_file[:4550, -1], bins='auto')
        plt.hist(self.work_file[4588:, -1], bins='auto')
        self.calc_confusion_matrix()

        return()

    def calc_grad(self):
        temp_vect = np.zeros(100)
        thresh =60
        for i in range(0, self.input_dataset_length):
            for j in range(31,100):
                if (np.abs((self.work_file[i,j-1] - self.work_file[i,j])) > thresh):
                    temp_vect[j-1] = 1
                else:
                    temp_vect[j-1] = 0
            self.work_file[i, -1] = np.sum(temp_vect)

        plt.hist(self.work_file[4588:, -1], bins='auto')
        plt.hist(self.work_file[:4588, -1], bins='auto')
        plt.show()
        return ()


    def calc_mean_and_std(self):
        '''
        from scipy.ndimage import gaussian_filter1d

        input_dataset_file  = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/std_thersh_clean/regular_channels_2015_10mm_tape_hist_norm_mach2.npy')[:,:-1]
        input_dataset_file  = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/ten_mm/std_thersh_clean/regular_channels_2015_with_10mm_tape_avg_norm.npy')[:,:-1]

        intg_vect = np.zeros(input_dataset_file.shape[0])
        kernel = np.array([1,-1])
        for i in range(0, input_dataset_file.shape[0]):
            cnv = np.convolve(kernel, input_dataset_file[i,:30] , 'valid')
            intg_vect[i] =np.sum(cnv)
        i = 300
        gss= gaussian_filter1d(self.work_file[i,:100], 1)
        kernel = np.array([-1, 0, 1])

        cnv  = np.convolve(kernel, gss , 'valid')
        #plt.plot(self.work_file[i, :100])
        #plt.plot(gss)
        #plt.plot(cnv)
        '''

        for i in range(0, self.input_dataset_length):
            for j in range(0, self.num_of_wind):
                #print (self.start_index - j ,  self.start_index - (self.wind_width + j))
                self.work_file[i, self.input_dataset_width + j + 1] = np.std(self.work_file[i, self.start_index - (self.wind_width + j) : self.start_index - j])
                self.work_file2[i, self.input_dataset_width + j + 1] = np.mean(self.work_file2[i, self.start_index - (self.wind_width + j) : self.start_index - j])
            self.work_file[i, -1] = np.min(self.work_file[i, 100:-1])#STD
            self.work_file2[i, -1] = np.max(self.work_file2[i, 100:-1])#Mean

        with_tape_sampels= 26691#2088
        x_axis_with_tape =  self.work_file2[:with_tape_sampels,100: -1]#self.work_file2[:with_tape_sampels, -1]
        y_axis_with_tape = self.work_file[:with_tape_sampels,100: -1]#self.work_file[:with_tape_sampels, -1]

        x_axis_wo_tape =  self.work_file2[with_tape_sampels:, 100:-1]# self.work_file2[with_tape_sampels:, -1]
        y_axis_wo_tape = self.work_file[with_tape_sampels:, 100:-1]#self.work_file[with_tape_sampels:, -1]
        x1=np.hstack((x_axis_with_tape,y_axis_with_tape))#.T
        #x1=x_axis_with_tape
        y1 = np.ones((x1.shape[0],1))
        x2=np.hstack((x_axis_wo_tape,y_axis_wo_tape))#.T
        #x2 =x_axis_wo_tape
        y2 = np.zeros((x2.shape[0],1))
        X = np.vstack((x1,x2))
        Y = np.vstack((y1, y2))

        #X = X[:,[28,47,56,67,84,127,169,181]]
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, shuffle=True)
        #X_train = X[1000:-1000, :]
        #y_train = Y[1000:-1000]
        #X_test = np.concatenate((X[:1000,:],X[-1000:,:]))
        #y_test = np.concatenate((Y[:1000], Y[-1000:]))

        logist_regression = LogisticRegression(penalty='l1',solver='liblinear').fit(X_train,y_train)
        #logist_regression = LogisticRegression().fit(X_train,y_train)
        print("logistic regression prediction accuracy is: ", logist_regression.score(X_test, y_test) * 100, "%")
        Y_pred = logist_regression.predict(X_test)

        '''
        from sklearn.naive_bayes import GaussianNB
        X=self.input_dataset_file
        Y=self.input_dataset_label
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, shuffle=True)
        gnb = GaussianNB()
        y_pred = gnb.fit(X_train, y_train).predict(X_test)
        print("Naive base prediction accuracy is: ", gnb.score(X_test, y_test) * 100, "%")
'''
        Desicion_Tree = DecisionTreeClassifier(criterion='entropy').fit(X_train, y_train)
        Y_pred_dt = Desicion_Tree.predict(X_test)
        print("Desicion Tree prediction accuracy is: ", Desicion_Tree.score(X_test, y_test) * 100, "%")
        plt.plot(Desicion_Tree.feature_importances_)


        matrix = confusion_matrix(y_test, Y_pred)
        #matrix = confusion_matrix(y_test, Y_pred_dt)

        tn, fp, fn, tp = matrix.ravel()
        print("True negative", tn)
        print('False negative', fn)
        print(((fn) / (fn + tn)) * 100, '% false negative Fails', '\n')

        print("True positive", tp)
        print("False positive", fp)
        print(((fp) / (fp + tp)) * 100, '% false positive Fails', '\n')

        mean_of_mean_with_tape = np.mean(x_axis_with_tape)
        std_of_mean_with_tape = np.std(x_axis_with_tape)

        mean_of_std_with_tape = np.mean(y_axis_with_tape)
        std_of_std_with_tape = np.std(y_axis_with_tape)

        idx=3333
        plt.plot(self.work_file[idx, :30])
        plt.plot(self.work_file[idx, 100:-1],color='red',label='STD')
        plt.plot(self.work_file2[idx, 100:-1],color='green',label='MEAN')
        plt.legend()

        plt.scatter(x_axis_with_tape, y_axis_with_tape)#,lable='with tape'
        #plt.scatter(mean_of_mean_with_tape, mean_of_std_with_tape, color='red', s=50)
        #plt.scatter(mean_of_mean_with_tape + 3 * std_of_mean_with_tape, mean_of_std_with_tape, color='green', s=50)
        #plt.scatter(mean_of_mean_with_tape - 3 * std_of_mean_with_tape, mean_of_std_with_tape, color='green', s=50)

        #plt.scatter(mean_of_mean_with_tape , mean_of_std_with_tape + 3 * std_of_std_with_tape, color='blue', s=50)
        #plt.scatter(mean_of_mean_with_tape, mean_of_std_with_tape - 3 * std_of_std_with_tape, color='blue', s=50)




        plt.scatter(x_axis_wo_tape, y_axis_wo_tape)  # ,lable='wo tape'
        '''
        mean_of_mean_wo_tape = np.mean(x_axis_wo_tape)
        std_of_mean_wo_tape = np.std(x_axis_wo_tape)

        mean_of_std_wo_tape = np.mean(y_axis_wo_tape)
        std_of_std_wo_tape = np.std(y_axis_wo_tape)

        
        plt.scatter(mean_of_mean_wo_tape, mean_of_std_wo_tape, color='red', s=50)
        plt.scatter(mean_of_mean_wo_tape + 3 * std_of_mean_wo_tape, mean_of_std_wo_tape, color='green', s=50)
        plt.scatter(mean_of_mean_wo_tape - 3 * std_of_mean_wo_tape, mean_of_std_wo_tape, color='green', s=50)

        plt.scatter(mean_of_mean_wo_tape, mean_of_std_wo_tape + 3 * std_of_std_wo_tape, color='blue', s=50)
        plt.scatter(mean_of_mean_wo_tape, mean_of_std_wo_tape - 3 * std_of_std_wo_tape, color='blue', s=50)
        '''
        plt.xlabel('Mean')
        plt.ylabel('STD')
        plt.show()


        with_tape_points = np.vstack((x_axis_with_tape,y_axis_with_tape)).T
        x_with = with_tape_points[:,0]
        y_with = with_tape_points[:,1]

        ell_with = EllipseModel()
        ell_with.estimate(with_tape_points)
        xc1, yc1, a1, b1, theta1 = ell_with.params


        wo_tape_points = np.vstack((x_axis_wo_tape,y_axis_wo_tape)).T
        x_wo = wo_tape_points[:,0]
        y_wo = wo_tape_points[:,1]

        ell_wo = EllipseModel()
        ell_wo.estimate(wo_tape_points)
        xc2, yc2, a2, b2, theta2 = ell_wo.params



        fig, axs = plt.subplots(1, 1, sharex=True, sharey=True)
        axs.scatter(x_with, y_with)
        axs.scatter(xc1, yc1, color='red', s=100)
        #axs.set_xlim(x_with.min(), x_with.max())
        #axs.set_ylim(y_with.min(), y_with.max())
        ell_patch = Ellipse((xc1, yc1), 2 * a1, 2 * b1, theta1 * 180 / np.pi, edgecolor='blue', facecolor='none')
        axs.add_patch(ell_patch)
        #plt.show()

        axs.scatter(x_wo, y_wo)
        axs.scatter(xc2, yc2, color='red', s=100)
        #axs.set_xlim(x_wo.min(), x_wo.max())
        #axs.set_ylim(y_wo.min(), y_wo.max())
        ell_patch2 = Ellipse((xc2, yc2), 2 * a2, 2 * b2, theta2 * 180 / np.pi, edgecolor='red', facecolor='none')
        axs.add_patch(ell_patch2)
        plt.xlabel('Mean')
        plt.ylabel('STD')

        plt.show()


        y_test = np.zeros((self.work_file.shape[0],1))
        y_test[:with_tape_sampels]=1

        thresh = 220
        y_pred = np.zeros((self.work_file.shape[0],1))
        y_pred=  (self.work_file2[:, -1] >thresh)*1

        matrix = confusion_matrix(y_test, y_pred)
        tn, fp, fn, tp = matrix.ravel()

        print("True negative", tn)
        print('False negative', fn)
        print(((fn) / (fn + tn)) * 100, '% false negative Fails', '\n')

        print("True positive", tp)
        print("False positive", fp)
        print(((fp) / (fp + tp)) * 100, '% false positive Fails', '\n')


        return()


    def calc_confusion_matrix(self):
        matrix = confusion_matrix(self.input_dataset_label, self.work_file[:, -1])
        tn, fp, fn, tp = matrix.ravel()

        print("True negative", tn)
        print('False negative', fn)
        print(((fn) / (fn + tn)) * 100, '% false negative Fails', '\n')

        print("True positive", tp)
        print("False positive", fp)
        print(((fp) / (fp + tp)) * 100, '% false positive Fails', '\n')

        return ()

    def point_above_thresh(self):
        num_of_points_wo_tape = np.loadtxt('C:/Projects/Python/Tape_detect/Tape_dataset/records/num_of_points_wo_tape.txt')
        sum_of_points_wo_tape = np.loadtxt('C:/Projects/Python/Tape_detect/Tape_dataset/records/sum_of_points_wo_tape.txt')
        start_of_ch_points_wo_tape = np.loadtxt('C:/Projects/Python/Tape_detect/Tape_dataset/records/start_of_channel_wo_tape.txt')
        plt.scatter(num_of_points_wo_tape, sum_of_points_wo_tape, label='wo tape',facecolors='none', edgecolors='r')
        wo_tape_x = np.vstack((num_of_points_wo_tape,sum_of_points_wo_tape)).T
        wo_tape_y = np.zeros((wo_tape_x.shape[0], 1))

        num_of_points_with_tape = np.loadtxt('C:/Projects/Python/Tape_detect/Tape_dataset/records/num_of_points_with_tape.txt')
        sum_of_points_with_tape = np.loadtxt('C:/Projects/Python/Tape_detect/Tape_dataset/records/sum_of_points_with_tape.txt')
        start_of_ch_points_with_tape = np.loadtxt('C:/Projects/Python/Tape_detect/Tape_dataset/records/start_of_channel_with_tape.txt')
        plt.scatter(num_of_points_with_tape, sum_of_points_with_tape, label='with tape',facecolors='none', edgecolors='b')
        with_tape_x = np.vstack((num_of_points_with_tape,sum_of_points_with_tape)).T
        with_tape_y = np.ones((with_tape_x.shape[0], 1))


        plt.hist(num_of_points_wo_tape, bins='auto')
        plt.hist(num_of_points_with_tape, bins='auto')
        plt.show()


        plt.hist(sum_of_points_wo_tape, bins='auto')
        plt.hist(sum_of_points_with_tape, bins='auto')
        plt.show()

        X = np.vstack((with_tape_x,wo_tape_x))
        Y = np.vstack((with_tape_y,wo_tape_y))
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, shuffle=True)
        logist_regression = LogisticRegression(penalty='l1',solver='liblinear').fit(X_train,y_train)  # thresh 0.999 FN 1.2 , FP 0.56
        #logist_regression = LogisticRegression().fit(X_train,y_train)  # thresh 0.999 FN 1.2 , FP 0.56

        print("logistic regression prediction accuracy is: ", logist_regression.score(X_test, y_test) * 100, "%")
        Y_pred = logist_regression.predict(X_test)
        grid = {"C":[0.1,0.5,0.7,1,1.2,1.5,1.7,2,3,4],"penalty": ["l1", "l2"], "solver": ["newton-cg", "lbfgs", "liblinear", "sag", "saga"]}

        logreg = LogisticRegression(max_iter=200)
        logreg_cv = GridSearchCV(logreg, grid, cv=10)
        logreg_cv.fit(X_train, y_train)

        print("tuned hpyerparameters :(best parameters) ", logreg_cv.best_params_)
        print("accuracy :", logreg_cv.best_score_)

        # Fit the classifier
        '''
        from sklearn import linear_model
        clf = linear_model.LogisticRegression(C=1e5)
        clf.fit(X, Y)
        # and plot the result
        plt.figure(1, figsize=(4, 3))
        plt.clf()
        plt.scatter(X[:,0] , Y, color='black', zorder=20)
        plt.show()
        plt.figure(1, figsize=(4, 3))
        plt.clf()
        plt.scatter(X[:, 1], Y, color='red', zorder=20)
        plt.show()
        '''

        matrix = confusion_matrix(y_test, Y_pred)
        tn, fp, fn, tp = matrix.ravel()

        print("True negative", tn)
        print('False negative', fn)
        print(((fn) / (fn + tn)) * 100, '% false negative Fails', '\n')

        print("True positive", tp)
        print("False positive", fp)
        print(((fp) / (fp + tp)) * 100, '% false positive Fails', '\n')


        '''
        x_line = np.linspace(0,25, num=25)
        a=150
        b=500
        line = (a*x_line)+b
        plt.plot(line)

        plt.xlabel('Num of points above threshold')
        plt.ylabel('Sum of points above threshold')
        plt.legend()
        plt.show()
        '''
        return

if __name__ == "__main__":


    #input_dataset_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/X_regular_channels_new_sens_2015_hist_norm.npy'

    #input_dataset_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/X_regular_channels_new_sens_2015_hist_norm_mach2.npy'
    #input_dataset_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Regular/X_regular_channels_2015_with_10mm_hist_norm.npy'
    #input_dataset_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/X_regular_channels_new_sens_2015_10mm_and_wo_hist_norm_mach1.npy'# with_tape_sampels 2088
    #input_dataset_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Thread/X_close_to_thread_channels_2015_hist_norm.npy'# with_tape_sampels 21635
    #input_dataset_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/watermark/X_watermark_channels_2015_hist_norm.npy'# with_tape_sampels 65017
    #input_dataset_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/Regular/X_regular_channels_10mm_tape_2015_hist_norm.npy'
    #input_dataset_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/X_regular_channels_new_sens_2015_begin_tape_and_wo_hist_norm_mach1.npy'#23876
    input_dataset_file_path = 'C:/Projects/Python/Tape_detect/Tape_dataset/datasets/datasets_2015/new_sens/X_regular_channels_new_sens_2015_mix_and_10mm_tape_and_wo_hist_norm_mach1.npy'#26691


    RWSC = Running_window_STD_classifier(input_dataset_file_path)
    #RWSC.std_calc()
    #RWSC.calc_grad()
    RWSC.calc_mean_and_std()
    #RWSC.point_above_thresh()