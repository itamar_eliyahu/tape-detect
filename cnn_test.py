import numpy as np
from keras.utils import to_categorical
import tensorflow as tf
from keras.layers import  Dropout, MaxPooling1D, Flatten, Input, Dense, MaxPool2D, Lambda, Bidirectional,Conv1D,GlobalAveragePooling1D,Reshape
from keras.models import Model,Sequential
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint,TensorBoard,EarlyStopping,LearningRateScheduler
import time
from sklearn.metrics import classification_report, confusion_matrix
from keras.utils.vis_utils import plot_model
import os
import graphviz
import timeit
import matplotlib.pyplot as plt
from cnn_model import one_dim_cnn_model,one_dim_cnn_model_for_thread_detect,one_dim_cnn_cust_model_for_third_part
from keras import backend as K
from keras.engine.input_layer import InputLayer
from scipy import signal
from keras import backend as K
from matplotlib.colors import LogNorm
from sklearn import mixture
from sklearn.tree import DecisionTreeClassifier
from sklearn.datasets.samples_generator import make_blobs
from scipy.stats import multivariate_normal
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score

def scale1 (list):
    #list -= np.average(list)
    #list /= np.std(list)

    #if (np.min(list) < -1 or np.max(list) >1):
    #list -= np.min(list)
    #list /= np.max(list) - np.min(list)
    #list *= 2
    #list -= 1


    list *= 10000
    #list= np.round(list)
    #list = list.astype(np.int16)
    
    return(list)

class Test_model():

    flags = {'thread_detect_cnn':1,'debug_mode':0,'save_weights_to_file':1,'save_features_to_file':0,'parse_weights':1,'scale_weights':0}

    def __init__(self, best_model_path,model,X_test,Y_test):
        self.best_model_path = best_model_path
        self.model = model
        self.X_test = X_test.clip(0, 255).astype(np.uint8)
        #self.X_test -= 127
        #self.X_test /=127

        self.Y_test =Y_test
        self.weights_scale_factor = 2**13#10000
        self.c_array_file_base_path = 'C:/Projects/Python/Tape_detect/test_vectors/'

        # just for thread detect cnn we need 1 channel conv layer+ avg layer for reducing inout dimension
        if (self.flags['thread_detect_cnn']):
            self.first_layer_index  = 1
            self.second_layer_index = 3# index 2 is for the AVG pooling layer which has no parameters
            self.third_layer_index  = 4
        else:
            self.first_layer_index  = 0
            self.second_layer_index = 1
            self.third_layer_index  = 2

    def load_weights(self):
        self.model.load_weights(self.best_model_path)
        self.all_weights = []
        for layer in self.model.layers:
            layer_w = layer.get_weights()
            #layer_casted = [(layer_w_uint8 * 255).astype(np.uint8) for layer_w_uint8 in layer_w]
            self.all_weights.append(layer_w)


        if (self.flags['scale_weights']):
            self.all_weights[1][0] = scale1(self.all_weights[1][0])
            self.all_weights[1][1] = scale1(self.all_weights[1][1])
            self.all_weights[2][0] = scale1(self.all_weights[2][0])
            self.all_weights[2][1] = scale1(self.all_weights[2][1])
            self.all_weights[5][0] = scale1(self.all_weights[5][0])
            self.all_weights[5][1] = scale1(self.all_weights[5][1])
            self.all_weights[7][0] = scale1(self.all_weights[7][0])
            self.all_weights[7][1] = scale1(self.all_weights[7][1])
            self.all_weights[9][0] = scale1(self.all_weights[9][0])
            self.all_weights[9][1] = scale1(self.all_weights[9][1])

            model_quant = self.model
            for i in range(0,len(self.model.layers)):
                model_quant.layers[i].set_weights(self.all_weights[i])

            self.model = model_quant
    def save_weight_c_format(self,weights_arr,biases_arr,text_file_name):

        biases_arr = np.expand_dims(biases_arr, axis=1)
        data_arr = np.hstack((weights_arr,biases_arr )).astype(int)
        rows, cols = data_arr.shape
        # c_file = open('C:/Projects/Python/Tape_detect/test_vectors/conv1D_16ch_weights_plus_bias_c.txt', 'w')
        c_file_path = str(self.c_array_file_base_path + str(text_file_name)+'.txt')
        c_file = open(c_file_path, 'w')
        c_file.write('{')
        for i in range(0, rows):
            c_file.write('{')
            for j in range(0, cols):
                if (j != cols - 1):
                    c_file.write(str((data_arr[i, j])) + ',')
                else:
                    c_file.write(str(data_arr[i, j]))
            if (i != rows - 1):
                c_file.write('}' + '\n')
            else:
                c_file.write('}')
        c_file.write('}')
        c_file.close()

    def weights_extractor(self):
        self.load_weights()

        if (self.flags['thread_detect_cnn']):
            self.conv1D_1ch_weights = np.array(self.all_weights[self.first_layer_index][0])
            self.conv1D_1ch_weights = self.conv1D_1ch_weights.reshape(1, 15)
            self.conv1D_1ch_weights *= self.weights_scale_factor
            self.conv1D_1ch_biases = np.array(self.all_weights[self.first_layer_index][1])
            self.conv1D_1ch_biases *= self.weights_scale_factor
            #np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/conv1D_1ch_weights.txt', self.conv1D_1ch_weights, fmt='%i')
            #np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/conv1D_1ch_biases.txt', self.conv1D_1ch_biases, fmt='%i')

        self.conv1D_16ch_weights = np.array(self.all_weights[self.second_layer_index][0])
        self.conv1D_16ch_weights = self.conv1D_16ch_weights.reshape(16, 16).T
        self.conv1D_16ch_weights *= self.weights_scale_factor
        self.conv1D_16ch_weights = self.conv1D_16ch_weights.astype(int)

        self.conv1D_16ch_biases = np.array(self.all_weights[self.second_layer_index][1])
        self.conv1D_16ch_biases = self.conv1D_16ch_biases.reshape(16, 1)
        self.conv1D_16ch_biases *= self.weights_scale_factor
        self.conv1D_16ch_biases = self.conv1D_16ch_biases.astype(int)

        self.FC_16ch_weights = np.array(self.all_weights[self.third_layer_index][0])
        self.FC_16ch_weights *= self.weights_scale_factor
        self.FC_16ch_weights = self.FC_16ch_weights.astype(int)

        self.FC_16ch_biases = np.array(self.all_weights[self.third_layer_index][1])
        self.FC_16ch_biases *= self.weights_scale_factor
        self.FC_16ch_biases = self.FC_16ch_biases.astype(int)

        if (self.flags['save_weights_to_file']):
            self.save_weight_c_format(self.conv1D_1ch_weights,self.conv1D_1ch_biases,'conv1D_1ch_weights')
            self.save_weight_c_format(self.FC_16ch_weights, self.FC_16ch_biases, 'FC_16ch_weights')
            self.save_weight_c_format(self.conv1D_16ch_weights, self.conv1D_16ch_biases, 'conv1D_16ch_weights')
            #np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/FC_16ch_biases.txt',self.FC_16ch_biases,fmt='%i')
            #np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/FC_16ch_weights.txt', self.FC_16ch_weights, fmt='%i')
            #np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/conv1D_16ch_weights.txt', self.conv1D_16ch_weights, fmt='%i')
            #np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/conv1D_16ch_biases.txt', self.conv1D_16ch_biases, fmt='%i')

        return()

    def predict(self):
        self.load_weights()
        all_weights_2 = []
        for layer in self.model.layers:
            layer_w = layer.get_weights()
            layer_casted = [(layer_w_uint8) for layer_w_uint8 in layer_w]
            # layer_casted = [(layer_w_uint8 * 255).astype(np.uint8) for layer_w_uint8 in layer_w]
            all_weights_2.append(layer_w)

        y_pred = self.model.predict(self.X_test)
        thresh = 0.51
        y_pred[y_pred < thresh] = 0
        y_pred[y_pred >= thresh] = 1
        #matrix= confusion_matrix(y_test.argmax(axis=1), y_pred.argmax(axis=1))
        matrix = confusion_matrix(self.Y_test, y_pred)
        tn, fp, fn, tp = matrix.ravel()
        print("True negative", tn)
        print('False negative', fn)
        print(((fn) / (fn + tn)) * 100, '% false negative Fails', '\n')

        print("True positive", tp)
        print("False positive", fp)
        print(((fp) / (fp + tp)) * 100, '% false positive Fails', '\n')

        print((fn + fp), 'Fails , out of', (fn + fp + tn + tp))
        print(((fn + fp) / (fn + fp + tn + tp)) * 100, '% Fails')

        total = y_pred[y_pred > 0]
        print("total")

    def features_extractor(self):
        self.load_weights()

        extractor = Model(inputs=self.model.inputs, outputs=[layer.output for layer in self.model.layers])
        Layers = [layer.output for layer in model.layers]
        self.features = extractor(self.X_test[0,:,:])
        #self.features = extractor(self.X_test)

        self.input_vect = self.features[0].numpy()
        self.input_vect = self.input_vect[0, :, :].T

        #np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/input_vect.txt', self.input_vect, fmt='%i')

        self.l1_outputs = self.features[1].numpy()
        self.l1_outputs = self.l1_outputs.reshape(1,75)
        np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/output_after_conv.txt', self.l1_outputs, fmt='%i')

        self.l2_outputs = self.features[2].numpy()
        self.l2_outputs = self.l2_outputs.reshape(1, 25)
        np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/output_after_AVG.txt', self.l2_outputs, fmt='%i')
        '''
        self.fc_outputs = self.features[2].numpy()

        self.l3_outputs = self.features[3].numpy()

        if (self.flags['save_features_to_file']):
            np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/input_vector.txt', self.input_vect,fmt='%i')
            np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/conv_1D_outputs.txt', self.l1_outputs[0,:,:],fmt='%f')
            np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/matmul_outputs.txt', self.fc_outputs[0,:,:],fmt='%f')
            np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/avg_outputs.txt', self.l3_outputs[0,:,:].T,fmt='%f')
        #self.output_features =  np.hstack((self.l3_outputs.reshape(self.l3_outputs.shape[0],16),self.Y_test))
        #np.save('C:/Projects/Python/Tape_detect/test_vectors/outputs_features.npy', self.output_features)
        '''
        return()

    def calc_layers_outputs(self):
        self.load_weights()
        self.weights_extractor()
        self.features_extractor()
        l1_weights = self.conv1D_16ch_weights#/ self.weights_scale_factor
        l1_bias = self.conv1D_16ch_biases#/ self.weights_scale_factor

        fc_weights = self.FC_16ch_weights#/ self.weights_scale_factor
        fc_bias = self.FC_16ch_biases#/ self.weights_scale_factor

        if (self.flags['thread_detect_cnn']):
            input_vect = np.zeros((1,89))
            input_vect[0,7:82] = self.input_vect
            one_ch_conv_layer_output = np.zeros((1,75))
            for stride in range(0, 75):
                print(stride,stride+15)
                data_section = input_vect[0, stride:stride + 15]
                print(data_section.shape)
                cnv_plus_bias = np.sum(data_section * self.conv1D_1ch_weights) + self.conv1D_1ch_biases
                one_ch_conv_layer_output[0,stride]=(np.maximum(0,cnv_plus_bias))#After Relu
            one_ch_conv_layer_output=one_ch_conv_layer_output/self.weights_scale_factor
            avg_pool_layer_output = np.mean(one_ch_conv_layer_output.reshape(-1, 3), axis=1)


        l1_outputs_calc = np.zeros((16,10))
        for ch in range (0,16):
            for stride in range(0, 10):
                data_section = self.input_vect[0, stride:stride + 16]
                cnv_plus_bias = np.sum(data_section * l1_weights[ch]) + l1_bias[ch]
                l1_outputs_calc[ch,stride]=(np.max([0,cnv_plus_bias[0]]))#After Relu
        l1_outputs_calc=l1_outputs_calc.T/self.weights_scale_factor
        l1_outputs = self.features[1].numpy()

        fc_outputs_calc = np.dot(l1_outputs_calc,fc_weights) + fc_bias
        fc_outputs_calc = np.maximum(fc_outputs_calc, 0)/self.weights_scale_factor
        fc_outputs = self.features[2].numpy()

        # matrix multiplication without transpose L! marix
        temp_outputs_calc = np.zeros((10, 16))
        temp_inp_mat = l1_outputs_calc.T
        for inp_mat_col in range (0,10):
            for inp_mat_row in range(0,16):
                temp_outputs_calc[inp_mat_col,inp_mat_row] =  np.maximum((np.dot(temp_inp_mat[:,inp_mat_col],fc_weights[:,inp_mat_row])+ fc_bias[inp_mat_row]),0)/self.weights_scale_factor

        l3_outputs = self.features[3].numpy()
        l3_outputs_calc = np.average(fc_outputs_calc, axis=0)/self.weights_scale_factor

        '''
        # matrix multiplication test
        #l3_weights_new =  np.transpose(l3_weights, (1,0,2)).copy()
        #l3_weights_new = l3_weights
        #l3_weights dim : (16,16,8)
        #l3_bias dim : (8,)
        #fc_outouts :: (16,16,8)
        l3_outouts_calc_temp = np.zeros((16,10, 8))
        l3_outouts_calc_temp_2 = np.zeros((16, 8, 10))
        for w_ch in range (0,16):
            for filter in range(0, 8):
                value = (fc_outouts[:, :] @ l3_weights[w_ch, :, filter])+ l3_bias[filter]
                value = np.maximum(0, value)  # Relu
                l3_outouts_calc_temp_2[w_ch,filter,:] = value
        for w_ch in range(0, 16):
            l3_outouts_calc_temp[w_ch,:,:] = l3_outouts_calc_temp_2[w_ch,:,:].T
        l3_outouts = np.sum(l3_outouts_calc_temp,axis=0)

        # by stride
        fc_outouts_pad_2 = np.zeros((10,25))
        fc_outouts_pad_2[:,:16] = fc_outouts_calc
        l3_outouts_calc_by_stride_temp = np.zeros((16,10, 8))
        l3_outouts_calc_by_stride_2_temp = np.zeros((16, 8, 10))
        for w_ch in range (0,16):
            for filter in range(0, 8):
                sigma =0.0
                values_vect = np.zeros(10)
                for din_row in range (0,10):
                    for stride in range (0,10):
                        value = fc_outouts_pad_2[din_row, stride:stride+16] @ l3_weights[w_ch, :, filter]+ l3_bias[filter] # each filter is a column of the weght matix
                        #value_2 = np.sum(fc_outouts_pad_2[din_row, stride:stride+16]* l3_weights[w_ch, :, filter]) + l3_bias[filter]
                        value = (np.max([0, value]))  # Relu
                        sigma += value
                    values_vect[din_row] = sigma
                l3_outouts_calc_by_stride_2_temp[w_ch,filter,:] = values_vect
        for w_ch in range(0, 16):
            l3_outouts_calc_by_stride_temp[w_ch,:,:] = l3_outouts_calc_by_stride_2_temp[w_ch,:,:].T
        l3_outouts_calc_sum_by_stride = np.sum(l3_outouts_calc_by_stride_temp,axis=0)
        '''
    def feature_classification(self):
        self.features_extractor()
        mask = self.output_features[:,16]==1
        True_features =self.output_features[mask==True,:]
        False_features = self.output_features[mask == False, :]

        # fit a Gaussian Mixture Model with two components
        # 0. Create dataset
        #X, Y = make_blobs(cluster_std=0.5, random_state=20, n_samples=1000, centers=5)
        # Stratch dataset to get ellipsoid dat
        #X = np.dot(X, np.random.RandomState(0).randn(2, 2))

        X_features = self.output_features[:,:-1]
        Y_features= self.output_features[:,-1]

        #GMM
        X=X_features[:10000,[4,10]]#most importance features
        x, y = np.meshgrid(np.sort(X[:, 0]), np.sort(X[:, 1]))
        XY = np.array([x.flatten(), y.flatten()]).T
        GMM = mixture.GaussianMixture(n_components=4, covariance_type='full',max_iter = 1000)
        GMM.fit(X)
        print('Converged:', GMM.converged_)  # Check if the model has converged
        print('Number of iterations:', GMM.n_iter_)
        means = GMM.means_
        covariances = GMM.covariances_
        # Predict
        Y = np.array([[0.5],[0.5]])
        prediction = GMM.predict_proba(Y.T)
        print(prediction)
        # Plot
        fig = plt.figure(figsize=(10, 10))
        ax0 = fig.add_subplot(111)
        ax0.scatter(X[:, 0], X[:, 1])
        ax0.scatter(Y[0, :], Y[1, :], c='orange', zorder=10, s=100)
        for m, c in zip(means, covariances):
            multi_normal = multivariate_normal(mean=m, cov=c)
            ax0.contour(np.sort(X[:, 0]), np.sort(X[:, 1]), multi_normal.pdf(XY).reshape(len(X), len(X)),colors='black', alpha=0.3)
            ax0.scatter(m[0], m[1], c='grey', zorder=10, s=100)

        plt.show()

        # Desicion tree
        X_train, X_test, y_train, y_test = train_test_split(X_features,Y_features, test_size=0.25, shuffle=True)
        Desicion_Tree = DecisionTreeClassifier(criterion='entropy').fit(X_train, y_train)
        Y_pred = Desicion_Tree.predict(X_test)
        print("Desicion Tree prediction accuracy is: ", Desicion_Tree.score(X_test, y_test) * 100, "%")

        plt.plot(Desicion_Tree.feature_importances_)
        plt.show()

        # Knn
        KNeighborsClassifier(algorithm='auto',leaf_size=30,metric='minkowski',metric_params=None,n_jobs=1,n_neighbors=5,p=2,weights='uniform')
        knn = KNeighborsClassifier()
        knn.fit(X_train, y_train)
        print("KNeighborsClassifier prediction accuracy is: ", knn.score(X_test, y_test) * 100, "%")






if __name__ == "__main__":

    X = np.load('C:/Projects/Python/Tape_detect/test_vectors/network_outputs.npy')
    #X = np.load('C:/Projects/Python/Tape_detect/test_vectors/outputs_features.npy')
    #X_features = X[:, :-1]
    #X_features = X[:,[1,15,11]]
    #Y_features = X[:, -1]

    X_from_c_design = np.loadtxt('C:/Projects/Python/Tape_detect/test_vectors/c_design_output.txt')
    X_features = X_from_c_design
    Y_features = X[:, -1][:-1]
    #np.savetxt('C:/Projects/Python/Tape_detect/test_vectors/Thread_detect_features_and_lables.txt',np.hstack((X_features,np.expand_dims(Y_features,axis=1))),fmt='%i')



    X_train, X_test, y_train, y_test = train_test_split(X_features, Y_features, test_size=0.25, shuffle=True)

    # Desicion tree
    Desicion_Tree = DecisionTreeClassifier(criterion='entropy').fit(X_train, y_train)
    Y_pred_dt = Desicion_Tree.predict(X_test)
    #print("Desicion Tree prediction accuracy is: ", Desicion_Tree.score(X_test, y_test) * 100, "%")
    #plt.plot(Desicion_Tree.feature_importances_)
    #plt.show()
    #Cross validation
    scores = cross_val_score(Desicion_Tree, X_features, Y_features, cv=5)
    print("Desicion_Tree cross_val Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))


    # logistic regression
    logist_regression = LogisticRegression(random_state=0).fit(X_train, y_train)
    Y_pred_lr = logist_regression.predict(X_test)
    Y_pred_lr = Y_pred_lr.T

    weights = logist_regression.coef_
    #weights = np.array([2.02974178, -2.02827026, -1.66486965,  3.7358174 , -0.32187554,1.56361319, -3.57341289,  2.51069765, -2.09967019, -0.23454262,0.06776431,  2.55470682,  0.17940604, -0.3072654 , -2.41347976,4.531925  ])

    input_vector= np.array([0, 0.22340667, 0.29714434, 0.31255781, 0 ,0.24365507, 0, 0, 0, 0,0, 2.01794781, 0, 0, 0,1.32968738])
    #z = (np.dot(input_vector, weights.T))
    #thresh = 0.9
    z = (np.dot(X_test, weights.T))

    thresh =0.9998
    Y_pred_lr_np_calculated = 1 / (1 + np.exp(-z))
    Y_pred_lr_np_calculated[Y_pred_lr_np_calculated <thresh] = 0
    Y_pred_lr_np_calculated[Y_pred_lr_np_calculated >= thresh] = 1

    #compare between Y_pred_lr_np_calculated and sclearn Y_pred
    res= np.hstack((Y_pred_lr_np_calculated,np.expand_dims(Y_pred_lr,axis =1)))
    comp = np.apply_along_axis(lambda x: res[:,0]==res[:,1],0, res)
    acc = np.sum(comp[:,0])/4706*100

    #print("logistic regression prediction accuracy is: ", logist_regression.score(X_test, y_test) * 100, "%")
    #plt.plot(Desicion_Tree.feature_importances_)
    #plt.show()

    #Cross validation
    scores = cross_val_score(logist_regression, X_features, Y_features, cv=5)
    print("logistic regression cross_val Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

    matrix = confusion_matrix(y_test, Y_pred_lr)
    tn, fp, fn, tp = matrix.ravel()
    print("True negative", tn)
    print('False negative', fn)
    print(((fn) / (fn + tn)) * 100, '% false negative Fails', '\n')

    print("True positive", tp)
    print("False positive", fp)
    print(((fp) / (fp + tp)) * 100, '% false positive Fails', '\n')

    #print((fn + fp), 'Fails , out of', (fn + fp + tn + tp))
    #print(((fn + fp) / (fn + fp + tn + tp)) * 100, '% Fails')

    x_1_indx = np.where(Y_features==1)
    x_0_indx = np.where(Y_features == 0)
    x_1 = X_features[x_1_indx,:].reshape(-1,3)
    y_1 = Y_features[x_1_indx]
    x_0 = X_features[x_0_indx, :].reshape(-1,3)
    y_0 = Y_features[x_0_indx]

    x = x_1[:,2][:5124]
    y=  x_0[:,2]
    m, b = np.polyfit(x,y ,deg=1)
    #plt.plot(x,'o', color='blue')
    #plt.plot(y,'x', color='orange')
    #plt.plot( m * x + b, color='red')
    #plt.show()

    #linear_regression_model = np.polyfit(x[:4000], y[:4000], deg=1)
    # Predicting values for the test set
    #linear_model_predictions = np.polyval(linear_regression_model, x[4000:])

    x_axis = np.arange(X_features.shape[0])
    #plt.plot(x_axis,X_features[:,15] ,'o', color='red')
    #plt.plot(x_axis, X_features[:, 11], 'o', color='blue')
    #plt.plot(x_axis, X_features[:, 1], 'o', color='orange')
    #plt.plot(x_1[:,1], 'X', color='orange')
    #plt.plot(x_0[:,1],  'X',color='blue')
    plt.plot(x_1[:,2], 'X', color='red')
    plt.plot(x_0[:,2],  'X',color='black')
    #plt.plot(x_1[:,0], 'X', color='yellow')
    #plt.plot(x_0[:,0],  'x',color='green')
    plt.show()
    '''
    # convolution along axis=1
    w1 = np.zeros(x.shape[:2] + y.shape[2:])
    for i in range(1, x.shape[1] - 1):
        w1[:, i - 1:i + 2, :] = x[:, i - 1:i + 2, :] @ y

    # matrix multiplication test
    l3_outouts_calc_temp = np.zeros((16, 10, 8))
    w = np.zeros((2, 4, 5))
    for w_ch in range(0, 2):
        for filter in range(0, 2):
            value = x[w_ch,:, :] @ y
            #value = np.maximum(0, value)  # Relu
            w[w_ch, :, :] = value

    '''
    n_outputs =1
    n_kernel_1 = 16
    n_kernel_2 = 16
    #Tape detect in the beginning
    #best_model_file_path = "C:/Projects/Python/Tape_detect/best_model/best_model.h5"
    #model = one_dim_cnn_cust_model_for_third_part(n_kernel_1, n_kernel_2, n_outputs)
    #X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_the_first_part_wo_thread/X_tape_detect_in_first_without_thread_first_part.npy')
    #Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_the_first_part_wo_thread/Y_tape_detect_in_first_without_thread_first_part.npy')


    # Thread detect in the whole signal
    best_model_file_path = "C:/Projects/Python/Tape_detect/best_model/thread_detect_best_model.h5"
    model = one_dim_cnn_model_for_thread_detect(n_kernel_1, n_kernel_2, n_outputs)
    X_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/X_tape_detect_in_whole_area_with_thread.npy')[:,:,:75]
    Y_test = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/Y_tape_detect_in_whole_area_with_thread.npy')

    #X = X_test.clip(0, 255).astype(np.uint8)

    TM = Test_model(best_model_file_path,model,X_test,Y_test)
    TM.calc_layers_outputs()
    #TM.load_weights()
    #TM.weights_extractor()
    #TM.predict()
    #TM.features_extractor()
    #TM.feature_classification()


    '''
    source = np.zeros((32,128,30000))
    fake_images = np.zeros_like(source)
    deviationFromPoint = 10
    
    for img in range (source.shape[2]):
        for row in range (source.shape[0]):
            for col in range(source.shape[1]):
                fake_images[row,col,img] = source[row,col,img] + np.random.randint(0,255)

    np.save('C:/Projects/Python/CRNN/dataset/fake_images.npy', fake_images)
    '''

    '''
    fake_images =np.load('C:/Projects/Python/CRNN/dataset/fake_images.npy' )
    fake_images = fake_images.reshape((fake_images.shape[2],fake_images.shape[0],fake_images.shape[1]))
    fake_images_lable = np.zeros((fake_images.shape[0],1))

    true_images = np.load('C:/Projects/Python/CRNN/dataset/serial_numbers_train_dataset_images.npy')
    true_images_lable = np.ones((true_images.shape[0],1))
    real_plus_fake_images = np.concatenate((true_images,fake_images))
    real_plus_fake_lables = np.vstack((true_images_lable, fake_images_lable))

    np.save('C:/Projects/Python/CRNN/dataset/real_plus_fake_images_for_descriminator_test.npy', real_plus_fake_images)
    np.save('C:/Projects/Python/CRNN/dataset/real_plus_fake_lables_for_descriminator_test.npy', real_plus_fake_lables)
    plt.imshow(fake_images[:,:,2],cmap='gray', vmin=0, vmax=255)
    plt.show()
    '''
    delta = 0
    delta_max = 0
    delta_min = 0.016

    '''
    for i in range (0,1000):
        start_time = timeit.timeit('"-".join(str(n) for n in range(100))', number=10000)
        #print("Start Time", timeit.timeit('"-".join(str(n) for n in range(100))', number=10000))
        for j in range (0,10):
            continue
        end_time = timeit.timeit('"-".join(str(n) for n in range(100))', number=10000)
        #print("End Time", timeit.timeit('"-".join(str(n) for n in range(100))', number=10000))
        delta = np.abs(end_time - start_time)

        if (delta > delta_max):
            delta_max = delta
        if (delta < delta_min) :
            delta_min = delta
    print('delta max',delta_max)
    print('delta min', delta_min)
    '''

