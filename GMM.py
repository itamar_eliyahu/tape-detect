import numpy as np
from scipy.stats import multivariate_normal
from sklearn.datasets import load_iris
from sklearn.mixture import GaussianMixture
from sklearn.model_selection import train_test_split


class GMM:
    def __init__(self, num_of_components, max_iter=5):
        self.num_of_components = num_of_components
        self.max_iter = int(max_iter)

    def initialize(self, X):
        self.shape = X.shape
        self.n_row, self.n_col = self.shape

        self.phi = np.full(shape=self.num_of_components, fill_value=1 / self.num_of_components)
        self.weights = np.full(shape=self.shape, fill_value=1 / self.num_of_components)

        random_row = np.random.randint(low=0, high=self.n_row, size=self.num_of_components)
        self.mu = [X[row_index, :] for row_index in random_row]
        self.sigma = [np.cov(X.T) for _ in range(self.num_of_components)]

    def e_step(self, X):
        # E-Step: update weights and phi holding mu and sigma constant
        self.weights = self.predict_proba(X)
        self.phi = self.weights.mean(axis=0)

    def m_step(self, X):
        # M-Step: update mu and sigma holding phi and weights constant
        for i in range(self.num_of_components):
            weight = self.weights[:, [i]]
            total_weight = weight.sum()
            self.mu[i] = (X * weight).sum(axis=0) / total_weight
            self.sigma[i] = np.cov(X.T,
                                   aweights=(weight / total_weight).flatten(),
                                   bias=True)

    def fit(self, X):
        self.initialize(X)
        for iteration in range(self.max_iter):
            self.e_step(X)
            self.m_step(X)

    def predict_proba(self, X):
        likelihood = np.zeros((self.n_row, self.num_of_components))
        for i in range(self.num_of_components):
            distribution = multivariate_normal(mean=self.mu[i],cov=self.sigma[i])
            likelihood[:, i] = distribution.pdf(X)

        numerator = likelihood * self.phi
        denominator = numerator.sum(axis=1)[:, np.newaxis]
        weights = numerator / denominator
        return weights

    def predict(self, X):
        weights = self.predict_proba(X)
        return np.argmax(weights, axis=1)



if __name__ == "__main__":
    iris = load_iris()
    X = iris.data
    np.random.seed(42)
    features_and_labels_arr = np.load('C:/Projects/Python/Tape_detect/test_vectors/tape_detect_in_watermark_channels_network_outputs_with_avg.npy')
    features_and_labels_arr2 = np.load('C:/Projects/Python/Tape_detect/test_vectors/network_outputs.npy')

    network_outoputs_features = features_and_labels_arr[:, :-1]
    Y_test= features_and_labels_arr[:, -1]
    X_train, X_test, y_train, y_test = train_test_split(network_outoputs_features,Y_test, test_size=0.25,shuffle=True)

    gmm = GaussianMixture(n_components=2,covariance_type='full', max_iter=20, random_state=0)
    Z = -gmm.score_samples(X_test)
    Z = Z.reshape(X.shape)

    CS = plt.contour(X, Y, Z, norm=LogNorm(vmin=1.0, vmax=1000.0),
                     levels=np.logspace(0, 3, 10))
    CB = plt.colorbar(CS, shrink=0.8, extend='both')
    plt.scatter(X_train[:, 0], X_train[:, 1], .8)

    plt.title('Negative log-likelihood predicted by a GMM')
    plt.axis('tight')
    plt.show()
    #gmm = GMM(num_of_components=30, max_iter=10)
    gmm.fit(network_outoputs_features)
    print("Stop")