from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
import numpy as np
from sklearn import tree

x = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/noNorm/x_data_test_mid.npy')
y = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/noNorm/y_data_test_mid.npy')
# indices = np.random.permutation(X.shape[0])
# images_train, images_test = X[indices[:int(X.shape[0] * 0.8)]], X[indices[int(X.shape[0] * 0.8):]]
# labels_train, labels_test = Y[indices[:int(X.shape[0] * 0.8)]], Y[indices[int(X.shape[0] * 0.8):]]
x_r = x.reshape(x.shape[0],x.shape[2])
X_train, X_test, y_train, y_test = train_test_split(x_r, y, test_size=0.25, shuffle=True , random_state=0)

table = np.concatenate((x_r,y),axis=1)
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

#df = pd.read_clipboard()
#table = df.pivot('Y', 'X', 'Value')
ax = sns.heatmap(table)
ax.invert_yaxis()
print(table)
plt.show()


clf = tree.DecisionTreeClassifier()
clf = clf.fit(x_r, y)
tree.plot_tree(clf)
gnb = GaussianNB()
y_pred = gnb.fit(X_train, y_train).predict(X_test)
print("Number of mislabeled points out of a total %d points : %d"% (X_test.shape[0], (y_test != y_pred).sum()))
