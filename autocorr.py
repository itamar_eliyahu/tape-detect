import numpy as np
import matplotlib.pyplot as plt

#samples_file = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/TapeNew_sensor_wo_norm/tape_detect_in_whole_area_with_thread/X_a4z.npy')
x_samples_file = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/X_regular_channels_wo_tape.npy')[:,:,:100]
y_samples_file = np.load('C:/Projects/Python/Tape_detect/Tape_dataset/datasets/Y_regular_channels_wo_tape.npy')

channels_wo_tape = x_samples_file[np.where(y_samples_file==0)]
samples_file = channels_wo_tape#.reshape(channels_wo_tape.shape[0],channels_wo_tape.shape[2])

#area_diff = area-area_avg
#max_diff = np.max(area_diff)
#max_diff_index_1 = np.where(area_diff == max_diff)
area=np.cumsum(samples_file,axis=1)[:,-1]
area_avg = np.mean(area)

area_diff = area-area_avg
area_diff = np.ma.array(area_diff, mask=False)

r = 1000
diff_and_ind = np.zeros((r,2))
#fig, axarr = plt.subplots(1, 1)
for i in range(0,r):
    max_diff = area_diff.max()
    max_diff_index = np.where(area_diff == max_diff)
    area_diff.mask[max_diff_index] = True
    diff_and_ind[i,0] =max_diff
    diff_and_ind[i, 1] = max_diff_index[0][0]
    #axarr.plot(samples_file[max_diff_index[0][0]])
#plt.show()


num_of_tape_channels = 1#10
len = 180#samples_file.shape[0] - num_of_tape_channels*2
fig, axarr = plt.subplots(2, 5)
offset_range = 1#10
for offset in range (0,offset_range):
    for i in range(0,len,num_of_tape_channels):
        autocorrelation = np.correlate(samples_file[i+offset], samples_file[i+10+offset], mode="full")
        #autocorrelation = np.correlate(np.array([10,20,30,40,50,60,70,80,90]),np.array([10,20,30,40,50,60,70,80,90]), mode="full")
        autocorrelation = autocorrelation[autocorrelation.size//2:]
        #print (i+offset,' ',i+offset+10)

        axarr[int(offset/5),int(offset %5)].set_title("channel%d" % offset,fontsize=10)
        axarr[int(offset/5),int(offset %5)].plot(autocorrelation)

plt.show()


