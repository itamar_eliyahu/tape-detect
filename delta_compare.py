import timeit
import numpy as np

if __name__ == "__main__":
    delta = 0
    delta_max = 0
    delta_min = 0.016

    for i in range (0,1000):
        start_time = timeit.timeit('"-".join(str(n) for n in range(100))', number=10000)
        #print("Start Time", timeit.timeit('"-".join(str(n) for n in range(100))', number=10000))
        for j in range (0,10):
            continue
        end_time = timeit.timeit('"-".join(str(n) for n in range(100))', number=10000)
        #print("End Time", timeit.timeit('"-".join(str(n) for n in range(100))', number=10000))
        delta = np.abs(end_time - start_time)

        if (delta > delta_max):
            delta_max = delta
        if (delta < delta_min) :
            delta_min = delta
    print('delta max',delta_max)
    print('delta min', delta_min)
