import numpy as np

#weights = np.loadtxt('C:/Projects/Python/Tape_detect/test_vectors/conv1D_16ch_weights.txt')
#biases = np.loadtxt('C:/Projects/Python/Tape_detect/test_vectors/conv1D_16ch_biases.txt')
weights = np.loadtxt('C:/Projects/Python/Tape_detect/test_vectors/FC_16ch_weights.txt')
biases = np.loadtxt('C:/Projects/Python/Tape_detect/test_vectors/FC_16ch_biases.txt')
biases =np.expand_dims(biases,axis=1)
data= np.hstack((weights,biases)).astype(int)
rows,cols = data.shape
#c_file = open('C:/Projects/Python/Tape_detect/test_vectors/conv1D_16ch_weights_plus_bias_c.txt', 'w')
c_file = open('C:/Projects/Python/Tape_detect/test_vectors/FC_16ch_weights_plus_bias_c.txt', 'w')
c_file.write('{')
for i in range (0,rows):
    c_file.write('{')
    for j in range (0,cols):
        if (j != cols-1):
            c_file.write(str((data[i, j])) + ',')
        else:
            c_file.write(str(data[i, j]) )
    if (i != rows - 1):
        c_file.write('}'+'\n')
    else:
        c_file.write('}')
c_file.write('}')
c_file.close()
