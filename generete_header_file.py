from jinja2 import Template, FileSystemLoader,Environment
import numpy as np

conv_1d_layer_1_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_13.npy')
conv_1d_layer_1_weights = conv_1d_layer_1_weights.reshape(8, 16)
conv_1d_layer_1_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_1.npy')
conv_1d_layer_1_biases = np.expand_dims(conv_1d_layer_1_biases,axis=0)
conv_1d_layer_1_w_b = np.hstack((conv_1d_layer_1_weights, conv_1d_layer_1_biases.T))


conv_1d_layer_2_weigths = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_15.npy')
conv_1d_layer_2_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_2.npy')


w_0 = conv_1d_layer_2_weigths[0, :, :].reshape(-1, 8).T
w_0 = np.hstack((w_0, np.zeros((w_0.shape[0],1)))).astype(np.int)
w_1 = conv_1d_layer_2_weigths[1, :, :].reshape(-1, 8).T
w_1 = np.hstack((w_1, np.zeros((w_1.shape[0],1)))).astype(np.int)
w_2 = conv_1d_layer_2_weigths[2, :, :].reshape(-1, 8).T
w_2 = np.hstack((w_2, np.zeros((w_2.shape[0],1)))).astype(np.int)
w_3 = conv_1d_layer_2_weigths[3, :, :].reshape(-1, 8).T
w_3 = np.hstack((w_3, np.zeros((w_3.shape[0],1)))).astype(np.int)


cls_dense1_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_10.npy')
cls_dense1_weights = cls_dense1_weights.T
cls_dense1_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_4.npy')
cls_dense1_biases_for_sw = np.expand_dims(cls_dense1_biases,axis=0)
dense1_w_b = np.hstack((cls_dense1_weights.T, cls_dense1_biases_for_sw.T)).astype(int)

cls_dense2_weights = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_11.npy')
cls_dense2_weights = cls_dense2_weights.T  # reshape(num_of_channles,kernel_length)  # .T # even if it looks like transposed , the result is equale to the model results withou transpose it
cls_dense2_biases = np.load('C:/Projects/Python/Tape_detect/best_model_cnn2/tape_detect_in_thread_channels/parameters_vector_for_section_5.npy')
cls_dense2_biases_for_sw = np.expand_dims(cls_dense2_biases,axis=0)
dense2_w_b = np.hstack((cls_dense2_weights.T, cls_dense2_biases_for_sw)).astype(int)

file_loader = FileSystemLoader('C:/Projects/Python/Tape_detect/header_template')
env = Environment(loader=file_loader)
f = open('C:/Projects/Python/Tape_detect/Quant_weights/TapeHeader.h', "w+")

template_reg = env.get_template('TapeHeader_regular.h')
template_wm = env.get_template('TapeHeader_watermark.h')
template_thr = env.get_template('TapeHeader_thread.h')

f.write(template_reg.render(filt1_w_b = conv_1d_layer_1_w_b,filt2_v1_w = w_0,filt2_v2_w = w_1,filt2_v3_w = w_2,filt2_v4_w = w_3, filt2_b =conv_1d_layer_2_biases,filt3_w_b=dense1_w_b,filt4_w_b=dense2_w_b[0,:],class_threshold=34))
f.write(template_wm.render(filt1_w_b = conv_1d_layer_1_w_b,filt2_v1_w = w_0,filt2_v2_w = w_1,filt2_v3_w = w_2,filt2_v4_w = w_3, filt2_b =conv_1d_layer_2_biases,filt3_w_b=dense1_w_b,filt4_w_b=dense2_w_b[0,:],class_threshold=34))
f.write(template_thr.render(filt1_w_b = conv_1d_layer_1_w_b,filt2_v1_w = w_0,filt2_v2_w = w_1,filt2_v3_w = w_2,filt2_v4_w = w_3, filt2_b =conv_1d_layer_2_biases,filt3_w_b=dense1_w_b,filt4_w_b=dense2_w_b[0,:],class_threshold=34))

f.close()